//index.js
//获取应用实例
const app = getApp()
import {diaplayTime, lookImgs} from '../../../utils/util';
let loadAgain;
Page({
    data: {
        teamData: {},
        list: [],
        params: {
            pageSize: 20,
            schoolId: '',
            pageNum: 1,
        },
        face: {}
    },
    onShow: function () {
        if (loadAgain) {
          return;
        }
        wx.showLoading();
        let url = app.apiUrl + '/face/face_rank_info';
        let that = this;
        that.setData({
            'params.schoolId': wx.getStorageSync('schoolId'),
        })
        app.request.requestGetApi(url, that.data.params, that, that.successFun, that.failFun, wx.getStorageSync('token'))
    },
    // onHide(){
    //   loadAgain=false
    // },
    onUnload(){
      loadAgain = false
    },
    goUserDetail(e) {
        debugger
        let that = this;
        let id = e.currentTarget.dataset.faceid;
        let url = app.apiUrl + '/face/query/' + id;
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'));
    },
    successFun1: function (res, selfObj) {
        if (res.data) {
            lookImgs({
                src: res.data[0].fileRelativePath,
                imgs: res.data
            })
        }
    },
    successFun: function (res, selfObj) {
        wx.hideLoading();
        res.data.faceRankVOS && res.data.faceRankVOS.forEach(item => {
            item.userAvatarUrl=item.userAvatarUrl.indexOf('https')>-1?item.userAvatarUrl:app.apiUrlimg+item.userAvatarUrl
        });
        let faceImg= res.data.userAvatarUrl && res.data.userAvatarUrl.indexOf('https')>-1?res.data.userAvatarUrl:app.apiUrlimg+res.data.userAvatarUrl
        selfObj.setData({
            faceData: res.data,
            'faceData.userAvatarUrl':faceImg,
            list: res.data.faceRankVOS,
        })
        loadAgain = true
    }

});
