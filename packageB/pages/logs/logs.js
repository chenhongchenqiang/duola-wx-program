const app = getApp()
import {diaplayTime, lookImgs} from '../../../utils/util';
Page({
    data: {
        releaseData: {},
        userInfo: {},
        pageIndex: 1,
        topList: [],
        activeInd: 0,
        deleteFlag: null,
        userId: null,
        releselist: [],
        params: {
            pageNum: 1,
            pageSize: app.globalData.pageSize,
            topicTypeId: '',
            // schoolId: wx.getStorageSync('schoolId'),
        },
        show1: false,
        deleteData: {},
        deleteType: false,
        showLoading: true,
        loading: false
    },
    chat(e) {
        debugger

        let receiverUserId = e.currentTarget.dataset.receiveruserid.userId;
        let friend = e.currentTarget.dataset.receiveruserid.nickName;
        let avatarUrl = e.currentTarget.dataset.receiveruserid.avatarUrl;
        let senderUserId = wx.getStorageSync('userInfo').userId;
        wx.navigateTo({
            url: '/packageB/pages/chat/chat?friend=' + friend+'&receiverUserId='+receiverUserId+'&senderUserId='+senderUserId+'&avatarUrl='+avatarUrl
        })
    },
    takeCare(e) {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            let url = app.apiUrl + '/user/follow';
            let status = e.currentTarget.dataset.status;
            let that = this;
            var params = {
                status: status,
                userId2: that.data.userId,
            };
            app.request.requestPostApi(url, params, that, that.successFun6, that.failFun, wx.getStorageSync('token'), status)
        }

    },
    successFun6(res, selfObj, status) {
        if (status == 1) {
            wx.showToast({
                icon: 'success',
                title: '关注成功',
            });
        } else {
            wx.showToast({
                icon: 'success',
                title: '取消关注',
            });
        }

        this.setData({
            'userInfo.followFlag': status == 1 ? true : false
        });
        console.log(111)
    },
    onReachBottom: function () {
        var that = this;
        if (that.data.params.pageNum < that.data.releaseData.pages) {
            that.setData({
                'params.pageNum': that.data.params.pageNum * 1 + 1,
                loading: true
            })
            that.getMore(that.data.activeInd);
        }
    },
    getMore(type) {
        let that = this;
        if (that.data.topList[type] == '表白墙') {
            let url = app.apiUrl + '/confess/list';
            that.setData({
                'params.queryMe': true
            })
            app.request.requestGetApi(url, that.data.params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
        }
        if (that.data.topList[type] == '动态') {
            that.getList()
        }
        if (that.data.topList[type] == '颜值') {
            let url = app.apiUrl + '/face/list';
            let params = {queryMe: true};
            app.request.requestGetApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'));
        }
        if (that.data.topList[type] == '组队') {
            let url = app.apiUrl + '/team/list';
            that.setData({
                'params.queryMe': that.data.deleteFlag ? true : false,
                'params.userId': that.data.deleteFlag ? '' : that.data.userId,
            })
            app.request.requestGetApi(url, that.data.params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
        }
    },
    check(e) {
        let that = this;
        let type;
        type = e.currentTarget.dataset.type;
        if (that.data.activeInd != type) {
            that.setData({
                activeInd: type,
                releaseData: {},
                face: [],
                releselist: [],
                'params.pageNum': 1,
                showLoading: true,
                loading: false
            })
        } else {
            return
        }
        that.getMore(that.data.activeInd)
    },
    successFun7: function (res, selfObj, data) {
        wx.hideLoading();
        selfObj.setData({
            face: res.data
        })
    },
    goDetail(e) {
        let releaseId = e.currentTarget.dataset.id;
        let type = e.currentTarget.dataset.type;
        let url = e.currentTarget.dataset.url;
        if (type == 2) {
            wx.navigateTo({
                url: '/packageB/pages/detailZj/index?id=' + releaseId
            })
        } else {
            wx.navigateTo({
                url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=' + type + '&url=' + url
            })
        }
    },
    goPhotos: function () {
        this.isAuth();
        wx.navigateTo({
            url: '/pages/release/index?type=4'
        })
    },
    lookImg(e) {
        lookImgs(e)
        loadAgain = true
    },
    lookPhoto(e){
        wx.previewImage({
            current: e.currentTarget.dataset.src, // 当前显示图片的http链接
            urls: [e.currentTarget.dataset.src]
        })
    },
    lookCar(e) {
        let deleteFlag = this.data.userInfo.id === wx.getStorageSync('userInfo').userId ? true : false
        let type = e.currentTarget.dataset.type;
        if (!JSON.parse(deleteFlag)) {
            wx.showToast({
                icon: 'success',
                title: '仅主人可见',
            });
            return
        }
        wx.navigateTo({
            url: '/pages/chatList1/index?type=' + type + '&deleteFlag=' + deleteFlag + '&userId=' + this.data.userInfo.id
        })
    },
    failFun: function (res, selfObj) {
        console.log('failFun', res)
    },
    goEdit() {
        wx.navigateTo({
            url: '/pages/proInfor/index'
        })
    },
    goLookPhotos() {
        wx.navigateTo({
            url: '/packageB/pages/rank/index'
        })
    },

    onHide() {
        //wx.setStorageSync('headChage', null);
    },
    onLoad() {
        // if (!wx.getStorageSync('headChage')) {
        //     return;
        // }
        let that = this;
        let pages = getCurrentPages() //获取加载的页面
        let currentPage = pages[pages.length - 1] //获取当前页面的对象
        let userId = currentPage.options.userId //如果要获取url中所带的参数可以查看options
        let deleteFlag = JSON.parse(currentPage.options.deleteFlag)

        that.setData({
            deleteFlag: deleteFlag,
            userId: userId,
            releaseData: {}
        })
        let url = app.apiUrl + '/user/base_info';
        let params = {}
        if (userId && !deleteFlag) {
            that.setData({
                topList: ['动态', '组队'],
            })
            let url1 = app.apiUrl + '/user/base_info/' + userId;
            app.request.requestGetApi(url1, params, that, that.successFun, that.failFun, wx.getStorageSync('token'), {
                userId,
                deleteFlag
            })
            return;
        } else {
            that.setData({
                topList: ['动态', '组队'],
            })
         /*   that.setData({
                topList: ['动态', '表白墙', '颜值', '组队'],
            })*/
            app.request.requestGetApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'), {
                userId,
                deleteFlag
            })
        }

    },
    successFun: function (res, selfObj, data) {
        let avatarUrl = res.data.avatarUrl.indexOf('https') > -1 ? res.data.avatarUrl : app.apiUrlimg + res.data.avatarUrl
        selfObj.setData({
            userInfo: res.data,
            'userInfo.avatarUrl': avatarUrl
        });
        wx.setNavigationBarTitle({
            title: res.data.nickName
        });
        this.getList(data)
    },
    getList(data) {
        let that = this;
        let url = app.apiUrl + '/release/list/';
        let params = {
            pageNum: that.data.params.pageNum,
            pageSize: app.globalData.pageSize,
            queryMe: that.data.deleteFlag ? true : false,
            userId: that.data.deleteFlag ? '' : that.data.userId,
            topicTypeId: '',
            // schoolId: wx.getStorageSync('schoolId'),
        };
        app.request.requestGetApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
    },
    isAuth() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
    },
    zan(e) {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            let that = this;
            let type = e.currentTarget.dataset.type;
            let typeId = e.currentTarget.dataset.typeid;
            let deleteFlag = e.currentTarget.dataset.deleteFlag;
            let likeFlag = e.currentTarget.dataset.likeflag;
            var params = {
                status: likeFlag ? false : true,
                type: type,
                typeId: typeId,
            };
            let url = app.apiUrl + '/user/like';
            app.request.requestPostApi(url, params, that, that.successFun4, that.failFun, wx.getStorageSync('token'), {
                type: type,
                typeId: typeId,
                status: !likeFlag
            })
        }

    },
    successFun4: function (res, selfObj, data) {
        selfObj.data.releselist && selfObj.data.releselist.forEach(item => {
            if (item.releaseId == data.typeId || item.confessId == data.typeId) {
                item.likeFlag = data.status;
                item.likes = data.status ? item.likes + 1 : item.likes - 1
            }
            return selfObj.data.releselist
        });
        selfObj.setData({
            releselist: selfObj.data.releselist,
        })
    },
    successFun1: function (res, selfObj) {
        wx.hideLoading();
        let that = this;
        var content = [];
        if (that.data.topList[that.data.activeInd] == '颜值') {
            res.data && res.data.forEach(item => {
                let d = null;
                if (item.releaseTime) {
                    d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
                }
                return item.releaseTime = diaplayTime(d);
            });
            content = that.data.releselist.concat(res.data);
            that.setData({
                face: res.data
            })
        } else {
            res.data.list && res.data.list.forEach(item => {
                let d = null;
                if (item.createTime) {
                    d = new Date(item.createTime.replace(/-/g, '/')).getTime();
                }
                if (item.releaseTime) {
                    d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
                }
                return item.releaseTime = diaplayTime(d);
            });
            content = that.data.releselist.concat(res.data.list)
        }
        //var content = that.data.releselist.concat(res.data.list)
        selfObj.setData({
            releaseData: res.data,
            releselist: content,
            showLoading: false,
            loading: false
        });
    },
    delete(e) {
        let type = e.currentTarget.dataset.type;
        let id = e.currentTarget.dataset.id;
        let deleteFlag = e.currentTarget.dataset.deleteflag
        this.setData({
            show1: true,
            deleteData: {
                type: type,
                id: id,
                deleteFlag: deleteFlag
            }
        })
    },
    listChange(e) {
        debugger
        console.log(e.detail)
        let type = e.detail.type;
        let id = e.detail.id;
        let that = this;
        let newList = that.data.releselist.filter(function (item) {
            if (type == 1) {
                return item.releaseId != id
            }
            if (type == 2) {
                return item.confessId != id
            }
            if (type == 3) {
                return item.faceId != id
            }
            if (type == 4) {
                return item.teamId != id
            }
        })
        that.setData({
            releselist: newList,
            show1: false,
            deleteType: true
        });
    }
})
