// pages/list/list.js


/**
 * 聊天页面
 */
Page({

    /**
     * 页面的初始数据
     */
    data: {
        socketOpen:false,
        _msgPool: [],
        inputType: '',
        inputValueEventTemp: '',
        extraObj: {
            chatInputShowExtra: false,
            chatInputExtraArr: [
                {picName: 'choose_picture', description: '照片'},
                {picName: 'take_photos', description: '拍摄'}
            ]
        },
        textMessage: '',
        chatItems: [],
        latestPlayVoicePath: '',
        chatStatue: 'open',
        extraArr: [{
            picName: 'choose_picture',
            description: '照片'
        }, {
            picName: 'take_photos',
            description: '拍摄'
        }, {
            picName: 'close_chat',
            description: '自定义功能'
        }],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },
    onUnload() {
        const socketOpen = this.data.socketOpen;
        if (socketOpen) {
            wx.closeSocket({});
            wx.onSocketClose(res => {
                console.log('WebSocket 已关闭！')
            });
        }
    },
    connect(){
        debugger
        let that = this;
        let pages = getCurrentPages() //获取加载的页面
        let currentPage = pages[pages.length - 1] //获取当前页面的对象
        let friend = currentPage.options.friend //如果要获取url中所带的参数可以查看options
        let senderUserId = currentPage.options.senderUserId;
        let receiverUserId = currentPage.options.receiverUserId;
        let friendHeadUrl=currentPage.options.avatarUrl;
        this.setData({
            pageHeight: wx.getSystemInfoSync().windowHeight,
        });
        wx.setNavigationBarTitle({
            title: friend || ''
        });
        wx.connectSocket({
            url: 'ws://47.104.240.210:30007/xnxw/websocket/' + senderUserId + '/' + receiverUserId,
            header: {
                'content-type': 'application/json'
            },
            success: function () {
                that.setData({ socketOpen: true });
                console.log('信道连接成功~')
            },

            fail: function (err) {
                wx.showToast({
                    title: '网络异常！',
                })
                console.log(err)
            }
        });
        wx.onSocketOpen((res) => {
            console.log('WebSocket连接已打开！');
        });



        wx.onSocketMessage((res) => {
            console.log(2222)
            let msg = JSON.parse(res.data);
            let sendMsg = {
                type: 'text',
                isMy:false,
                msgUserId: 1123213,//这条消息的拥有者是my
                msgUserName: 1123213,
                msgUserHeadUrl: 111,
                friendId: 1123213,
                friendHeadUrl: friendHeadUrl,
                friendName: 1123213,
                timestamp: 1123213,
                duration: 1123213,
                content: msg.content
            }
            that.updateViewWhenSendSuccess(sendMsg)
        })

        wx.onSocketError((res) => {
            console.log('WebSocket连接打开失败，请检查！', res);
        });
    },
    onShow(){
       this.connect();
    },
    _sendMsgImp(content) {
        debugger
        if (this.data.socketOpen) {
            wx.sendSocketMessage({
                data: JSON.stringify(content),
                success: (res) => {
                    debugger
                    let sendMsg = {
                        type: 'text',
                        isMy:true,
                        msgUserId: 1123213,//这条消息的拥有者是my
                        msgUserName: 1123213,
                        msgUserHeadUrl: wx.getStorageSync('userInfo').avatarUrl,
                        friendId: 1123213,
                        friendHeadUrl: 1123213,
                        friendName: 1123213,
                        timestamp: 1123213,
                        duration: 1123213,
                        content: content.content
                    }
                    this.updateViewWhenSendSuccess(sendMsg)
                },
                fail: (res) => {
                    console.log(22)
                }
            })
        }
    },
    updateViewWhenSendSuccess(sendMsg) {
        debugger
        console.log('发送成功', sendMsg);
        let that = this;
        that.data.chatItems.push(sendMsg)
        that.setData({
            chatItems:that.data.chatItems
        });
    },

    /*聊天*/
    _chatInput$bind$focus$event() {
        this.setData({
            inputType: 'text'
        })
    },
    _chatInput$extra$click$event() {
        const isShow = !this.data.extraObj.chatInputShowExtra;
        this.setData({
            'extraObj.chatInputShowExtra': isShow
        });
    },
    _chatInput$send$text$message02() {
        this.setData({
            textMessage: '',
            inputType: 'none'
        }, () => {
            if (!!this.data.inputValueEventTemp) {
                this._sendMsgImp({content: this.data.inputValueEventTemp});
                this.data.inputValueEventTemp = '';
            }
        });
    },
    _chatInput$bind$blur$event() {
        setTimeout(() => {
            if (!this.data.inputValueEventTemp) {
                this.data.inputValueEventTemp = '';
                this.setData({
                    inputType: 'none',
                });
            }
            this.setData({
                'extraObj.chatInputShowExtra': false
            });
        });
    },
    _chatInput$send$text$message(e) {
        this.setData({
            textMessage: ''
        }, () => {
            debugger
            let content = e.detail.value;
            this._sendMsgImp({content});
            this.data.inputValueEventTemp = '';
        });
    },
    _chatInput$getValue$event(e) {
        debugger
        const {detail: {value: textMessage}} = e;
        this.data.inputValueEventTemp = textMessage;
        this.setData({
            textMessage
        })
    },

    /**
     * 关闭webSocket
     */


    /*聊天E*/
    onReady() {
        this.chatInput = this.selectComponent('#chatInput');
    },
    onSendMessageEvent(e) {
        let content = e.detail.value;
        this.sendMsg({type: IMOperator.TextType, content});
    },

    /**
     * 点击extra中的item时触发
     * @param e
     */
    onExtraItemClickEvent(e) {
        console.warn(e);
        let chooseIndex = parseInt(e.detail.index);
        if (chooseIndex === 2) {
            this.myFun();
            return;
        }
        wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['compressed'],
            sourceType: chooseIndex === 0 ? ['album'] : ['camera'],
            success: (res) => {
                this.sendMsg({type: IMOperator.ImageType, content: res.tempFilePaths[0]})
            }
        });
    },
    /**
     * 点击extra按钮时触发
     * @param e
     */
    onExtraClickEvent(e) {
        console.log(e);
    },
    //模拟上传文件，注意这里的cbOk回调函数传入的参数应该是上传文件成功时返回的文件url，这里因为模拟，我直接用的savedFilePath
    simulateUploadFile({savedFilePath, duration, itemIndex}) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                let urlFromServerWhenUploadSuccess = savedFilePath;
                resolve({url: urlFromServerWhenUploadSuccess});
            }, 1000);
        });
    },

    /**
     * 自定义事件
     */
    myFun() {
        wx.showModal({
            title: '小贴士',
            content: '演示更新会话状态',
            confirmText: '确认',
            showCancel: true,
            success: (res) => {
                if (res.confirm) {
                    this.msgManager.sendMsg({type: IMOperator.CustomType})
                }
            }
        })
    },

    resetInputStatus() {
        //this.chatInput.closeExtraView();
    },

    onUnload() {

    },

    async sendMsg({content, itemIndex}) {
        try {
            const {msg} = await this.imOperator.onSimulateSendMsg({content})
            this.UI.updateViewWhenSendSuccess(msg, itemIndex);
            return {msg};
        } catch (e) {
            console.error(e);
            this.UI.updateViewWhenSendFailed(itemIndex);
        }
    },
    /**
     * 重发消息
     * @param e
     */
    resendMsgEvent(e) {
        const itemIndex = parseInt(e.currentTarget.dataset.resendIndex);
        const item = this.data.chatItems[itemIndex];
        this.UI.updateDataWhenStartSending(item, false, false);
        this.msgManager.resend({...item, itemIndex});
    },
});
