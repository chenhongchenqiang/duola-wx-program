const app = getApp()
let pageStart = 1;
var start;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        hotList: [],
        mesData:{},
        requesting: false,
        end: false,
        emptyShow: false,
        page: pageStart,
        listData: [],
    },
    goBack() {
        wx.navigateBack()
    },
    getList(type, currentPage) {
      debugger
      this.setData({
        requesting: true
      })
      app.httpGet({
        url: `/message/list?pageNum=${currentPage}`,token:wx.getStorageSync('token')
      }).then((res) => {
        if(currentPage==1&&res.data.list.length==0){
          this.setData({
            emptyShow: true
          })
        }
        this.setData({
          requesting: false
        })
        if (type === 'refresh') {
          this.setData({
            hotList: res.data.list,
            page: currentPage + 1,
            end: res.data.pages==currentPage
          })
        } else {
          this.setData({
            hotList: this.data.hotList.concat( res.data.list),
            page: currentPage + 1,
            end: res.data.pages==currentPage
          })
        }
      })
    },
    // 刷新数据
    refresh() {
      this.getList('refresh', pageStart);
      this.setData({
        empty: false
      })
    },
    // 加载更多
    more() {
      this.getList('more', this.data.page);
    },
    onLoad() {
      this.getList('refresh', pageStart);
    },
  listen(e) {
    let that = this;
    let obj = e.currentTarget.dataset.item;
    var params = {
      status: 0,
      eventId: parseInt(obj.eventId),
      userId:'',
    };
    let url = app.apiUrl + '/message/listen';
    if (!wx.getStorageSync('token')) {
      wx.navigateTo({
        url: '/pages/author/index'
      })
      return
    } else {
      app.request.requestPostApi(url, params, that, that.successFunListen, that.failFun, wx.getStorageSync('token'), {
        eventId: obj.eventId,
      })
    }
  },
  successFunListen: function (res, selfObj, data) {
    let that=this;
    let newList = that.data.hotList.filter(function (item) {
      return item.eventId != data.eventId
    })
    wx.setStorageSync('releaseId2', data.eventId)
    wx.setStorageSync('listenFlag',  false)
    that.setData({
      hotList: newList,
    });
  },
  

    successFunHot: function (res, selfObj) {
      let content = selfObj.data.hotList.concat(res.data.list);
        selfObj.setData({
          hotList: content,
          mesData:res.data,
          showLoading: false,
          loading: false
        })
    },

 
    goHotDetail(e) {
        let releaseId = e.currentTarget.dataset.item.eventId;
        let radio;
        if (e.currentTarget.dataset.item.releaseFiles[0]) {
            radio = e.currentTarget.dataset.item.releaseFiles[0].width / e.currentTarget.dataset.item.releaseFiles[0].height;
        }
        wx.navigateTo({
            url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=0'
        })

    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },


})