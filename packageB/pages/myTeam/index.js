//index.js
//获取应用实例
const app = getApp()

import {diaplayTime} from '../../../utils/util';

Page({
    data: {
        showSingUp: false,
        params: {
            pageNum: 1,
            queryMe: false,
            schoolId: '',
            userId: ''
        },
        teamList: [],
        request: {
            phone: '',
            remark: '',
            teamId: '',
            userName: '',
            wechat: ''
        },
        teamData: {},
        list: [],
        navbarHeight: 0,
        activeId: 0,
        load: true,
      topicTypeId:'',
      showLoading:true,
      loading:false
    },
    onLoad() {
        let that = this;
        let url = app.apiUrl + '/index/topic_type_list';
        let params = {type: 1};
        app.request.requestGetApi(url, params, that, that.successFun2, that.failFun)
    },
    onShow: function () {
        let that = this;
        if (typeof this.getTabBar === 'function' &&
            that.getTabBar()) {
            that.getTabBar().setData({
                selected: 3
            })
        }
        if (app.globalData.release || wx.getStorageSync('teamChanged')) {
            wx.pageScrollTo({
                scrollTop: 0,
            })
            that.setData({
                teamData: {},
                list: [],
                navbarHeight: app.globalData.navbarHeight,
                'params.pageNum': 1,
                isIphoneX: app.globalData.isIphoneX
            });
            that.commonList()
        }
    },
    onHide() {
        app.globalData.release = false;
    },
    onReachBottom: function () {
        let that = this;
        if (that.data.load) {
            if (that.data.params.pageNum < that.data.teamData.pages) {
                that.setData({
                    'params.pageNum': that.data.params.pageNum * 1 + 1,
                  loading:true
                })
                that.loadMore();
            }
        }
    },
    loadMore() {
        let that = this;
        that.commonList()
    },
    onPullDownRefresh() {
        var that = this;
        that.setData({
            'params.pageNum':1,
        })
        wx.showNavigationBarLoading() //在标题栏中显示加载
        that.commonList(that.data.topicTypeId)
    },
    successFun3: function (res, selfObj) {
        res.data.list && res.data.list.forEach(item => {
            let d = new Date(item.createTime.replace(/-/g, '/')).getTime();
            item.teamUsers&&item.teamUsers.forEach(good => {
                good.avatarUrl=good.avatarUrl.indexOf('https')>-1?good.avatarUrl:app.apiUrlimg+good.avatarUrl
            });
            item.createTime = diaplayTime(d);
        });
        selfObj.setData({
            teamData: res.data,
            list: res.data.list
        })
    },
    commonList(id) {
        let that = this;
        let topicTypeId = id ? id : '';
        app.httpGet({
            url: '/team/join_list?topicTypeId=' + topicTypeId + '&pageNum=' + that.data.params.pageNum + '&pageSize=' + app.globalData.pageSize + '&schoolId=' + wx.getStorageSync('schoolId'),
            token: wx.getStorageSync('token'),
            loading: true
        }).then((res) => {
            debugger
            wx.hideNavigationBarLoading() //完成停止加载
            wx.stopPullDownRefresh() //停止下拉刷新
            that.successFun(res)
        })
    },
    

    choiceTeam(e) {
        let index = e.currentTarget.dataset.index;
        let id = e.currentTarget.dataset.id;
      if (this.data.activeId==index) return;
        this.setData({
            activeId: index,
            teamData: {},
            list: [],
          topicTypeId:id,
            'params.pageNum':1,
          showLoading:true
        })
        this.commonList(id)
    },
    successFun2: function (res, selfObj) {
        selfObj.setData({
            teamList:[{id:'',name:'全部'}] .concat(res.data),
        });
      if (!app.globalData.release || !wx.getStorageSync('teamChanged')) {
        selfObj.commonList()
      }
    },
    successFun: function (res) {
        debugger
        let that=this;
        res.data.list && res.data.list.forEach(item => {
            let d = new Date(item.createTime.replace(/-/g, '/')).getTime();
            item.createTime = diaplayTime(d);
            item.teamUsers && item.teamUsers.forEach(good => {
                good.avatarUrl = good.avatarUrl.indexOf('https') > -1 ? good.avatarUrl : app.apiUrlimg + good.avatarUrl
            });
            item.process = (Math.round(item.curren / (item.left + item.current) * 10000) / 100.00) + "%"
        });
        let content = [];
        if(that.data.params.pageNum==1){
            content = res.data.list
        }else{
            content = that.data.list.concat(res.data.list)
        }
       
        that.setData({
            teamData: res.data,
            list: content,
            showLoading: false,
            loading: false
        })
        wx.setStorageSync('teamChanged', false);
    },
   
    goDetail(e) {
        let id=e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/packageB/pages/detailZj/index?id='+id
        })
    },
    startTeam() {
        wx.navigateTo({
            url: '/pages/release/index?type=2'
        })
    },
    myTeam() {
        wx.navigateTo({
            url: '/packageB/pages/myTeam/index'
        })
    },
    singUp(e) {
        this.setData({
            showSingUp: true,
            'request.teamId': e.currentTarget.dataset.teamid
        })
    },
    bindKeyInput: function (e) {
        let type = e.target.dataset.type;
        if (type == 1)
            this.setData({
                'request.userName': e.detail.value
            })
        if (type == 2)
            this.setData({
                'request.phone': e.detail.value
            })
        if (type == 3)
            this.setData({
                'request.wechat': e.detail.value
            })
        if (type == 4)
            this.setData({
                'request.remark': e.detail.value
            })

    },
    orderSign() {
        let that = this;
        let url = app.apiUrl + '/team/join';
        let params = that.data.request;
        app.request.requestPostApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
    },
    successFun1: function (res, selfObj) {
        if (res.code == 200) {
            wx.showToast({
                icon: 'success',
                title: '加入成功',
            })
        } else {
            wx.showToast({
                title: res.message,
            })
        }
    },
    addSure() {
        debugger
        console.log(111)
        wx.requestSubscribeMessage({
            tmplIds: app.globalData.template_id,
            success(res) {
                wx.showToast({
                    title: '订阅OK！',
                    duration: 1000,
                })
            }
        });

    },
    hide() {
        this.setData({
            showSingUp: false
        })
    }
});
