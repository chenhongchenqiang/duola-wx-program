//index.js
//获取应用实例
const app = getApp()
import {diaplayTime, lookImgs, tongZhi} from '../../../utils/util';

Page({
    data: {
        showSingUp: false,
        params: {
            pageNum: 1,
            queryMe: false,
            pageIndex: 1,
            schoolId: '',
            userId: ''
        },
        teamData: {},
        list: [],
        resh: false,
        navbarHeight: 0,
        load: true,
        sharData: {},
        show: false,
        show1: false,
        nickName: '',
        showLoading: true,
        loading: false
    },
    onShow: function () {
        let that = this;
        if (typeof this.getTabBar === 'function' &&
            that.getTabBar()) {
            that.getTabBar().setData({
                selected: 1
            })
        }
        if (wx.getStorageSync('confessId')) {
            that.data.list && that.data.list.forEach(item => {
                if (item.confessId == wx.getStorageSync('confessId')) {
                    if (app.globalData.commentId) {
                        let newList = item.comments.filter(function (good) {
                            return good.id != app.globalData.commentId
                        })
                        item.comments = newList
                        item.reviews = wx.getStorageSync('reviews');
                        ;
                    }
                    if (wx.getStorageSync('confessContent')) {
                        debugger
                        item.comments.unshift(app.globalData.confessContent)
                        item.reviews = wx.getStorageSync('reviews');
                        ;
                    }
                    if ((item.likeFlag && wx.getStorageSync('likeFlag')) || (!item.likeFlag && !wx.getStorageSync('likeFlag'))) {
                        return
                    }
                    item.likeFlag = wx.getStorageSync('likeFlag');
                    item.likes = wx.getStorageSync('likeFlag') ? item.likes + 1 : item.likes - 1
                }
                return that.data.list
            });
            that.setData({
                'list': that.data.list,
            })
        }
    },
    onReachBottom: function () {
        let that = this;
        if (that.data.load && !that.data.resh) {
            if (that.data.params.pageNum < that.data.teamData.pages) {
                that.setData({
                    'params.pageNum': that.data.params.pageNum * 1 + 1,
                    loading: true
                })
                that.loadMore();
            }
        }
    },
    onPullDownRefresh() {
        var that = this;
        that.setData({
            resh: true,
            list: [],
        });
        let data = {type: 1}
        if (!that.data.lodaing) {
            wx.showNavigationBarLoading() //在标题栏中显示加载
            setTimeout(function () {
                let url = app.apiUrl + '/confess/list';
                that.setData({
                    'params.pageNum': 1
                });
                app.request.requestGetApi(url, that.data.params, that, that.successFun, that.failFun, wx.getStorageSync('token'), data)
                wx.hideNavigationBarLoading() //完成停止加载
                wx.stopPullDownRefresh() //停止下拉刷新
            }, 1500);
        }
    },
    loadMore() {
        let that = this;
        that.commonList()
    },
    commonList() {
        let that = this;
        let url = app.apiUrl + '/confess/list';
        app.request.requestGetApi(url, that.data.params, that, that.successFun, that.failFun, wx.getStorageSync('token'))
    },
    onHide() {
        app.globalData.release = false;
        wx.setStorageSync('confessId', '');
        wx.setStorageSync('likeFlag', null);
        wx.setStorageSync('confessContent', '');
        wx.setStorageSync('reviews', '');
        app.globalData.confessContent = {},
            app.globalData.commentId = null
    },
    onLoad: function () {
        let that = this;
        that.setData({
            navbarHeight: app.globalData.navbarHeight
        });
        that.setData({
            'params.schoolId': wx.getStorageSync('schoolId')
        });
        that.commonList()
    },
    successFun: function (res, selfObj, data) {
        debugger
        wx.hideLoading();
        let that = this;
        res.data.list && res.data.list.forEach(item => {
            let d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
            item.releaseTime = diaplayTime(d);
            item.userAvatarUrl = item.userAvatarUrl.indexOf('https') > -1 ? item.userAvatarUrl : app.apiUrlimg + item.userAvatarUrl
            if (item.releaseFiles.length == 1) {
                item.releaseFiles.forEach(good => {
                    good.radio = good.width / good.height;
                });
            }
            /*  if(item.releaseFiles.length>0){
                  item.bgImg=app.apiUrlimg+item.releaseFiles[0].fileRelativePath;
              }*/
        });
        let content;
        if (data && data.type == 1) {
            content = res.data.list
        } else {
            content = that.data.list.concat(res.data.list)
        }
        selfObj.setData({
            teamData: res.data,
            list: content,
            showLoading: false,
            loading: false,
            resh: false
        })
    },
    lookImg(e) {
        lookImgs(e)
    },
    zan(e) {
        let that = this;
        let obj = e.currentTarget.dataset.item;
        let type = 2;
        let typeId = obj.confessId;
        let deleteFlag = obj.deleteFlag;
        let likeFlag = obj.likeFlag;
        let userId = obj.userId;
        let contentText = obj.contentText;
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
        if (wx.getStorageSync('userInfo')) {
            that.setData({
                nickName: wx.getStorageSync('userInfo').nickName
            })
        }
        ;
        var params = {
            status: likeFlag ? false : true,
            type: type,
            typeId: typeId,
        };
        let url = app.apiUrl + '/user/like';
        app.request.requestPostApi(url, params, that, that.successFun4, that.failFun, wx.getStorageSync('token'), {
            type: type,
            typeId: typeId,
            status: !likeFlag,
            userId: userId,
            contentText: contentText,
            nickName: that.data.nickName,
            confess: true
        })
    },
    successFun4: function (res, selfObj, data) {
        selfObj.data.list && selfObj.data.list.forEach(item => {
            if (item.confessId == data.typeId) {
                item.likeFlag = data.status;
                item.likes = data.status ? item.likes + 1 : item.likes - 1
            }
            return selfObj.data.list
        });
        selfObj.setData({
            list: selfObj.data.list,
        })
        if (data.status) {
            selfObj.tongZhi(data)
        }
    },
    successFun10(res, selfObj, data) {
        tongZhi(res, selfObj, data)
    },
    tongZhi(data) {
        let that = this;
        let url = app.apiUrl + '/index/get_openid/' + data.userId;
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFun10, that.failFun, wx.getStorageSync('token'), data)
    },
    goUserDetail(e) {
        let userId = e.currentTarget.dataset.userid;
        let deleteFlag = e.currentTarget.dataset.deleteflag;
        wx.navigateTo({
            url: '/packageB/pages/logs/logs?userId=' + userId + '&deleteFlag=' + deleteFlag
        })
    },
    share(e) {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            let sharData = e.currentTarget.dataset.item;
            this.setData({
                show: true,
                sharData: sharData
            })
        }
    },

    onShareAppMessage: function onShareAppMessage(res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        let pic;

        if (this.data.sharData.releaseFiles.length > 0) {
            if (this.data.sharData.releaseFiles[0].coverRelativePath) {
                pic = app.apiUrlimg + this.data.sharData.releaseFiles[0].coverRelativePath;
            } else {
                pic = app.apiUrlimg + this.data.sharData.releaseFiles[0].fileRelativePath;
            }
        }
        this.setData({
            show: false
        })
        return {
            title: this.data.sharData.contentText,
            path: this.data.sharData.releaseFiles[0] && this.data.sharData.releaseFiles[0].radio > 1 && this.data.sharData.releaseFiles[0].bizFileType == 2 ? '/packageB/pages/videoDetail/index?id=' + this.data.sharData.confessId + '&type=1&sence=1' : '/packageB/pages/dynamicDetail/index?id=' + this.data.sharData.confessId + '&type=1&sence=1',
            imageUrl: pic ? pic : '../../../images/home/logo.png',
            success: function (res) {
                // 转发成功之后的回调
                if (res.errMsg == 'shareAppMessage:ok') {
                    console.log(1)
                }
            },
        }
    },

    isAuth() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
    },

    bindTouchStart: function (e) {
        this.startTime = e.timeStamp;
    },
    bindTouchEnd: function (e) {
        this.endTime = e.timeStamp;
    },
    bingLongTap: function (e) {
        console.log("长按");
        wx.setClipboardData({
            data: e.currentTarget.dataset.txt,
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                });
            }
        });
    },
    goConfessIonDe(e) {
        if (this.endTime - this.startTime < 350) {
            let confessId = e.currentTarget.dataset.confessid;
            let bizFileType = e.currentTarget.dataset.bizfiletype;
            let radio = e.currentTarget.dataset.radio;
            wx.navigateTo({
                url: '/packageB/pages/dynamicDetail/index?id=' + confessId + '&type=1'
            })

        }
    },
    goPhotos() {
        wx.navigateTo({
            url: '/pages/photos/index'
        })
    },
    addSure() {
        debugger
        console.log(111)
        wx.requestSubscribeMessage({
            tmplIds: app.globalData.template_id,
            success(res) {
                wx.showToast({
                    title: '订阅OK！',
                    duration: 1000,
                })
            }
        });

    },
    delete(e) {
        let type = e.currentTarget.dataset.type;
        let id = e.currentTarget.dataset.id;
        let deleteFlag = e.currentTarget.dataset.deleteflag
        this.setData({
            show1: true,
            deleteData: {
                type: type,
                id: id,
                deleteFlag: deleteFlag
            }
        })
    },
    send() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
        } else {
            wx.navigateTo({
                url: '/pages/release/index?type=' + 3
            })
        }

    },
});
