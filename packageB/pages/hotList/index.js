const app = getApp()

var start;
Page({

    /**
     * 页面的初始数据
     */
    data: {
      hotList:[]
    },
    goBack(){
      wx.navigateBack()
    },
    onLoad: function (options) {
        let that = this;
        let url = app.apiUrl + '/release/hot_post_list/' + wx.getStorageSync('schoolId');
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFunHot, that.failFun)
    },
    successFunHot: function (res, selfObj) {
      let that = this;
      selfObj.setData({
        hotList: res.data,
      })
    },
  goHotDetail(e) {
    debugger
    let releaseId = e.currentTarget.dataset.id;
    let userType = e.currentTarget.dataset.usertype;
    let radio;
    if (e.currentTarget.dataset.item.releaseFiles[0]){
      radio = e.currentTarget.dataset.item.releaseFiles[0].width / e.currentTarget.dataset.item.releaseFiles[0].height;
    }
      wx.navigateTo({
          url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=0'
      })
   

  },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
       
    },


})