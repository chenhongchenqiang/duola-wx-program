const app = getApp();
let loadAgain;
import {diaplayTime, lookImgs, tongZhi} from '../../../utils/util';

Page({
    data: {
        pageIndex: 1,
        load: true,
        releaseId: null,
        comment: [],
        commentData: {},
        basicInfo: {},
        likeStatus: false,
        auto_height: true,
        fixHeight: '',
        height: 0,
        isInput: false,
        show: false,
        content: '',
        focusInput: false,
        userId2: '',
        nickName2: '',
        placeholdertxt: '写评论',
        isMask: false,
        topicType: null,
        sence: 0,
        show1: false,
        play: false,
        nickName: '',
        basicInfoNoContent: false,
        loading: false,
        id: '',
        type: '',
        share: false,
        sendImg: '',
        fullScreen:1
    },
    goElem: function (e) {
        //淘宝联盟   'pages/sharePid/web/index?scene=s.click.ele.me%2FPujGstu'
        //饿了吗   'ele-recommend-price/pages/guest/index?inviter=5c9ffba1'
        this.setData({
            fullScreen: 0,
        })
        let link=e.currentTarget.dataset.link
        wx.navigateToMiniProgram({
            appId: 'wxece3a9a4c82f58c9',
            path:link,
            envVersion: 'release',
            success(res) {
                // 打开成功
            }
        })
    },
    goMeituan: function (e) {
        let link=e.currentTarget.dataset.link
        wx.navigateToMiniProgram({
            appId: 'wx2c348cf579062e56',
            path:link,
            // path:'outer_packages/r2xinvite/coupon/coupon?inviteCode=NnOIp-QOs8SiYF1dcSlL5r8phPrCf6qkH7evMyjIoup2NXxNCLYcBbd3bqpv2X2Ir6i4KKgJoM5qmZ9vqVG3uYTD3lJahFVRoSB0MTvfl2ggH54Om07Oo5SfNlPz0efI0Cx2SeC6bskw8_hcullip1qpBYEo-dhVftXalUfOjX0',
            envVersion: 'release',
            success(res) {
                // 打开成功
            }
        })
    },
    choseImg() {
        var _this = this;
        wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['original', 'compressed'],
            // 指定是原图还是压缩图，默认两个都有
            sourceType: ['album', 'camera'],
            // 指定来源是相册还是相机，默认两个都有
            success: function (res) {
                debugger
                // 返回选定照片的本地文件路径tempFilePath可以作为img标签的src属性显示图片
                //然后请求接口把图片传给后端存到服务器即可
                _this.setData({
                    sendImg: res.tempFilePaths[0],
                    loadAgin: true
                })
            }
        })
    },
    bindTouchStart: function (e) {
        this.startTime = e.timeStamp;
    },
    bindTouchEnd: function (e) {
        this.endTime = e.timeStamp;
    },
    bingLongTap: function (e) {
        console.log("长按");
        wx.setClipboardData({
            data: e.currentTarget.dataset.txt,
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                });
            }
        });
    },

    onLoad(query) {
        debugger
        this.commonList()
        // if (query && !query.id) {
        //     const querAll = decodeURIComponent(query.scene);
        //     let scene = {
        //         id: querAll.match(/(\S*)&/)[1],
        //         type: querAll.match(/&(\S*)/)[1],
        //     }
        //     this.setData({
        //         share: true,
        //         id: scene.id,
        //         type: scene.type,
        //     })
        //     this.commonList('', scene)
        // } else {
        //     this.commonList()
        // }
        this.setData({
            urlType: query.url
        })
    },
    onUnload() {
        debugger
        loadAgain = false
    },
    onHide() {
        debugger
        loadAgain = true
    },
    onShow() {
        let nowTime=new Date().getTime();
        let beforeTime=wx.getStorageSync('beforeTime')||0;
        let time=nowTime-beforeTime
        this.setData({
            isIphoneX: app.globalData.isIphoneX
        })
        if(time>=21600000){
            wx.setStorageSync('beforeTime',nowTime);
            this.setData({
                fullScreen: 1,
            })
        }else{
            this.setData({
                fullScreen: 0,
            })
        }
        this.getBanner();
       
        // if (loadAgain&&this.data.urlType!=1) {
        //     loadAgain = false;
        //     return;
        // } else {
        //     this.commonList()
        // }
    },

    getBanner() {
        let that = this;
        let url = app.apiUrl + '/index/banner';
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFunBanner, that.failFun)
    },
    hideScreen() {
        this.setData({
            fullScreen: 0,
        })
     
    },
    successFunBanner: function (res, selfObj) {
        selfObj.setData({
            banner: res.data,
        })
    },
  
    keyboardHeightChange() {
        wx.onKeyboardHeightChange(res => {
            const height = res.height;
            if(height!=0){
                this.setData({
                    isMask:true
                });
            }
            this.setData({
                height: height,
            });
        })
    },
    copy: function (e) {
        console.log("长按");
        wx.setClipboardData({
            data: e.currentTarget.dataset.txt,
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                });
            }
        });
    },
    takeCare() {

        let url = app.apiUrl + '/user/follow';
        let that = this;
        var params = {
            status: 1,
            userId2: that.data.basicInfo.userId,
        };
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
        } else {
            app.request.requestPostApi(url, params, that, that.successFun6, that.failFun, wx.getStorageSync('token'))
        }
    },
    successFun6() {
        wx.showToast({
            icon: 'success',
            title: '关注成功',
        });
        this.setData({
            'basicInfo.followFlag': true
        });
        console.log(111)
    },
    onReachBottom: function () {
        var that = this;
        if (that.data.load) {
            if (that.data.pageIndex < that.data.commentData.pages) {
                that.setData({
                    pageIndex: that.data.pageIndex * 1 + 1,
                })
                if (that.data.share) {
                    that.loadMore('more', {id: that.data.id, type: that.data.type});
                } else {
                    that.loadMore('more');
                }
            }
        }
    },
    loadMore(type1, obj) {
        let that = this;
        that.commonList(type1, obj)
    },
    commonList(type1, query) {
        wx.showLoading({
            mask: true
        })
        let that = this;
        let pages = getCurrentPages() //获取加载的页面
        let currentPage = pages[pages.length - 1] //获取当前页面的对象
        let releaseId = query == undefined && currentPage.options.id ? parseInt(currentPage.options.id) : query != undefined && query.id ? query.id : ''//如果要获取url中所带的参数可以查看
        let type = query == undefined && currentPage.options.type ? parseInt(currentPage.options.type) : query != undefined && query.type ? query.type : ''
        let sence = parseInt(currentPage.options.sence);
        console.log(sence)
        console.log(releaseId)
        that.setData({
            topicType: type,
            sence: sence,
            MyUserId: sence ? '' : wx.getStorageSync('userInfo').userId
        });

        if (releaseId) {
            if (type1 != 'more') {
                let url1 = type == 1 ? app.apiUrl + '/confess/detail/' + releaseId : app.apiUrl + '/release/detail/' + releaseId;
                app.request.requestGetApi(url1, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
            }

            var params = {
                pageNum: that.data.pageIndex,
                pageSize: app.globalData.pageSize,
                releaseId: releaseId,
                commentType: type,
            };
            let url = app.apiUrl + '/release/comment_list/';
            app.request.requestGetApi(url, params, that, that.successFun5, that.failFun, wx.getStorageSync('token'))
        }
    },
    successFun5: function (res, selfObj) {
        let that = this;
        res.data.list && res.data.list.forEach(item => {
            let d = new Date(item.createTime.replace(/-/g, '/')).getTime();
            item.createTime = diaplayTime(d);
            item.content = item.content && item.content.replace(/\n|\r/g, "");
            item.avatarUrl = item.avatarUrl.indexOf('https') > -1 ? item.avatarUrl : app.apiUrlimg + item.avatarUrl;
            if (item.childrens.length > 0) {
                item.childrens.forEach(items => {
                    let time = new Date(items.createTime.replace(/-/g, '/')).getTime();
                    items.createTime = diaplayTime(time);
                    items.content = items.content && items.content.replace(/\n|\r/g, "");
                    items.avatarUrl = items.avatarUrl.indexOf('https') > -1 ? items.avatarUrl : app.apiUrlimg + items.avatarUrl;
                })
            }
        });
        var content
        if (that.data.pageIndex == 1) {
            content = res.data.list
        } else {
            content = that.data.comment.concat(res.data.list)
        }

        selfObj.setData({
            comment: content,
            totalPage: res.data.pages,
            commentData: res.data,
            loading: true
        });
        wx.hideLoading();
    },
    lookMore() {
        debugger
        wx.navigateTo({
            url: '/packageB/pages/zanList/index?obj=' + JSON.stringify(this.data.basicInfo.likeUsers),
        })
    },
    goRelease1(e) {
        debugger
        if (this.endTime - this.startTime < 350) {
            this.setData({
                commentId: e.currentTarget.dataset.commentid,
                focusInput: true,
                userId2: e.currentTarget.dataset.userid,
                nickName2: e.currentTarget.dataset.nickname,
                placeholdertxt: '回复' + e.currentTarget.dataset.nickname,
            })
        }
    },


    goRelease() {

        let that = this;
        let userId = that.data.userId2 ? that.data.userId2 : '';
        let releaseId = that.data.basicInfo.releaseId || that.data.basicInfo.confessId;
        // let url = app.apiUrl + '/release/comment';
        let url = app.apiUrl + '/release/submit_comment';
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        if (that.data.content.length == 0&&!that.data.sendImg) {
            that.setData({
                mask: true
            });
            wx.showToast({
                title: "评论不能为空",
                icon: 'none',
                duration: 2000
            });
            return
        }
        wx.showLoading({
            mask:true
        })
        if (that.data.sendImg) {
            wx.uploadFile({
                url: app.apiUrl2 + '/document/upload/1/1',
                filePath: that.data.sendImg,
                name: 'fileData',
                header: {
                    "X-Xnxw-Token": wx.getStorageSync('token'),
                },
                success: function (res) {
                    if (JSON.parse(res.data).code == 400) {
                        wx.showToast({
                            title: JSON.parse(res.data).message,
                            icon: 'none'
                        })
                        return
                    } else {
                        that.setData({
                            sendImg: (JSON.parse(res.data).data)[0].relativePath
                        })
                        that.subCommon(url,releaseId,userId);
                    }
                },

                fail: function (res) {
                    return
                    wx.showModal({
                        title: '错误提示',
                        content: '上传图片失败',
                        showCancel: false,
                        success: function (res) {
                        }
                    })
                }
            });
        }else{
            that.subCommon(url,releaseId,userId);
        }

    },
    subCommon(url,releaseId,userId) {
        wx.hideLoading();
        let that = this;
        let params = {
            commentId: that.data.commentId ? parseInt(that.data.commentId) : null,
            commentType: that.data.type ? parseInt(that.data.type) : that.data.topicType,
            picPath: that.data.sendImg,
            content: that.data.content,
            releaseId: releaseId,
            userId: userId,
        };
        app.request.requestPostApi(url, params, that, that.successFun4, that.failFun, wx.getStorageSync('token'))
    },
    successFun4: function (res, selfObj) {
        if (wx.getStorageSync('userInfo')) {
            this.setData({
                nickName: wx.getStorageSync('userInfo').nickName
            })
        }
        ;
        if (res.code === 200) {
            let that = this;
            wx.showToast({
                icon: 'success',
                title: '评论成功',
            })
            let data = {
                avatarUrl: selfObj.data.basicInfo.fakeFlag?res.data.fakeAvatarUrl:wx.getStorageSync('userInfo').avatarUrl.indexOf('https') > -1 || wx.getStorageSync('userInfo').avatarUrl.indexOf('http') > -1 ? wx.getStorageSync('userInfo').avatarUrl : app.apiUrlimg + wx.getStorageSync('userInfo').avatarUrl,
                avatarUrl2: "",
                content: selfObj.data.content?selfObj.data.content:'图片评论',
                childrens: [],
                createTime: "刚刚",
                deleteFlag: true,
                id: res.data.commentId,
                likeFlag: false,
                likes: 0,
                nickName: selfObj.data.basicInfo.fakeFlag?res.data.fakeName:wx.getStorageSync('userInfo').nickName,
                nickName2: selfObj.data.nickName2,
                picPath: selfObj.data.sendImg,
                userId: wx.getStorageSync('userInfo').userId,
                userId2: selfObj.data.userId2,
            };
            //selfObj.data.comment.unshift(data);
            if (that.data.commentId) {
                that.data.comment.forEach((item, index) => {
                    debugger
                    if (item.id == that.data.commentId) {
                        item.childrens.unshift(data)
                    }
                })
                that.setData({
                    commentId: null
                })
            } else {
                that.data.comment.unshift(data);
            }

            selfObj.setData({
                comment: that.data.comment,
                content: '',
                sendImg:'',
                nickName: wx.getStorageSync('userInfo').nickName,
                userId: '',
                placeholdertxt: '写评论',
                nickName2: '',
                'basicInfo.reviews': parseInt(that.data.basicInfo.reviews) + 1,
            })
            //评论通知
            if (that.data.basicInfo.releaseId) {
                debugger
                wx.setStorageSync('releaseId', that.data.basicInfo.releaseId)
                wx.setStorageSync('reviews', that.data.basicInfo.reviews)
                that.tongZhi({
                    userId: that.data.userId2 || that.data.basicInfo.userId,
                    typeId: that.data.basicInfo.releaseId,
                    confessContent: data,
                    contentText: that.data.basicInfo.contentText,
                    nickName: that.data.nickName
                })
            }
            if (that.data.basicInfo.confessId) {
                wx.setStorageSync('confessId', that.data.basicInfo.confessId)
                wx.setStorageSync('reviews', that.data.basicInfo.reviews)
                that.tongZhi({
                    userId: that.data.userId2 || that.data.basicInfo.userId,
                    typeId: that.data.basicInfo.confessId,
                    confessContent: data,
                    contentText: that.data.basicInfo.contentText,
                    nickName: that.data.nickName,
                    confess: true
                })
            }
            wx.setStorageSync('likeFlag', that.data.basicInfo.likeFlag)
            app.globalData.confessContent = that.data.comment.slice(0, 3);
        } else {
            wx.showToast({
                title: res.errMsg,
            })
        }
        wx.requestSubscribeMessage({
            tmplIds: app.globalData.template_id,
        });
    },
    onShareAppMessage: function onShareAppMessage(res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        let pic;
        this.setData({
            show: false
        })
        if (this.data.basicInfo.releaseFiles.length > 0) {
            if (this.data.basicInfo.releaseFiles[0].coverRelativePath) {
                pic = app.apiUrlimg + this.data.basicInfo.releaseFiles[0].coverRelativePath;
            } else {
                pic = app.apiUrlimg + this.data.basicInfo.releaseFiles[0].fileRelativePath;
            }
        }
        return {
            title: this.data.basicInfo.contentText,
            path: this.data.basicInfo.releaseId ? '/packageB/pages/dynamicDetail/index?id=' + this.data.basicInfo.releaseId + '&type=0&sence=1' : '/packageB/pages/dynamicDetail/index?id=' + this.data.basicInfo.confessId + '&type=1&sence=1',
            imageUrl: pic ? pic : '../../../images/home/logo.png',
            success: function (res) {
                // 转发成功之后的回调
                if (res.errMsg == 'shareAppMessage:ok') {
                    console.log(1)
                }
            },
        }
    },
    listen(e) {
        let that = this;
        let obj = e.currentTarget.dataset;
        let typeId = obj.typeid;
        let listenFlag = that.data.basicInfo.listenFlag;
        let userId = that.data.basicInfo.userId;
        var params = {
            status: listenFlag ? 0 : 1,
            userId: '',
            eventId: parseInt(typeId),
        };
        let url = app.apiUrl + '/message/listen';
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            app.request.requestPostApi(url, params, that, that.successFunListen, that.failFun, wx.getStorageSync('token'), {
                typeId: typeId,
                listenFlag: !listenFlag,
                userId: userId,
            })
        }
    },
    successFunListen: function (res, selfObj, data) {
        if (this.data.basicInfo.releaseId) {
            wx.setStorageSync('releaseId2', this.data.basicInfo.releaseId)
            wx.setStorageSync('listenFlag', !this.data.basicInfo.listenFlag)
        }
        selfObj.setData({
            'basicInfo.listenFlag': !this.data.basicInfo.listenFlag
        })
    },
    care() {
        let userId2 = this.data.basicInfo.userId;
        let status = !this.data.basicInfo.status;
    },
    shareAttr() {
        this.setData({
            show: false,
        })
    },
    share(e) {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            this.setData({
                show: true,
            })
        }
    },
    lookImg(e) {
        this.setData({
            urlType: 0,
        })
        lookImgs(e)
        loadAgain = true
    },

    successFun1: function (res, selfObj) {
        if (res.data == null) {
            selfObj.setData({
                basicInfoNoContent: true,
            })
        } else {
            let d = new Date(res.data.releaseTime.replace(/-/g, '/')).getTime();
            let userAvatarUrl = res.data.userAvatarUrl.indexOf('https') > -1 ? res.data.userAvatarUrl : app.apiUrlimg + res.data.userAvatarUrl
            res.data.likeUsers && res.data.likeUsers.forEach((item, index) => {
                item.avatarUrl = item.avatarUrl.indexOf('https') > -1 ? item.avatarUrl : app.apiUrlimg + item.avatarUrl
            })
            if (res.data.releaseFiles.length == 1) {
                res.data.releaseFiles.forEach(good => {
                    good.radio = good.width / good.height;
                });
            } else {
                res.data.releaseFiles.sort(function (a, b) {
                    return (a.order - b.order)
                });
            }
            ;
            selfObj.setData({
                basicInfo: res.data,
                'basicInfo.contentText': res.data.contentText.replace(/\n(\n)*( )*(\n)*\n/g,"\n"),
                'basicInfo.releaseTime': diaplayTime(d),
                'basicInfo.userAvatarUrl': userAvatarUrl
            })

            if (res.data.listenFlag) {
                var params = {
                    eventIds: [parseInt(res.data.releaseId)],
                };
                let url = app.apiUrl + '/message/read';
                app.request.requestPostApi(url, params, selfObj, selfObj.successFunRead, selfObj.failFun, wx.getStorageSync('token'))
            }
        }
    },
    successFunRead: function (res, selfObj) {
        console.log('successFunRead', res)
        app.globalData.message = res.data
    },
    failFun: function (res, selfObj) {
        console.log('failFun', res)
        wx.showToast({
            title: res.message,
        })
    },

    goUserDetail(e) {
        let fakeFlag = e.currentTarget.dataset.fakeflag;
        let userId = e.currentTarget.dataset.userid;
        let deleteFlag;
        let type = e.currentTarget.dataset.type;
        if (type == 1) {
            deleteFlag = wx.getStorageSync('userInfo').userId == userId ? true : false
        } else {
            deleteFlag = e.currentTarget.dataset.deleteflag;
        }
        if(fakeFlag){
            wx.showToast({
                title: '该用户为匿名用户',
            });
        }else{
            wx.navigateTo({
                url: '/packageB/pages/logs/logs?userId=' + userId + '&deleteFlag=' + deleteFlag
            })
        }

    },
    isAuth() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
        }
        ;
        return
    },
    collect(e) {
        let that = this;
        let type = e.currentTarget.dataset.type;
        let typeId = e.currentTarget.dataset.typeid;
        let collectFlag = e.currentTarget.dataset.collectFlag;
        let params = {
            status: that.data.basicInfo.collectFlag ? false : true,
            type: parseInt(type),
            typeId: parseInt(typeId)
        };
        let url = app.apiUrl + '/user/collect';
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            app.request.requestPostApi(url, params, that, that.successFun9, that.failFun, wx.getStorageSync('token'), {
                type: type,
                typeId: typeId
            })
        }
    },
    zan(e) {
        debugger
        let that = this;
        let type = e.currentTarget.dataset.type;
        let typeId = e.currentTarget.dataset.typeid;
        let likeFlag = e.currentTarget.dataset.likeflag;
        let params = {};
        if (type == 1) {
            params = {
                status: likeFlag ? false : true,
                type: type,
                typeId: typeId,
            }
        } else {
            params = {
                status: that.data.basicInfo.likeFlag ? false : true,
                type: type,
                typeId: typeId,
            }
        }
        let url = app.apiUrl + '/user/like';
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            app.request.requestPostApi(url, params, that, that.successFun2, that.failFun, wx.getStorageSync('token'), {
                type: type,
                typeId: typeId
            })
        }
    },
    successFun9: function (res, selfObj, data) {
        selfObj.setData({
            'basicInfo.collectFlag': !this.data.basicInfo.collectFlag
        })

    },
    successFun2: function (res, selfObj, data) {
        debugger
        if (wx.getStorageSync('userInfo')) {
            this.setData({
                nickName: wx.getStorageSync('userInfo').nickName
            })
        }
        ;
        let comment = this.data.comment;
        if (data.type == 1) {
            comment.forEach((item, index) => {
                if (item.id == data.typeId) {
                    item.likeFlag = !item.likeFlag;
                    if (!item.likeFlag) {
                        item.likes -= 1;
                    } else {
                        item.likes += 1;
                    }
                }
                return item
            })
            selfObj.setData({
                comment: comment,
            })
            return
        }
        let likes;
        if (this.data.basicInfo.likeFlag) {
            let likeUser = this.data.basicInfo.likeUsers.filter(function (item) {
                return item.userId != wx.getStorageSync('userInfo').userId
            });
            this.setData({
                'basicInfo.likeUsers': likeUser
            })
            likes = parseInt(this.data.basicInfo.likes) - 1;
        } else {
            let likeUser = this.data.basicInfo.likeUsers.concat(wx.getStorageSync('userInfo'))
            likeUser && likeUser.forEach((item, index) => {
                item.avatarUrl = item.avatarUrl.indexOf('https') > -1 || item.avatarUrl.indexOf('http') > -1 ? item.avatarUrl : app.apiUrlimg + item.avatarUrl
            })
            this.setData({
                'basicInfo.likeUsers': likeUser
            })
            likes = parseInt(this.data.basicInfo.likes) + 1;
        }
        //点赞通知
        if (this.data.basicInfo.releaseId) {
            wx.setStorageSync('releaseId', this.data.basicInfo.releaseId)
            this.tongZhi({
                userId: this.data.basicInfo.userId,
                typeId: this.data.basicInfo.releaseId,
                contentText: this.data.basicInfo.contentText,
                nickName: this.data.nickName
            })

        }
        if (this.data.basicInfo.confessId) {
            wx.setStorageSync('confessId', this.data.basicInfo.confessId)
            this.tongZhi({
                userId: this.data.basicInfo.userId,
                typeId: this.data.basicInfo.confessId,
                contentText: this.data.basicInfo.contentText,
                nickName: this.data.nickName,
                confess: true
            })
        }
        wx.setStorageSync('likeFlag', !this.data.basicInfo.likeFlag)
        selfObj.setData({
            'basicInfo.likes': likes,
            'basicInfo.likeFlag': !this.data.basicInfo.likeFlag
        })
    },
    successFun10(res, selfObj, data) {
        selfObj.setData({
            userId2: '',
        });
        tongZhi(res, selfObj, data)
    },
    tongZhi(data) {
        let that = this;
        let url = app.apiUrl + '/index/get_openid/' + data.userId;
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFun10, that.failFun, wx.getStorageSync('token'), data)
    },

    inputFocus(e) {
        console.log(e, '键盘弹起')
        let that = this;
        that.setData({
            height: e.detail.height,
            isInput: true,
            isMask:true
        })
        setTimeout(function () {

        }, 200);

    },
    bindinputChange: function (e) {
        let that = this
        that.setData({
            content: e.detail.value,
        });
    },
    inputBlur() {
        console.log('键盘收起')
        this.setData({
            height: 0,
            isInput: false,
        })
    },
    hideMask() {
        debugger
        this.setData({
            // content: '',
            isMask: false,
            isInput: false,
            focusInput: false,
            userId2: '',
            nickName2: '',
            placeholdertxt: '写评论',
            commentId: null
        })
    },
    full(e) {
        debugger
    },
    videoTap: function () {
        //获取video
        this.videoContext = wx.createVideoContext('myVideo')
        if (this.data.play) {
            //开始播放
            this.videoContext.play()//开始播放
            this.setData({
                play: false
            })
        } else {
            //当play==false 显示图片 暂停

            this.videoContext.pause()//暂停播放
            this.setData({
                play: true
            })
        }
    },

    delete(e) {
        debugger
        let type = e.currentTarget.dataset.type;
        let id = e.currentTarget.dataset.id;
        let commentId = e.currentTarget.dataset.commentid ? e.currentTarget.dataset.commentid : '';
        let deleteFlag = e.currentTarget.dataset.deleteflag
        this.setData({
            show1: true,
            deleteData: {
                type: type,
                id: id,
                commentId: commentId,
                deleteFlag: deleteFlag,
                userType:wx.getStorageSync('userType'),
            }
        })
    },
    listChange(e) {
        debugger
        let that = this;
        if (e.detail) {
            let type = e.detail.type;
            let id = e.detail.id;
            let commentId = e.detail.commentId;
            for (var key in that.data.comment) {
                if (type == 5 && that.data.comment[key].id == commentId) {
                    let child = that.data.comment[key].childrens
                    for (var key1 in child) {
                        if (child[key1].id === id) {
                            child.splice(key1, 1)
                        }
                    }
                }
                if (type == 5 && that.data.comment[key].id == id) {
                    that.data.comment.splice(key, 1)
                }
            }
            that.setData({
                comment: that.data.comment
            })
            app.globalData.commentId = commentId;
            that.setData({
                'basicInfo.reviews': parseInt(that.data.basicInfo.reviews) - 1
            });
            if (that.data.basicInfo.releaseId) {
                wx.setStorageSync('releaseId', that.data.basicInfo.releaseId)
            } else {
                wx.setStorageSync('confessId', that.data.basicInfo.confessId)
            }
            wx.setStorageSync('reviews', that.data.basicInfo.reviews)
            wx.setStorageSync('likeFlag', that.data.basicInfo.likeFlag)
            app.globalData.confessContent = that.data.comment.slice(0, 3);
        }
        that.setData({
            show1: false,
        });


    }
})