const app=getApp();
Page({
    data:{
        content:'',
        releaseId:null,
        userId:null,
        mask:false
    },
    onShow(){
        let pages = getCurrentPages() //获取加载的页面
        let currentPage = pages[pages.length - 1] //获取当前页面的对象
        let releaseId = currentPage.options.releaseId //如果要获取url中所带的参数可以查看options
        let userId = currentPage.options.userId
        this.setData({
            releaseId:releaseId,
            userId:userId
        })
    },
    bindinputChange: function(e) {
        console.log(e);
        let that = this
        that.setData({
            content: e.detail.value,
        });
    },
    goRelease(){
        let that=this;
        let url = app.apiUrl + '/release/comment';
        if(that.data.content.length==0){
            that.setData({
                mask:true
            });
            wx.showToast({
                title: "请填写内容",
                icon: 'none',
                duration: 2000
            });
            setTimeout(function () {
                that.setData({
                    mask:false
                });
            },2000);
            return
        }
        wx.showLoading()

        let params = {
            content: that.data.content,
            releaseId:  that.data.releaseId,
            userId: that.data.userId,
        };
        app.request.requestPostApi(url, params, that, that.successFun1, that.failFun,wx.getStorageSync('token'))
    },
    fun(){
        this.setData({
            mask:false
        });
    },
    successFun1: function (res, selfObj) {
        let that=this;
        wx.hideLoading();
        wx.navigateTo({
            url: '/packageB/pages/dynamicDetail/index?id='+that.data.releaseId
        })
    },
    failFun: function (res, selfObj) {
        console.log('failFun', res)
    },
})