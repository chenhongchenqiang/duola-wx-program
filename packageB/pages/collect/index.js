const app = getApp()
let loadAgain;
import {diaplayTime, lookImgs} from '../../../utils/util';

Page({
    data: {
        releaseData: {},
        userInfo: {},
        pageIndex: 1,
        topList: ['动态', '表白墙'],
        activeInd: 0,
        deleteFlag: null,
        userId: null,
        releselist:[],
        params: {
            pageNum: 1,
            pageSize: app.globalData.pageSize,
        },
        show1:false,
        deleteData:{},
        deleteType:false,
      showLoading:true,
      loading:false
    },

    takeCare(e){
      if (!wx.getStorageSync('token')) {
        wx.navigateTo({
          url: '/pages/author/index'
        })
        return
      }else{
        let url = app.apiUrl + '/user/follow';
        let status = e.currentTarget.dataset.status;
        let that = this;
        var params = {
          status: status,
          userId2: that.data.userId,
        };
        app.request.requestPostApi(url, params, that, that.successFun6, that.failFun, wx.getStorageSync('token'), status)
      }
        
    },
  successFun6(res, selfObj, status){
    if (status==1){
      wx.showToast({
        icon: 'success',
        title: '关注成功',
      });
    }else{
      wx.showToast({
        icon: 'success',
        title: '取消关注',
      });
    }
        
        this.setData({
          'userInfo.followFlag':status==1?true:false
        });
        console.log(111)
    },
    onReachBottom: function () {
        var that = this;
        if (that.data.params.pageNum < that.data.releaseData.pages) {
            that.setData({
                'params.pageNum': that.data.params.pageNum * 1 + 1,
              loading:true
            })
            that.getMore(that.data.activeInd);
        }
    },
    getMore(type){
        let that=this;
        if (that.data.topList[type] == '表白墙') {
          that.setData({
            activeInd:1
          })

        }
        if (that.data.topList[type] == '动态') {
          that.setData({
            activeInd: 0
          })
           
        }
      that.getList()
    },
    check(e) {
        let that = this;
        let type;
        type = e.currentTarget.dataset.type;
        if (that.data.activeInd != type) {
            that.setData({
                activeInd: type,
                releaseData: {},
                face: [],
                releselist:[],
                'params.pageNum':1,
                showLoading:true,
              loading:false
            })
        }else{
          return
        }
        that.getMore(that.data.activeInd)
    },
    successFun7: function (res, selfObj, data) {
        wx.hideLoading();
        selfObj.setData({
            face: res.data
        })
    },
    goDetail(e) {
      debugger
        let releaseId = e.currentTarget.dataset.id;
        let bizFileType = e.currentTarget.dataset.bizfiletype;
        let type = e.currentTarget.dataset.type;
        let radio = e.currentTarget.dataset.radio;
        if(type==2){
          wx.navigateTo({
            url: '/packageB/pages/detailZj/index?id=' + releaseId
          })
        }else{
          if (bizFileType == 2 && radio>1) {
            wx.navigateTo({
              url: '/packageB/pages/videoDetail/index?id=' + releaseId + '&type=' + type
            })
          } else {
            wx.navigateTo({
              url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=' + type
            })
          }
        }
    },

    lookImg(e) {
        lookImgs(e)
        loadAgain = true
    },

    failFun: function (res, selfObj) {
        console.log('failFun', res)
    },
    goEdit() {
        wx.navigateTo({
            url: '/pages/proInfor/index'
        })
    },
  goLookPhotos(){
    wx.navigateTo({
      url: '/packageB/pages/rank/index'
    })
  },
    onUnload(){
      loadAgain = false
    },
    onHide() {
      loadAgain = true
    },
    onShow() {
        if (loadAgain) {
            loadAgain = false;
            return;
        }
        this.getList()
    },
    successFun: function (res, selfObj, data) {
        selfObj.setData({
            userInfo: res.data,
        });
        wx.setNavigationBarTitle({
            title: res.data.nickName
        });
        this.getList(data)
    },
    getList() {
        let that = this;
        let url = app.apiUrl + '/user/collect_list/';
        let params = {
          queryMe:true,
          type:this.data.activeInd,
            pageNum: that.data.params.pageNum,
            pageSize: app.globalData.pageSize,
        };
        app.request.requestGetApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
    },

    zan(e) {
      if (!wx.getStorageSync('token')) {
        wx.navigateTo({
          url: '/pages/author/index'
        })
        return
      }else{
        let that = this;
        let type = e.currentTarget.dataset.type;
        let typeId = e.currentTarget.dataset.typeid;
        let deleteFlag = e.currentTarget.dataset.deleteFlag;
        let likeFlag = e.currentTarget.dataset.likeflag;
        var params = {
          status: likeFlag ? false : true,
          type: type,
          typeId: typeId,
        };
        let url = app.apiUrl + '/user/like';
        app.request.requestPostApi(url, params, that, that.successFun4, that.failFun, wx.getStorageSync('token'), {
          type: type,
          typeId: typeId,
          status: !likeFlag
        })
      }
        
    },
    successFun4: function (res, selfObj, data) {
        selfObj.data.releselist && selfObj.data.releselist.forEach(item => {
            if (item.releaseId == data.typeId || item.confessId == data.typeId) {
                item.likeFlag = data.status;
                item.likes = data.status ? item.likes + 1 : item.likes - 1
            }
            return selfObj.data.releselist
        });
        selfObj.setData({
            releselist: selfObj.data.releselist,
        })
    },
    successFun1: function (res, selfObj) {
        wx.hideLoading();
        let that = this;
        var content=[];
      if (that.data.topList[that.data.activeInd] == '颜值'){
          res.data&&res.data.forEach(item => {
            let d = null;
            if (item.releaseTime) {
              d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
            }
            return item.releaseTime = diaplayTime(d);
          });
          content = that.data.releselist.concat(res.data);
          that.setData({
            face: res.data
          })
        }else{
          res.data.list&&res.data.list.forEach(item => {
            let d = null;
            if (item.createTime) {
              d = new Date(item.createTime.replace(/-/g, '/')).getTime();
            }
            if (item.releaseTime) {
              d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
            }
            return item.releaseTime = diaplayTime(d);
          });
          content = that.data.releselist.concat(res.data.list)
        }
        //var content = that.data.releselist.concat(res.data.list)
        selfObj.setData({
            releaseData: res.data,
            releselist: content,
          showLoading:false,
          loading:false
        });
    },
    delete(e){
        let type=e.currentTarget.dataset.type;
        let id=e.currentTarget.dataset.id;
        let deleteFlag=e.currentTarget.dataset.deleteflag
        this.setData({
            show1: true,
            deleteData: {
                type:type,
                id:id,
                deleteFlag:deleteFlag,
              collect:e.currentTarget.dataset.collect
            }
        })
    },
    listChange(e) {
      debugger
        console.log(e.detail)
        let type=e.detail.type;
        let id=e.detail.id;
        let that=this;
        let newList=that.data.releselist.filter(function (item) {
            if(type==0){
                return item.releaseId!=id
            }
            if(type==1){
                return item.confessId!=id
            }
            if(type==3){
                return item.faceId!=id
            }
            if(type==4){
                return item.teamId!=id
            }
        })
        that.setData({
            releselist: newList,
            show1: false,
            deleteType:true
        });
    }
})
