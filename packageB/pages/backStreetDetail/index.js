const app = getApp();
let loadAgain;
import {diaplayTime, lookImgs, tongZhi} from '../../../utils/util';

Page({
    data: {
        isIphoneX: app.globalData.isIphoneX,
        pageIndex: 1,
        load: true,
        releaseId: null,
        comment: [],
        commentData: {},
        basicInfo: {},
        likeStatus: false,
        auto_height: true,
        fixHeight: '',
        height: 0,
        isInput: false,
        show: false,
        content: '',
        focusInput: false,
        userId2: '',
        nickName2: '',
        placeholdertxt: '写评论',
        isMask: false,
        topicType: null,
        sence: 0,
        show1: false,
        play: false,
        nickName: '',
        basicInfoNoContent: false,
        loading: false,
        id: '',
        type: '',
        share: false,
    },

    bindTouchStart: function (e) {
        this.startTime = e.timeStamp;
    },
    bindTouchEnd: function (e) {
        this.endTime = e.timeStamp;
    },
    bingLongTap: function (e) {
        console.log("长按");
        wx.setClipboardData({
            data: e.currentTarget.dataset.txt,
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                });
            }
        });
    },

    onLoad(query) {
        debugger
        if (query && !query.id) {
            let scene = {
                id: query.scene,
            }
            this.commonList('', scene)
        } else {
            this.commonList('')
        }
    },
    onUnload() {
        loadAgain = false
    },
    onHide() {
        loadAgain = true
    },
    onShow() {
        // if (loadAgain) {
        //     loadAgain = false;
        //     return;
        // } else {
        //     this.commonList()
        // }
    },
    freeTell(e){
        let phone=e.currentTarget.dataset.phone
        wx.makePhoneCall({
          phoneNumber: phone,
    
        })
      },


    commonList(type1, query) {
        debugger
        wx.showLoading({
            mask: true
        })
        let that = this;
        let pages = getCurrentPages() //获取加载的页面
        let currentPage = pages[pages.length - 1] //获取当前页面的对象
        let backId = query == undefined && currentPage.options.id ? parseInt(currentPage.options.id) : query.id//如果要获取url中所带的参数可以查看

        if (backId) {
            if (type1 != 'more') {
                let url1 = app.apiUrl + '/back/street/detail/' + backId ;
                app.request.requestGetApi(url1, {}, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
            }

            // var params = {
            //     pageNum: that.data.pageIndex,
            //     pageSize: app.globalData.pageSize,
            //     releaseId: releaseId,
            //     commentType: type,
            // };
            // let url = app.apiUrl + '/release/comment_list/';
            // app.request.requestGetApi(url, params, that, that.successFun5, that.failFun, wx.getStorageSync('token'))
        }
    },






    onShareAppMessage: function onShareAppMessage(res) {
        debugger
        if (res.from === 'button') {
            console.log(res.target)
        }
        let pic;
        this.setData({
            show: false
        })
        if (this.data.basicInfo.releaseFiles.length > 0) {
            if (this.data.basicInfo.releaseFiles[0].coverRelativePath) {
                pic = app.apiUrlimg + this.data.basicInfo.releaseFiles[0].coverRelativePath;
            } else {
                pic = app.apiUrlimg + this.data.basicInfo.releaseFiles[0].fileRelativePath;
            }
        }
        return {
            title: this.data.basicInfo.contentText,
            path:  '/packageB/pages/backStreetDetail/index?id=' + this.data.basicInfo.id + '&type=0&sence=1',
            imageUrl: pic ? pic : '../../../images/home/logo.png',
            success: function (res) {
                // 转发成功之后的回调
                if (res.errMsg == 'shareAppMessage:ok') {
                    console.log(1)
                }
            },
        }
    },


    shareAttr() {
        this.setData({
            show: false,
        })
    },
    share(e) {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            this.setData({
                show: true,
            })
        }
    },
    lookImg(e) {
        lookImgs(e)
        loadAgain = true
    },

    successFun1: function (res, selfObj) {
        debugger
        if (res.data == null) {
            selfObj.setData({
                basicInfoNoContent: true,
            })
        } else {
            debugger
            res.data.likeUsers && res.data.likeUsers.forEach((item, index) => {
                item.avatarUrl = item.avatarUrl.indexOf('https') > -1 ? item.avatarUrl : app.apiUrlimg + item.avatarUrl
            })
            if (res.data.releaseFiles.length == 1) {
                res.data.releaseFiles.forEach(good => {
                    good.radio = good.width / good.height;
                });
            } else {
                res.data.releaseFiles.sort(function (a, b) {
                    return (a.order - b.order)
                });
            }
            ;
            selfObj.setData({
                basicInfo: res.data,

            })

            
        }
        wx.hideLoading()
    },
  
    failFun: function (res, selfObj) {
        console.log('failFun', res)
        wx.showToast({
            title: res.message,
        })
    },


    isAuth() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
        }
        ;
        return
    },








    hideMask() {
        debugger
        this.setData({
            content: '',
            isMask: false,
            isInput: false,
            focusInput: false,
            userId2: '',
            nickName2: '',
            placeholdertxt: '写评论',
            commentId: null
        })
    },




})