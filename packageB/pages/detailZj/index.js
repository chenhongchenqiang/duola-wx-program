const app = getApp();
import {diaplayTime, lookImgs, tongZhi} from '../../../utils/util';

let loadAgain;
Page({
    data: {
        pageIndex: 1,
        releaseId: null,
        load: true,
        comment: [],
        commentData: {},
        basicInfo: {},
        likeStatus: false,
        auto_height: true,
        fixHeight: '',
        isInput: false,
        navbarHeight: 0,
        focusInput: false,
        userId2: '',
        nickName2: '',
        placeholdertxt: '写评论',
        isMask: false,
        topicType: null,
        height: 0,
        show: false,
        showTouSu: false,
        showSingUp: false,
        request: {
            phone: '',
            remark: '',
            teamId: '',
            userName: '',
            wechat: ''
        },
        content: '',
        focus: false,
        minHeight: 0,
        show1: false,
        showActionsheet: false,
        showSingUp2: false,
        showSingUp3: false,
        basicInfoNoContent: false,
    },
    lookPerson() {
        this.setData({
            showActionsheet: true,
            showSingUp2: true
        })
    },
    copy: function (e) {
        console.log("长按");
        wx.setClipboardData({
            data: e.currentTarget.dataset.txt,
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                });
            }
        });
    },
    share() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
        } else {
            this.setData({
                show: true,
            })
        }
    },
    shareAttr() {
        this.setData({
            show: false,
        })
    },
    _closeActionSheet(e) {
        this.setData({
            showActionsheet: false,
            showSingUp2: false,
            showSingUp3: false
        });
    },
    commonList(type1, query) {
        debugger
        wx.showLoading();
        let that = this;
        let pages = getCurrentPages() //获取加载的页面
        let currentPage = pages[pages.length - 1] //获取当前页面的对象
        let teamId = query == undefined && currentPage.options.id ? parseInt(currentPage.options.id) : query != undefined && query.id ? query.id : ''//如果要获取url中所带的参数可以查看
        that.setData({
            navbarHeight: app.globalData.navbarHeight,
            minHeight: app.globalData.height
        });
        if (teamId) {
            if (type1 != 'more') {
                let url1 = app.apiUrl + '/team/detail/' + teamId;
                app.request.requestGetApi(url1, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
            }

            var params = {
                pageNum: that.data.pageIndex,
                pageSize: app.globalData.pageSize,
                releaseId: teamId,
                commentType: 2,
            };
            let url = app.apiUrl + '/release/comment_list/';
            app.request.requestGetApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'))
        }
    },
    keyboardHeightChange(e) {
        const height = e.detail.height;
        if(height!=0){
            this.setData({
                isMask:true
            });
        }
        this.setData({
            height: height,
        });
    },
    onShow() {
        this.setData({
            isIphoneX: app.globalData.isIphoneX
        })
    },
    onLoad(query) {
        // if (loadAgain) {
        //     loadAgain = false;
        //     return;
        // }
        if (query && !query.id) {
            let scene = {
                id: query.scene,
            }
            this.setData({
                share: true,
                id: query.scene
            })
            this.commonList('', scene)
        } else {
            this.commonList('')
        }

    },
    // onHide() {
    //   this.setData({
    //     comment: []
    //   })
    // },
    onReachBottom: function () {
        debugger
        var that = this;
        if (that.data.load) {
            if (that.data.pageIndex < that.data.commentData.pages) {
                that.setData({
                    pageIndex: that.data.pageIndex * 1 + 1,
                })
                if (that.data.share) {
                    that.loadMore('more', {id: that.data.id});
                } else {
                    that.loadMore('more');
                }
            }
        }
    },
    loadMore(type1, obj) {
        let that = this;
        that.commonList(type1, obj)
    },
    // loadMore() {
    //     wx.showLoading();
    //     let that = this;
    //     that.commonList('more')
    // },
    bindTouchStart: function (e) {
        this.startTime = e.timeStamp;
    },
    bindTouchEnd: function (e) {
        this.endTime = e.timeStamp;
    },
    bingLongTap: function (e) {
        console.log("长按");
        wx.setClipboardData({
            data: e.currentTarget.dataset.txt,
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                });
            }
        });
    },

    goRelease2(e) {
        if (this.endTime - this.startTime < 350) {
            console.log("点击")
            this.setData({
                focusInput: true,
                userId2: e.currentTarget.dataset.userid,
                nickName2: e.currentTarget.dataset.nickname,
                placeholdertxt: '回复' + e.currentTarget.dataset.nickname,
            })
        }
    },
    goRelease(e) {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
        } else {
            let that = this;
            let teamId = that.data.basicInfo.teamId;
            let userId = that.data.userId2 ? that.data.userId2 : '';
            let nickName = e.currentTarget.dataset.nickName;
            let url = app.apiUrl + '/release/comment';
            if (that.data.content.length == 0) {
                that.setData({
                    mask: true
                });
                wx.showToast({
                    title: "请填写内容",
                    icon: 'none',
                    duration: 2000
                });
                return
            }

            let params = {
                commentType: 2,
                content: that.data.content,
                releaseId: teamId,
                userId: userId,
            };
            app.request.requestPostApi(url, params, that, that.successFun4, that.failFun, wx.getStorageSync('token'))

        }
    },

    successFun4: function (res, selfObj) {
        let that = this;
        if (res.code === 200) {
            wx.showToast({
                icon: 'none',
                title: '评论成功',
            })
            let data = {
                avatarUrl: wx.getStorageSync('userInfo').avatarUrl.indexOf('https') > -1 ? wx.getStorageSync('userInfo').avatarUrl : app.apiUrlimg + wx.getStorageSync('userInfo').avatarUrl,
                avatarUrl2: "",
                content: selfObj.data.content,
                createTime: "刚刚",
                deleteFlag: true,
                id: res.data,
                likeFlag: false,
                likes: 0,
                nickName: wx.getStorageSync('userInfo').nickName,
                nickName2: selfObj.data.nickName2,
                userId: wx.getStorageSync('userInfo').userId,
                userId2: selfObj.data.userId2
            };
            selfObj.data.comment.unshift(data);
            selfObj.setData({
                comment: that.data.comment,
                placeholdertxt: '写评论',
                content: '',
                isMask: false,
                height: 0,
                nickName2: '',
            });
            //评论通知
            if (that.data.basicInfo.teamId) {
                debugger
                that.tongZhi({
                    userId: that.data.userId2 || that.data.basicInfo.userId,
                    typeId: that.data.basicInfo.teamId,
                    confessContent: data,
                    contentText: that.data.basicInfo.contentText,
                    nickName: that.data.nickName,
                    number: 1
                })
            }
        } else {
            wx.showToast({
                title: res.errMsg,
            })
        }
        wx.requestSubscribeMessage({
            tmplIds: app.globalData.template_id,
        });
    },
    successFun: function (res, selfObj) {
        let that = this;
        res.data.list && res.data.list.forEach(item => {
            let d = new Date(item.createTime.replace(/-/g, '/')).getTime();
            item.createTime = diaplayTime(d);
            item.avatarUrl = item.avatarUrl.indexOf('https') > -1 ? item.avatarUrl : app.apiUrlimg + item.avatarUrl
        });
        var content = that.data.comment.concat(res.data.list)
        selfObj.setData({
            comment: content,
            totalPage: res.data.pages,
            commentData: res.data,
        });
        wx.hideLoading()
    },
    successFun1: function (res, selfObj) {
        if (res.data == null) {
            selfObj.setData({
                basicInfoNoContent: true,
            })
        } else {
            let userAvatarUrl = res.data.userAvatarUrl.indexOf('https') > -1 ? res.data.userAvatarUrl : app.apiUrlimg + res.data.userAvatarUrl
            let d = new Date(res.data.releaseTime.replace(/-/g, '/')).getTime();
            res.data.teamUsers && res.data.teamUsers.forEach(item => {
                item.avatarUrl = item.avatarUrl.indexOf('https') > -1 ? item.avatarUrl : app.apiUrlimg + item.avatarUrl
            });
            selfObj.setData({
                basicInfo: res.data,
                'basicInfo.releaseTime': diaplayTime(d),
                'basicInfo.userAvatarUrl': userAvatarUrl
            })
        }
    },

    failFun: function (res, selfObj) {
        console.log('failFun', res)
    },
    singUp() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
        } else {
            this.setData({
                showSingUp: true,
                'request.teamId': this.data.basicInfo.teamId
            })
        }
    },
    bindKeyInput: function (e) {
        let type = e.target.dataset.type;
        if (type == 1)
            this.setData({
                'request.userName': e.detail.value
            })
        if (type == 2)
            this.setData({
                'request.phone': e.detail.value
            })
        if (type == 3)
            this.setData({
                'request.wechat': e.detail.value
            })
        if (type == 4)
            this.setData({
                'request.remark': e.detail.value
            })

    },
    orderSign() {
        debugger
        let that = this;
        if (!that.data.request.userName) {
            wx.showToast({
                icon: 'none',
                title: '填写报名用户',
            })
            return
        } else if (!that.data.request.phone) {
            wx.showToast({
                icon: 'none',
                title: '填写联系方式',
            })
            return
        } else {
            let url = app.apiUrl + '/team/join';
            let params = that.data.request;
            app.request.requestPostApi(url, params, that, that.successFun2, that.failFun, wx.getStorageSync('token'))
        }
    },
    successFun2: function (res, selfObj) {
        wx.hideLoading()
        if (res.code == 200) {
            wx.showToast({
                icon: 'success',
                title: '加入成功',
            })
            let newJoiner = {
                avatarUrl: wx.getStorageSync('userInfo').avatarUrl,
                userId: res.data.userId,
                index: res.data.current,
            };
            if (selfObj.data.basicInfo.left == selfObj.data.basicInfo.teamUsers.length) {
                selfObj.setData({
                    'basicInfo.left': 0,
                    'basicInfo.status': 2
                });
            }
            ;
            selfObj.setData({
                showSingUp: false,
                'basicInfo.joinFlag': true,
                'basicInfo.teamUsers': selfObj.data.basicInfo.teamUsers.concat(newJoiner)
            });
            selfObj.tongZhi({
                userId: selfObj.data.basicInfo.userId,
                contentText: selfObj.data.basicInfo.contentText,
                phone_number: selfObj.data.request.phone,
                number: 1,
                id: selfObj.data.basicInfo.teamId
            })
        } else {
            wx.showToast({
                title: res.message,
            })
        }
    },
    successFun10(res, selfObj, data) {
        selfObj.setData({
            userId2: '',
        });
        tongZhi(res, selfObj, data)
    },
    tongZhi(data) {
        let that = this;
        let url = app.apiUrl + '/index/get_openid/' + data.userId;
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFun10, that.failFun, wx.getStorageSync('token'), data)
    },
    hide() {
        this.setData({
            showSingUp: false
        })
    },

    onShareAppMessage: function onShareAppMessage(res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        let pic = app.apiUrlimg + this.data.basicInfo.releaseFiles[0].fileRelativePath;
        this.setData({
            show: false
        })
        return {
            title: this.data.basicInfo.contentText,
            path: '/packageB/pages/detailZj/index?id=' + this.data.basicInfo.teamId,
            imageUrl: pic ? pic : '../../../images/home/logo.png',
            success: function (res) {
                // 转发成功之后的回调
                if (res.errMsg == 'shareAppMessage:ok') {
                    console.log(1)
                }
            },
        }
    },


    listChange() {
        debugger
        let that = this;
        that.setData({
            showTouSu: false,
        });
    },

    goUserDetail(e) {
        let userId = e.currentTarget.dataset.userid;
        let deleteFlag;
        let type = e.currentTarget.dataset.type;
        if (type == 1) {
            deleteFlag = wx.getStorageSync('userInfo').userId == userId ? true : false
        } else {
            deleteFlag = e.currentTarget.dataset.deleteflag;
        }
        wx.navigateTo({
            url: '/packageB/pages/logs/logs?userId=' + userId + '&deleteFlag=' + deleteFlag
        })
    },
    lookImg(e) {
        lookImgs(e)
        //loadAgain = true
    },
    isAuth() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
    },
    inputFocus(e) {
        console.log(e, '键盘弹起')
        let that = this;
        setTimeout(function () {
            that.setData({
                height: e.detail.height,
                isInput: true,
                isMask: true
            })
        }, 200);

    },
    bindinputChange: function (e) {
        console.log(e);
        let that = this
        that.setData({
            content: e.detail.value,
        });
    },
    hideMask() {
        debugger
        this.setData({
            content: '',
            isMask: false,
            isInput: false,
            height: 0,
            userId2: '',
            nickName2: '',
            placeholdertxt: '写评论',
        })
    },
    inputBlur() {
        console.log('键盘收起')
        this.setData({
            height: 0,
            isInput: false
        })
    },

    focusButn: function () {
        this.setData({
            focusInput: true,
            isInput: true
        })
    },
    takeCare() {
        let url = app.apiUrl + '/user/follow';
        let that = this;
        var params = {
            status: 1,
            userId2: that.data.basicInfo.userId,
        };
        app.request.requestPostApi(url, params, that, that.successFun6, that.failFun, wx.getStorageSync('token'))
    },
    successFun6() {
        wx.showToast({
            icon: 'success',
            title: '关注成功',
        });
        this.setData({
            'basicInfo.followFlag': true
        });
        console.log(111)
    },
    delete(e) {
        let type = e.currentTarget.dataset.type;
        let id = e.currentTarget.dataset.id;
        let deleteFlag = e.currentTarget.dataset.deleteflag
        this.setData({
            show1: true,
            showTouSu: true,
            deleteData: {
                type: type,
                id: id,
                deleteFlag: deleteFlag
            }
        })
    },
})