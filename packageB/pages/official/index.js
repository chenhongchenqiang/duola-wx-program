const app = getApp();
let loadAgain;
import {diaplayTime,lookImgs} from '../../../utils/util';
Page({
    data: {
        pageIndex: 1,
        load:true,
        releaseId: null,
        comment: [],
        commentData: {},
        basicInfo: {},
        likeStatus:false,
        auto_height:true,
        fixHeight:'',
        height: 0,
        isInput:false,
        show: false,
        content:'',
        focusInput: false,
        userId2:'',
        nickName2:'',
        placeholdertxt:'写评论',
        isMask:false,
        topicType:null,
        sence:0,
        show1:false,
      play:false
    },

    onShow() {
       
        if (loadAgain) {
            loadAgain = false;
            return;
        }
      wx.showLoading()
        this.commonList()
    },
    takeCare(){
        let url = app.apiUrl + '/user/follow';
        let that=this;
        var params = {
            status: 1,
            userId2:that.data.basicInfo.userId,
        };
        app.request.requestPostApi(url, params, that, that.successFun6, that.failFun,wx.getStorageSync('token'))
    },
    successFun6(){
        wx.showToast({
            icon: 'success',
            title: '关注成功',
        });
        this.setData({
            'basicInfo.followFlag':true
        });
        console.log(111)
    },
    onReachBottom: function () {
        var that = this;
        if (that.data.load) {
            if (that.data.pageIndex < that.data.commentData.pages) {
                that.setData({
                    pageIndex: that.data.pageIndex * 1 + 1,
                })
                that.loadMore();
            }
        }
    },
    loadMore() {
        wx.showLoading();
        let that = this;
        that.commonList('more')
    },
    commonList(type1){
        debugger
        let that = this;
        let pages = getCurrentPages() //获取加载的页面
        let currentPage = pages[pages.length - 1] //获取当前页面的对象
        let releaseId = parseInt(currentPage.options.id) //如果要获取url中所带的参数可以查看options
        let type = parseInt(currentPage.options.type);
        let sence = parseInt(currentPage.options.sence);
        console.log(sence)
        console.log(releaseId)
        that.setData({
           topicType:type,
            sence:sence,
            MyUserId:sence?'': wx.getStorageSync('userInfo').userId
        });

        if (releaseId) {
            if(type1!='more') {
                let url1 = type === 1 ? app.apiUrl + '/confess/detail/' + releaseId : app.apiUrl + '/release/detail/' + releaseId;
                app.request.requestGetApi(url1, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
            }

            var params = {
                pageNum: that.data.pageIndex,
                pageSize: app.globalData.pageSize,
                releaseId:releaseId,
                commentType:type,
            };
            let url = app.apiUrl + '/release/comment_list/';
            app.request.requestGetApi(url, params, that, that.successFun5, that.failFun, wx.getStorageSync('token'))
        }
    },
    successFun5: function (res, selfObj) {
        let that=this;
        res.data.list&&res.data.list.forEach(item => {
            let d = new Date(item.createTime.replace(/-/g, '/')).getTime();
            item.createTime = diaplayTime(d);
        });
        var content = that.data.comment.concat(res.data.list)
        selfObj.setData({
            comment: content,
            totalPage: res.data.pages,
            commentData: res.data
        });
        wx.hideLoading();
    },

    goRelease1(e) {
        this.setData({
            focusInput: true,
            userId2:e.currentTarget.dataset.userid,
            nickName2:e.currentTarget.dataset.nickname,
            placeholdertxt:'回复'+e.currentTarget.dataset.nickname,
        })
    },
    goRelease(){
        let that=this;
        let userId = that.data.userId2?that.data.userId2:'';
        let releaseId=that.data.basicInfo.releaseId||that.data.basicInfo.confessId;
        let url = app.apiUrl + '/release/comment';
        that.isAuth();
        if(that.data.content.length==0){
            that.setData({
                mask:true
            });
            wx.showToast({
                title: "请填写内容",
                icon: 'none',
                duration: 2000
            });
            return
        }
        let params = {
            commentType:that.data.topicType,
            content: that.data.content,
            releaseId: releaseId,
            userId: userId,
        };
        app.request.requestPostApi(url, params, that, that.successFun4, that.failFun, wx.getStorageSync('token'))
    },

    successFun4:function (res, selfObj) {
        if(res.code===200){
            let that=this;
            wx.showToast({
                icon:'none',
                title: '评论成功',
            })
            let data={
              avatarUrl: wx.getStorageSync('userInfo').avatarUrl,
                avatarUrl2: "",
                content: selfObj.data.content,
                createTime: "刚刚",
                deleteFlag: true,
                id: res.data,
                likeFlag: false,
                likes: 0,
              nickName: wx.getStorageSync('userInfo').nickName,
                nickName2: selfObj.data.nickName2,
                userId:  selfObj.data.basicInfo.userId,
                userId2: selfObj.data.userId2,
            };
            selfObj.data.comment.unshift(data);
            selfObj.setData({
                comment:that.data.comment,
                content:'',
                isMask:false,
                nickName:'',
                userId:'',
              'commentData.total': that.data.commentData.total+1

            })
        }else{
            wx.showToast({
                title: res.errMsg,
            })
        }


    },
    onShareAppMessage(res) {
        if (res.from === 'button') {
            // 来自页面内转发按钮
            console.log(res.target)
        }

        let pic;
        if(this.data.basicInfo.releaseFiles.length>0){
            pic=app.apiUrlimg+this.data.basicInfo.releaseFiles[0].fileRelativePath;
        }
        return {
            title:this.data.basicInfo.contentText,
            path: '/packageB/pages/dynamicDetail/index?id=' + this.data.basicInfo.releaseId ,
            imageUrl:pic?pic:'',
        };
    },
    care(){
        let userId2=this.data.basicInfo.userId;
        let status=!this.data.basicInfo.status;
    },
    share(){
        this.setData({
            show:true
        })
    },
    lookImg(e){
        lookImgs(e)
        loadAgain=true
    },

    successFun1: function (res, selfObj) {
        wx.hideLoading();
        let d = new Date(res.data.releaseTime.replace(/-/g, '/')).getTime();
        selfObj.setData({
            basicInfo: res.data,
            'basicInfo.releaseTime':diaplayTime(d)
        })
    },
    failFun: function (res, selfObj) {
        console.log('failFun', res)
        wx.showToast({
            title:res.message,
        })
    },

    goUserDetail(e){
        let userId=e.currentTarget.dataset.userid;
        let deleteFlag=e.currentTarget.dataset.deleteflag;
        wx.navigateTo({
            url: '/packageB/pages/logs/logs?userId=' + userId+'&deleteFlag='+deleteFlag
        })
    },
    isAuth(){
        if(!wx.getStorageSync('token')){
            wx.navigateTo({
                url:'/pages/author/index'
            })
            return
        };
    },
    zan(e){
        let that=this;
        let type=e.currentTarget.dataset.type;
        let typeId=e.currentTarget.dataset.typeid;
        let likeFlag=e.currentTarget.dataset.likeflag;
        that.isAuth();
        let params={};
        if(type==1){
            params={
                status: likeFlag?false:true,
                type: type,
                typeId:typeId,
            }
        }else{
            params={
                status: that.data.basicInfo.likeFlag?false:true,
                type: type,
                typeId:typeId,
            }
        }
        let url = app.apiUrl + '/user/like';
        app.request.requestPostApi(url, params, that, that.successFun2, that.failFun, wx.getStorageSync('token'),{type:type,typeId:typeId})
    },
    successFun2:function (res, selfObj,data) {
        let comment=this.data.comment;
        if(data.type==1){
           comment.forEach((item,index)=>{
                if(item.id==data.typeId){
                    item.likeFlag=!item.likeFlag;
                    if(!item.likeFlag){
                        item.likes-=1;
                    }else{
                        item.likes+=1;
                    }
                }
                return item
            })
            selfObj.setData({
                comment:comment,
            })
            return
        }
        let likes;
        if(this.data.basicInfo.likeFlag){
            let likeUser=this.data.basicInfo.likeUsers.filter(function (item) {
              return item.userId !=wx.getStorageSync('userInfo').userId
            });
            this.setData({
                'basicInfo.likeUsers':likeUser
            })
            likes=parseInt(this.data.basicInfo.likes)-1;
        }else{
            let likeUser=this.data.basicInfo.likeUsers.concat(wx.getStorageSync('userInfo'))
            this.setData({
                'basicInfo.likeUsers':likeUser
            })
            likes=parseInt(this.data.basicInfo.likes)+1;
        }
        if(this.data.basicInfo.releaseId){
            wx.setStorageSync('releaseId', this.data.basicInfo.releaseId)
        }
        if(this.data.basicInfo.confessId){
            wx.setStorageSync('confessId', this.data.basicInfo.confessId)
        }
        wx.setStorageSync('likeFlag', !this.data.basicInfo.likeFlag)
        selfObj.setData({
            'basicInfo.likes':likes,
            'basicInfo.likeFlag':!this.data.basicInfo.likeFlag
        })
    },

    inputFocus(e) {
        console.log(e, '键盘弹起')
        let that=this;
        setTimeout(function(){
            that.setData({
                height: e.detail.height,
                isInput: true,
                isMask:true
            })
        }, 200);

    },
    bindinputChange: function(e) {
        let that = this
        that.setData({
            content: e.detail.value,
        });
    },
    inputBlur() {
        console.log('键盘收起')
        this.setData({
            height: 0,
            isInput: false,
        })
    },
    hideMask(){
        this.setData({
            content: '',
            isMask: false,
            isInput: false,
            userId2:'',
            nickName2:'',
            placeholdertxt:'写评论',
        })
    },
    full(e){
        debugger
    },
  videoTap: function () {
    //获取video
    this.videoContext = wx.createVideoContext('myVideo')
    if (this.data.play) {
      //开始播放
      this.videoContext.play()//开始播放
      this.setData({
        play: false
      })
    } else {
      //当play==false 显示图片 暂停

      this.videoContext.pause()//暂停播放
      this.setData({
        play: true
      })
    }
  },

    delete(e){
        let type=e.currentTarget.dataset.type;
        let id=e.currentTarget.dataset.id;
        let deleteFlag=e.currentTarget.dataset.deleteflag
        this.setData({
            show1: true,
            deleteData: {
                type:type,
                id:id,
                deleteFlag:deleteFlag
            }
        })
    },
  listChange(e) {
    console.log(e.detail)
    let type = e.detail.type;
    let id = e.detail.id;
    let that = this;
    let newList = that.data.comment.filter(function (item) {
      if (type == 5) {
        return item.id != id
      }
    })
    that.setData({
      comment: newList,
      show1: false,
      'commentData.total': that.data.commentData.total - 1
    });
  }
})