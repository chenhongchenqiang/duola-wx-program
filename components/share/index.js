const app = getApp();
Component({
    options: {
        multipleSlots: true,
        addGlobalClass: true
    },
    properties: {
        title: {
            type: String,
            value: ''
        },
        showActionsheet: {
            type: Boolean,
            value: false
        },
        actions: {
            type: Array,
            value: [],
            observer: '_groupChange'
        },
        goodsDetail: {
            type: Object
        },
        sharData: {
            type: Object
        },
        maskHidden: {
            type: Boolean,
            value: false
        },
        showCanvas: {
            type: Boolean,
            value: false
        },
        canvasImg: {
            type: String,
            value: ''
        },
        shareCanvasHeight: {
            type: Number,
            value: 1000
        },
        shareCanvasImg: {
            type: Number,
            value: 100
        },
        qrcodeUrl: {
            type: String,
            value: ''
        },
        adminQrcode: {
            type: String,
            value: ''
        },
    },

    methods: {
        _closeActionSheet(e) {
            this.setData({
                showActionsheet: false,
            });
            this.triggerEvent('shareAttr');
        },

        _createPoster(e) {
            debugger
            let self = this;
            self.triggerEvent('shareAttr');
            let json = {
                qrcodeUrl: this.data.qrcodeUrl,
                shareContent: this.data.sharData.contentText.replace(/\=/g,"").replace(/\?/g,""),
                playerName: this.data.sharData.userNickName.replace(/\=/g,"").replace(/\?/g,""),
                slogan: this.data.sharData.userSchool,
                topic: this.data.sharData.confessId ? '表白墙' :this.data.sharData.topicTypeStr?this.data.sharData.topicTypeStr: this.data.sharData.topic,
                file: this.data.sharData.releaseFiles
            };
            this.setData({
              showActionsheet: false,
              navbarHeight: app.globalData.navbarHeight,
              isIphoneX: app.globalData.isIphoneX
            });
            wx.showLoading({
                title: '海报生成中...',
            })
            canvasWxQrcode();
            function canvasWxQrcode() {
                let teamId = self.data.sharData.teamId;
                let backId= self.data.sharData.id;
                let releaseId=self.data.sharData.releaseId;
                let confessId=self.data.sharData.confessId;
                let pageUrl = 'pages/index/index';
                // let pageUrl = teamId ? 'packageB/pages/detailZj/index' : backId?'packageB/pages/backStreetDetail/index':'packageB/pages/dynamicDetail/index';
                let aa;
                if (self.data.sharData.releaseId) {
                    aa = parseInt(self.data.sharData.releaseId) + "&" + 0
                }
                if (self.data.sharData.confessId) {
                    aa = parseInt(self.data.sharData.confessId) + "&" + 1
                }
                if (backId) {
                    aa = parseInt(backId)
                }
                if (teamId) {
                    aa = parseInt(teamId)
                }
                wx.request({
                    url: app.apiUrl + '/wx/getwxacodeUrl',
                    data: {
                        page: pageUrl,
                        scene: aa,
                        width: 300
                    },
                    header: {
                        'content-type': 'application/json'
                    },
                    method: 'POST',
                    dataType: 'json',
                    success: function (res) {
                        json.qrcodeUrl = app.apiUrlimg + res.data.data;
                        wx.hideLoading({})
                        wx.navigateTo({
                            url: '/pages/outer/index?type=canvas&obj='+JSON.stringify(json)
                        })
                    },
                    fail: function () {
                        wx.hideLoading();
                        wx.showToast({
                            title: '网络出错',
                            icon: 'fail',
                            duration: 2000
                        })

                    },
                });
            }
        },
    },

})