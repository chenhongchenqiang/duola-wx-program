const clientWidth = wx.getSystemInfoSync().screenWidth;
const app = getApp();
import {base64src} from '../../utils/util';

let x = 0,
    y = 0;
let ctxs = null;
let ctxW = 750;
let ctxH = 1000;
let imgTopPos = 374;
let xs = 0;
Component({
    options: {
        multipleSlots: true,
        addGlobalClass: true
    },
    properties: {
        title: {
            type: String,
            value: ''
        },
        showActionsheet: {
            type: Boolean,
            value: false
        },
        actions: {
            type: Array,
            value: [],
            observer: '_groupChange'
        },
        goodsDetail: {
            type: Object
        },
        sharData: {
            type: Object
        },
        maskHidden: {
            type: Boolean,
            value: false
        },
        showCanvas: {
            type: Boolean,
            value: false
        },
        canvasImg: {
            type: String,
            value: ''
        },
        shareCanvasHeight: {
            type:Number,
            value: 1000
        },
        shareCanvasImg: {
            type: Number,
            value: 100
        },
        qrcodeUrl: {
            type: String,
            value: ''
        },
        adminQrcode: {
            type: String,
            value: ''
        },
    },

    methods: {
        _closeActionSheet(e) {
            this.setData({
                showActionsheet: false,
            });
            this.triggerEvent('shareAttr');
        },
        delImg() {
            this.createSelectorQuery()
                .select('#posterCanvas')
                .fields({node: true, size: true})
                .exec(async (res) => {
                    const canvas = res[0].node;
                    const ctx = canvas.getContext('2d');
                    canvas.width = res[0].width;
                    canvas.height = res[0].height;
                    ctx.clearRect(0, 0, 320, 410); //清空画板
                })
            this.setData({
                showCanvas: false,
                maskHidden: false,
                canvasImg: ''
            })
            wx.hideLoading();
            this.triggerEvent('shareAttr');
        },
        _drawQrcode() {
            let that = this;
            let pageUrl =  'packageB/pages/dynamicDetail/index'
            wx.request({
                url: app.apiUrl + '/wx/getwxacodeUrl',
                data: {
                    page: pageUrl,
                    scene: that.data.sharData.releaseId,
                    width: 300
                },
                header: {
                    'content-type': 'application/json'
                },
                method: 'POST',
                dataType: 'json',
                success: function (res) {
                    debugger
                    that.setData({
                        qrcodeUrl: res.data//"http://www.qiyuanheshan.com/document//xnxw/topic/A442591EF7E441FBB94F25619807466D.jpg",
                    });
                    //服务器小程序码地址
                },
                fail: function () {
                    return
                },
            });
        },
        _createPoster(e) {
            debugger
            let self = this;
            //self._drawQrcode();
            let len = self.data.sharData.contentText.length;
            let cnavasTop = len >= 60 ? 160 : len >= 45 ? 120 : len >= 30 ? 80 : len >= 15 ? 40 : 0;
            self.setData({
                shareCanvasHeight: cnavasTop+825,
                shareCanvasImg: 750 / (700 / (cnavasTop + 825))
            });
            let json = {
                qrcodeUrl: this.data.qrcodeUrl,
                shareContent: this.data.sharData.contentText.replace(/\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDE4F]/g, ""),
                playerName: this.data.sharData.userNickName,
                slogan: this.data.sharData.userSchool,
                topic: this.data.sharData.confessId ? '表白墙' : this.data.sharData.topic,
            };
            this.setData({
                maskHidden: true,
                showActionsheet: false,
                showCanvas: true,
                navbarHeight: app.globalData.navbarHeight,
                isIphoneX: app.globalData.isIphoneX
            });
            wx.showLoading({
                title: '海报生成中...',
            })
            this.createSelectorQuery()
                .select('#posterCanvas')
                .fields({node: true, size: true})
                .exec(async (res) => {
                    const canvas = res[0].node;
                    const ctx = canvas.getContext('2d');
                    canvas.width = res[0].width;
                    canvas.height = res[0].height;
                    ctx.clearRect(0, 0, 700, 820); //清空画板
                    ctx.fillStyle = '#fff';
                    ctx.fillRect(0, 0, 700, 820);
                    //写背景
                    ctx.fillStyle = '#029bdf';
                    ctx.fillRect(0, 0, 700, self.data.shareCanvasHeight);
                    ctx.fillStyle = '#ffffff';
                    ctx.fillRect(40, 40, 620, self.data.shareCanvasHeight - 80);


                    //当前编写高
                    let topicWidth = (320 - json.topic.length * 40) / 2;
                    //话题背景
                    ctx.save();
                    ctx.translate(0, 120);//设置画布上的(0,0)位置，也就是旋转的中心点
                    ctx.rotate(-20*Math.PI/180);
                    ctx.fillStyle ='#ffd231';
                    ctx.fillRect(10,10,320,80);
                    ctx.restore();
                    //话题
                    ctx.save();
                    ctx.translate(0, 120);//设置画布上的(0,0)位置，也就是旋转的中心点
                    ctx.rotate(-20 * Math.PI / 180);
                    ctx.fillStyle='#333';
                    ctx.font = "40px normal";
                    ctx.fillText(json.topic, topicWidth, 64);
                    ctx.restore();
                    // ctx.fillRect(50,20,100,50);
                    // ctx.rotate(20*Math.PI/180);
                    // ctx.stroke();

                    //文案内容
                    let txtLeftPos = 150;

                    ctx.textBaseline = "top";
                    ctx.textAlign = 'left';

                    //内容

                    ctx.font = "40px bold"; //设置字体大小，默认10
                    ctx.fillStyle = '#000'; //文字颜色：默认黑色
                    //ctx.fillText(json.shareContent, 10, 190)//绘制文本
                    if (json.shareContent.length > 15) {
                        var a = json.shareContent.substr(0, 15);
                        var b = json.shareContent.substr(16, json.shareContent.length).length > 15 ? `${json.shareContent.substr(15, 15)}` : json.shareContent.substr(15, json.shareContent.length);
                        var c = json.shareContent.substr(30, json.shareContent.length).length > 15 ? `${json.shareContent.substr(30, 15)}` : json.shareContent.substr(30, json.shareContent.length);
                        var d = json.shareContent.substr(45, json.shareContent.length).length > 15 ? `${json.shareContent.substr(45, 15)}` : json.shareContent.substr(45, json.shareContent.length);
                        var e = json.shareContent.substr(60, json.shareContent.length).length > 15 ? `${json.shareContent.substr(60, 15)}...` : json.shareContent.substr(60, json.shareContent.length);
                        ctx.fillText(a, 70, 200);
                        ctx.fillText(b, 70, 250);
                        ctx.fillText(c, 70, 300);
                        ctx.fillText(d, 70, 350);
                        ctx.fillText(e, 70, 400);
                    } else {
                        ctx.fillText(json.shareContent, 70, 200);
                    }
                    console.log("写完字")
                    //生成主图
                    let len = json.shareContent.length
                    if (len == 0) {
                        imgTopPos = 213
                    }
                    if (0 < len && len <= 15) {
                        imgTopPos = 263
                    }
                    if (15 < len && len <= 30) {
                        imgTopPos = 313
                    }
                    if (30 < len && len <= 45) {
                        imgTopPos = 363
                    }
                    if (45< len && len <= 60) {
                        imgTopPos = 413
                    }
                    if (len > 60) {
                        imgTopPos = 463
                    }
                    //第一张
                    const mainImg = canvas.createImage();
                    if (this.data.sharData.releaseFiles[0] && !this.data.sharData.releaseFiles[0].coverRelativePath) {
                        mainImg.src = app.apiUrlimg + this.data.sharData.releaseFiles[0].fileRelativePath;
                    } else if (this.data.sharData.releaseFiles[0] && this.data.sharData.releaseFiles[0].coverRelativePath) {
                        mainImg.src = app.apiUrlimg + this.data.sharData.releaseFiles[0].coverRelativePath;
                    } else {
                        mainImg.src = "./logobg.jpg"
                    }

                    let mainImgPo = await new Promise((resolve, reject) => {
                        mainImg.onload = () => {
                            resolve(mainImg)
                        }
                        mainImg.onerror = (e) => {
                            reject(e)
                        }
                    });
                    let h = mainImgPo.height;
                    let w = mainImgPo.width;
                    let radio = mainImgPo.height / mainImgPo.width;
                    // 剪切矩形区域
                    ctx.save();
                    ctx.beginPath()//开始创建一个路径
                    ctx.rect(70, imgTopPos, 275, 275);
                    ctx.clip()//裁剪
                    if (radio >= 1) {
                        ctx.drawImage(mainImgPo, 70, imgTopPos - (275 * radio - 275) / 2, 275, h * 275 / w);
                    } else {
                        ctx.drawImage(mainImgPo, 70 - (275 / radio - 275) / 2, imgTopPos, 275 / radio, 265);
                    }
                    ctx.closePath();
                    ctx.restore();
                    //第二张
                    if (this.data.sharData.releaseFiles[1]&&this.data.sharData.releaseFiles[1]) {
                        const mainImg2 = canvas.createImage();
                        if (this.data.sharData.releaseFiles[1] && !this.data.sharData.releaseFiles[1].coverRelativePath) {
                            mainImg2.src = app.apiUrlimg + this.data.sharData.releaseFiles[1].fileRelativePath;
                        } else if (this.data.sharData.releaseFiles[1] && this.data.sharData.releaseFiles[1].coverRelativePath) {
                            mainImg2.src = app.apiUrlimg + this.data.sharData.releaseFiles[1].coverRelativePath;
                        } else {
                            mainImg2.src = ""
                        }

                        let mainImgPo2 = await new Promise((resolve, reject) => {
                            mainImg2.onload = () => {
                                resolve(mainImg2)
                            }
                            mainImg2.onerror = (e) => {
                                reject(e)
                            }
                        });
                        let h2 = mainImgPo2.height;
                        let w2 = mainImgPo2.width;
                        let radio2 = mainImgPo2.height / mainImgPo2.width;
                        // 剪切矩形区域
                        ctx.save();
                        ctx.beginPath()//开始创建一个路径
                        ctx.rect(355, imgTopPos, 275, 275);
                        ctx.clip()//裁剪
                        if (radio2 >= 1) {
                            ctx.drawImage(mainImgPo2, 355, imgTopPos - (275 * radio2 - 275) / 2, 275, h2 * 265 / w2);
                        } else {
                            ctx.drawImage(mainImgPo2, 355 - (275 / radio2 - 275) / 2, imgTopPos, 275 / radio2, 265);
                        }
                        // ctx.drawImage(mainImgPo2, 310, imgTopPos, 230, h * 230 / w);
                        ctx.closePath();
                        ctx.restore();
                    }
                    //梦版
                    debugger
                    if (this.data.sharData.releaseFiles[0]&&this.data.sharData.releaseFiles.length > 2) {
                        ctx.beginPath();
                        ctx.fillStyle = "rgba(0,0,0,0.5)";
                        ctx.fillRect(355, imgTopPos, 275, 275);
                        ctx.stroke();
                    }
                    if (this.data.sharData.releaseFiles[0]&&this.data.sharData.releaseFiles.length > 2) {
                        let num = '共' + this.data.sharData.releaseFiles.length + '图'
                        ctx.font = "28px normal"; //设置字体大小，默认10
                        ctx.fillStyle = '#ffffff'; //文字颜色：默认黑色
                        ctx.fillText(num, 443, imgTopPos + 125)//绘制文本
                    }
                    //哆啦校圈
                    ctx.font = "28px normal"; //设置字体大小，默认10
                    ctx.fillStyle = '#000000'; //文字颜色：默认黑色
                    ctx.fillText('哆啦校圈', 220, imgTopPos+335)//绘制文本
                    //哆啦校圈
                    ctx.font = "24px normal"; //设置字体大小，默认10
                    ctx.fillStyle = '#666666'; //文字颜色：默认黑色
                    ctx.fillText('来哆啦校圈，有一说一', 284, imgTopPos + 340)//绘制文本
                    //哆啦校圈
                    ctx.font = "24px normal"; //设置字体大小，默认10
                    ctx.fillStyle = '#999999'; //文字颜色：默认黑色
                    ctx.fillText('长按识别，查看详情', 220, imgTopPos+382)//绘制文本
                    //学校
                    ctx.save();
                    ctx.translate(200, imgTopPos+400);//设置画布上的(0,0)位置，也就是旋转的中心点
                    ctx.rotate(-20 * Math.PI / 180);
                    ctx.fillStyle = "rgba(0,0,0,0.1)";
                    ctx.font = "50px normal";
                    ctx.fillText(json.slogan, 120, 140);
                    ctx.restore();

                    function canvasWxQrcode() {
                        let teamId = self.data.sharData.teamId;
                        let pageUrl = teamId ? 'packageB/pages/detailZj/index' : 'packageB/pages/dynamicDetail/index';
                        let aa;
                        if (self.data.sharData.releaseId) {
                            aa = parseInt(self.data.sharData.releaseId) + "&" + 0
                        }
                        if (self.data.sharData.confessId) {
                            aa = parseInt(self.data.sharData.confessId) + "&" + 1
                        }
                        if (teamId) {
                            aa = parseInt(teamId)
                        }
                        wx.request({
                            url: app.apiUrl + '/wx/getwxacodeUrl',
                            data: {
                                page: pageUrl,
                                // scene: parseInt(self.data.sharData.releaseId) & 0 || teamId || self.data.sharData.confessId & 1,
                                scene: aa,
                                width: 300
                            },
                            header: {
                                'content-type': 'application/json'
                            },
                            method: 'POST',
                            dataType: 'json',
                            success: function (res) {

                                const qrcodImg = canvas.createImage();
                                qrcodImg.src = app.apiUrlimg + res.data.data;
                                qrcodImg.onload = () => {
                                    ctx.drawImage(qrcodImg, 70, imgTopPos+305, 130, 130);
                                }


                                //绘制完成
                                setTimeout(() => {
                                    wx.canvasToTempFilePath({
                                        canvas: canvas,
                                        success: function (res) {
                                            //设置最终canvas高度

                                            let tempFilePath = res.tempFilePath
                                            self.setData({
                                                canvasImg: tempFilePath,
                                                showCanvas: true,
                                            })
                                        },
                                        fail: function (err) {
                                            wx.showToast({
                                                title: '网络出错',
                                                icon: 'fail',
                                                duration: 2000
                                            })
                                            self.setData({
                                                showCanvas: false,
                                                maskHidden: false,
                                                canvasImg: ''
                                            })
                                        }
                                    })
                                    wx.hideLoading();
                                }, 1000)
                            },
                            fail: function () {
                                wx.hideLoading();
                                wx.showToast({
                                    title: '网络出错',
                                    icon: 'fail',
                                    duration: 2000
                                })
                                self.setData({
                                    showCanvas: false,
                                    maskHidden: false,
                                    canvasImg: ''
                                })
                            },
                        });
                    }

                    canvasWxQrcode();


                });
        },

        getPhotosAuthorize: function () {
            let self = this;
            wx.getSetting({
                success(res) {
                    console.log(res)
                    if (!res.authSetting['scope.writePhotosAlbum']) {
                        wx.authorize({
                            scope: 'scope.writePhotosAlbum',
                            success() {
                                console.log('授权成功')
                                self.saveImg();
                            },
                            //用户拒绝
                            fail() {
                                console.log("用户再次拒绝")
                            }
                        })
                    } else {
                        self.saveImg();
                    }
                }
            })
        },
        /**
         * 保存到相册
         */
        async saveImg() {
            let self = this;
            const query = this.createSelectorQuery();
            const canvasObj = await new Promise((resolve, reject) => {
                query.select('#posterCanvas')
                    .fields({node: true, size: true})
                    .exec(async (res) => {
                        resolve(res[0].node);
                    })
            });
            console.log(canvasObj);
            const ctx = canvasObj.getContext('2d');
            wx.canvasToTempFilePath({
                //fileType: 'jpg',
                //canvasId: 'posterCanvas', //之前的写法
                canvas: canvasObj, //现在的写法
                success: (res) => {
                    console.log(res);
                    self.setData({canClose: true});
                    //保存图片
                    wx.saveImageToPhotosAlbum({
                        filePath: res.tempFilePath,
                        success: function (data) {
                            wx.showToast({
                                title: '已保存到相册',
                                icon: 'success',
                                duration: 2000
                            })
                            ctx.clearRect(0, 0, 320, 410); //清空画板
                            self.setData({
                                showCanvas: false,
                                maskHidden: false,
                                canvasImg: ''
                            })
                            // setTimeout(() => {
                            //   self.setData({show: false})
                            // }, 6000);
                        },
                        fail: (res) => {
                            wx.getSetting({
                                success: res => {
                                    let authSetting = res.authSetting
                                    if (!authSetting['scope.writePhotosAlbum']) {
                                        wx.showModal({
                                            title: '提示',
                                            content: '您未开启保存图片到相册的权限，请点击确定去开启权限！',
                                            success(res) {
                                                if (res.confirm) {
                                                    wx.openSetting()
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                            setTimeout(() => {
                                wx.hideLoading()
                                this.setData({
                                    visible: false
                                })
                            }, 300)
                        },
                        // fail: function (err) {
                        //   console.log(err);
                        //   if (err.errMsg === "saveImageToPhotosAlbum:fail auth deny") {
                        //     console.log("当初用户拒绝，再次发起授权")
                        //     self.getPhotosAuthorize()
                        //   } else {
                        //     util.showToast("请截屏保存分享");
                        //   }
                        // },
                        complete(res) {
                            wx.hideLoading();
                            self.triggerEvent('shareAttr');
                        }
                    })
                },
                fail(res) {
                    self.triggerEvent('shareAttr');
                }
            }, this)


        }

    },

})