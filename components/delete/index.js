const clientWidth = wx.getSystemInfoSync().screenWidth;
const app = getApp();
let x = 0,
    y = 0;
let ctxs = null;
let ctxW = 750;
let ctxH = 1000;
let xs = 0;
Component({
    options: {
        multipleSlots: true,
        addGlobalClass: true
    },
    properties: {
        title: {
            type: String,
            value: ''
        },
        showActionsheet: {
            type: Boolean,
            value: false
        },
        actions: {
            type: Array,
            value: [],
            observer: '_groupChange'
        },
        goodsDetail: {
            type: Object
        },
        deleteData: {
            type: Object
        },

    },
    data: {

    },
    methods: {
        _closeActionSheet(e) {
            this.setData({
                showActionsheet: false,
            });
          this.triggerEvent('listChange')
        },
        delete(){
          if (!wx.getStorageSync('token')) {
            wx.navigateTo({
              url: '/pages/author/index'
            })
          }else{
            debugger
            let that = this;
            let deleteData = that.properties.deleteData;
            //动态1
            if (deleteData.type == 1) {
              var params = {};
              let url = app.apiUrl + '/release/delete/' + deleteData.id;
              app.request.requestPostApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'), deleteData)
            }
            //表白2
            if (deleteData.type == 2) {
              var params = {};
              let url = app.apiUrl + '/confess/delete/' + deleteData.id;
              app.request.requestPostApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'), deleteData)
            }
            //颜值3
            if (deleteData.type == 3) {
              var params = {};
              let url = app.apiUrl + '/face/delete/' + deleteData.id;
              app.request.requestPostApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'), deleteData)
            }
            //组队4
            if (deleteData.type == 4) {
              var params = {};
              let url = app.apiUrl + '/team/delete/' + deleteData.id;
              app.request.requestPostApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'), deleteData)
            }
            //评论5
            if (deleteData.type == 5) {
              var params = {};
              let url = app.apiUrl + '/release/delete_comment/' + deleteData.id;
              app.request.requestPostApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'), deleteData)
            }
          }
            
        },
      collect(e){
        debugger
        if (!wx.getStorageSync('token')) {
          wx.navigateTo({
            url: '/pages/author/index'
          })
        } else {
          let that = this;
          let deleteData = that.properties.deleteData;
          let type = e.currentTarget.dataset.type;
          let params = {
            typeId: deleteData.id,
            status: false,
            type: deleteData.type
          };

          let url = app.apiUrl + '/user/collect';
          app.request.requestPostApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'), deleteData)
        }
      },
        complaint(e) {
            if (!wx.getStorageSync('token')) {
              wx.navigateTo({
                url: '/pages/author/index'
              })
            }else{
              let that = this;
              let deleteData = that.properties.deleteData;
              let type = e.currentTarget.dataset.type;
              //动态1
              let objectType = deleteData.type == 1 ? 0 : deleteData.type == 2 ? 1 : deleteData.type == 5 ? 2 : deleteData.type == 4 ? 3 : null;
              let params = {
                objectId: deleteData.id,
                objectType: objectType,
                schoolId: wx.getStorageSync('schoolId'),
                type: type
              };

              let url = app.apiUrl + '/complaint/create/';
              app.request.requestPostApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
            }
        },
        successFun: function (res, selfObj, deleteData) {
            wx.showToast({
                title: '删除成功',
                icon: 'success',
                duration: 3000
            });
            selfObj.triggerEvent('listChange', deleteData);
        },
        successFun1: function (res, selfObj) {
            wx.showToast({
                title: '投诉成功',
                icon: 'success',
                duration: 3000
            });
            selfObj.triggerEvent('listChange');
            
            selfObj.setData({
                showActionsheet: false,
            });
        },
    }
})