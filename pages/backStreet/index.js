//index.js
//获取应用实例
import {diaplayTime, lookImgs, tongZhi} from '../../utils/util';

const app = getApp();

Page({
    data: {
        isIphoneX: app.globalData.isIphoneX,
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 1000,
        swiperCurrent: 0,
        selectCurrent: 0,
        noramalData: [],
        ownerData: [],
        leftList: [],
        rightList: [],
        leftHight: 0,
        rightHight: 0
    },


    onShareAppMessage: function onShareAppMessage(res) {

        if (res.from === 'button') {
            console.log(res.target)
        }
        let pic;
        this.setData({
            show: false
        })
        if (this.data.sharData.releaseFiles.length > 0) {
            if (this.data.sharData.releaseFiles[0].coverRelativePath) {
                pic = app.apiUrlimg + this.data.sharData.releaseFiles[0].coverRelativePath;
            } else {
                pic = app.apiUrlimg + this.data.sharData.releaseFiles[0].fileRelativePath;
            }
        }
        return {
            title: this.data.sharData.contentText,
            path: this.data.sharData.releaseFiles[0] && this.data.sharData.releaseFiles[0].radio > 1 && this.data.sharData.releaseFiles[0].bizFileType == 2 ? '/packageB/pages/videoDetail/index?id=' + this.data.sharData.releaseId + '&type=0&sence=1' : '/packageB/pages/dynamicDetail/index?id=' + this.data.sharData.releaseId + '&type=0&sence=1',
            imageUrl: pic ? pic : '../../images/home/logo.png',
            success: function (res) {
                // 转发成功之后的回调
                if (res.errMsg == 'shareAppMessage:ok') {
                    console.log(1)
                }
            },
        }
    },
    goInfo() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
        } else {
            wx.navigateTo({
                url: '/pages/proInfor/index'
            })
        }
    },
    goBaidu(e) {
        debugger
        let id = e.currentTarget.dataset.id;
        let name = e.currentTarget.dataset.name;
        wx.navigateTo({
            url: '/pages/backStreetList/index?id=' + id + '&name=' + name
        })
    },
    gojiaoxiao(e) {
        debugger
        wx.navigateTo({
            url: '/pages/backStreetList/index?id=303&name=驾校'
        })
    },
    gozufang(e) {
        debugger
        wx.navigateTo({
            url: '/pages/backStreetList/index?id=301&name=租房'
        })
    },
    goDetail(e) {
        let id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/packageB/pages/backStreetDetail/index?id=' + id
        })
    },
    onShow: function () {
        let that = this;
        if (typeof this.getTabBar === 'function' &&
            that.getTabBar()) {
            that.getTabBar().setData({
                selected: 1
            })
        }
        that.getBanner()
        that.getTopic()
        that.getStreet()
        // that.getOwenList()
    },
    onLoad() {
        let that = this;

    },

    getBanner() {
        let that = this;
        let url = app.apiUrl + '/index/banner/';
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFunBanner, that.failFun)
    },
    successFunBanner: function (res, selfObj) {
        selfObj.setData({
            banner: res.data,
        })
    },
    getOwenList() {
        let that = this;
        app.httpGet({
            url: '/back/street/good_house_sort_list?schoolId=' + wx.getStorageSync('schoolId') + '&topicTypeId=303',
            loading: true
        }).then((res) => {
            that.setData({
                ownerData: res.data,
            })
        })
    },
    getStreet() {
        let that = this;
        that.setData({
            noramalData:[],
        })
        app.httpGet({
            url: '/back/street/good_street_random_list?schoolId=' + wx.getStorageSync('schoolId'),
            loading: true
        }).then((res) => {
            debugger
            that.setData({
                noramalData: res.data,
            })
            if(res.data.length==0){
                wx.showModal({
                    title: '提示',
                    content: '本校暂未开通后街，欢迎有想法的同学加入我们！',
                    confirmText:'联系电话',
                    showCancel:false,
                    success(res) {
                        if (res.confirm) {
                            wx.makePhoneCall({
                                phoneNumber:'13396550802'
                            })
                        }
                    }
                })
            }else{
                that.imgList()
            }
        })
    },
    imgList() {
        var that = this;
        var allData = that.data.noramalData;
        //定义两个临时的变量来记录左右两栏的高度，避免频繁调用setData方法
        var leftH = that.data.leftHight;
        var rightH = that.data.rightHight;
        var leftData = [];
        var rightData = [];
        var leftHight = 0, rightHight = 0, itemWidth = 0, maxHeight = 0;
        wx.getSystemInfo({
            success: (res) => {
                let percentage = 750 / res.windowWidth;
                let margin = 2 / percentage;
                itemWidth = (res.windowWidth - margin) / 2;//计算 瀑布流展示的宽度
                maxHeight = 400//计算瀑布流的最大高度，防止长图霸屏 maxHeight = itemWidth / 0.8
            }
        });
        for (let i = 0; i < allData.length; i++) {
            let tmp = allData[i];
            tmp.width = tmp.releaseFiles.length > 0 ? parseInt(tmp.releaseFiles[0].width) : 0;
            tmp.height = tmp.releaseFiles.length > 0 ? parseInt(tmp.releaseFiles[0].height) : 0;
            tmp.itemWidth = 343
            let per = tmp.width / tmp.itemWidth;
            tmp.itemHeight = tmp.height / per;
            if (tmp.itemHeight > maxHeight) {
                tmp.itemHeight = maxHeight;
            }
            if (leftH == rightH || leftH < rightH) {//判断左右两侧当前的累计高度，来确定item应该放置在左边还是右边
                leftData.push(tmp);
                leftH += tmp.itemHeight;
            } else {
                rightData.push(tmp);
                rightH += tmp.itemHeight;
            }
        }


        //更新左右两栏的数据以及累计高度
        that.setData({
            leftHight: leftH,
            rightHight: rightH,
            leftList: leftData,
            rightList: rightData
        })
    },
    getTopic() {
        app.httpGet({
            url: '/index/topic_type_list?type=2',
            loading: false
        }).then((res) => {
            this.setData({
                topic: res.data,
            })
        })
    }

})
