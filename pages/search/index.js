//index.js
//获取应用实例
import {diaplayTime, lookImgs, tongZhi} from '../../utils/util';

const app = getApp();

Page({
    data: {

        load: true,
        homeData: {},

        type: 0,
        topicTypeId: '',
        show: false,
        showAll: false,
        showAuth: false,
        resh: false,
        school: [],
        nowSchool: '',
        params: {
            topicTypeId: '',
            pageNum: 1,
        },
        navbarHeight: 0,
        sharData: {},
        nickName: '',
        scrollTop: null,
        showLoading: false,
        lodaing: false,
        searchInput: '',
      searchIng:false
    },

    listenerSearchInput(e) {
      if (this.data.searchIng && !e.detail.value){
        this.setData({
          searchIng:false,
          homeData:{}
        });
      }
        this.setData({
            searchInput: e.detail.value,
        });
    },
    goSearch(e){
      let that=this;
      let key=e.currentTarget.dataset.key;
        that.setData({
          showLoading: true,
          homeData: {},
          searchInput: key,
        })
        let url = app.apiUrl + '/release/list';
        var params = {
          topicTypeId: '',
          pageNum: 1,
          schoolId: wx.getStorageSync('schoolId'),
          key: key
        };
        app.request.requestGetApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
    },
    Search(){
        let that = this;
        if (that.data.searchInput.length == 0) {
            return
        } else {
          that.setData({
            showLoading: true,
            homeData:{}
          })
            let url = app.apiUrl + '/release/list';
            var params = {
                topicTypeId: '',
                pageNum: 1,
                schoolId: wx.getStorageSync('schoolId'),
                key:that.data.searchInput
            };
            app.request.requestGetApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
        }
    },

    lookImg(e) {
        lookImgs(e)
    },

    lookVideo(e) {
        wx.navigateTo({
            url: '../../packageB/pages/videoDetail/index'
        })
    },

    isAuth() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
    },


    bindTouchStart: function (e) {
        this.startTime = e.timeStamp;
    },
    bindTouchEnd: function (e) {
        this.endTime = e.timeStamp;
    },
    bingLongTap: function (e) {
        console.log("长按");
        wx.setClipboardData({
            data: e.currentTarget.dataset.txt,
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                });
            }
        });
    },
    goDetail(e) {
        if (this.endTime - this.startTime < 350) {
            let releaseId = e.currentTarget.dataset.id;
            wx.navigateTo({
                url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=0'
            })
        }

    },


    goUserDetail(e) {
        let userId = e.currentTarget.dataset.userid;
        let deleteFlag = e.currentTarget.dataset.deleteflag;
        wx.navigateTo({
            url: '/packageB/pages/logs/logs?userId=' + userId + '&deleteFlag=' + deleteFlag
        })
    },

    isAuth() {
        if (!app.globalData.token && !wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
    },
    goVDetail() {
        wx.navigateTo({
            url: '/packageB/pages/official/index?id=160&type=0'
        })
    },
    onReachBottom: function () {
        var that = this;
        if (that.data.load) {
            if (that.data.params.pageNum < that.data.homeData.pages) {
                that.setData({
                    'params.pageNum': that.data.params.pageNum * 1 + 1,
                    lodaing: true
                })
                that.loadMore();
            }
        }
    },


    successFunEdit(res, selfObj) {
        selfObj.setData({
            showAll: false,
            showAuth: false
        });
    },


    onLoad() {
        this.setData({
            schoolId: wx.getStorageSync('schoolId')
        })
    },


    successFun1: function (res, selfObj) {
        res.data.list && res.data.list.forEach(item => {
            let d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
            item.releaseTime = diaplayTime(d);
            item.userAvatarUrl=item.userAvatarUrl.indexOf('https')>-1?item.userAvatarUrl:app.apiUrlimg+item.userAvatarUrl
            if (item.releaseFiles.length == 1) {
                item.releaseFiles.forEach(good => {
                    good.radio = good.width / good.height;
                });
            }
        });
        selfObj.setData({
            homeData: res.data,
            scrollLeft: 0,
            showLoading: false,
            resh:false,
          searchIng:true
        })
    },
    onShow: function () {
        // wx.pageScrollTo({
        //     scrollTop: 0,
        // })
        this.getTop()
    },

    getTop() {
        let that = this;
        let url = app.apiUrl + '/index/get_search_key_list';
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'))
    },
    successFun: function (res, selfObj) {
        selfObj.setData({
            nav: res.data,
            showLoading:false
        })
    },
    onHide() {

    },


    loadMore() {
        let that = this;
        let url = app.apiUrl + '/release/list';
        var params = {
            pageNum: that.data.params.pageNum,
            pageSize: app.globalData.pageSize,
            topicTypeId: that.data.topicTypeId,
            schoolId: wx.getStorageSync('schoolId')
        };
        app.request.requestGetApi(url, params, that, that.successFun2, that.failFun, wx.getStorageSync('token'))
    },




    successFun2: function (res, selfObj) {
        let that = this;
        res.data.list.forEach(item => {
            let d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
            item.userAvatarUrl=item.userAvatarUrl.indexOf('https')>-1?item.userAvatarUrl:app.apiUrlimg+item.userAvatarUrl
            item.releaseTime = diaplayTime(d);
            if (item.releaseFiles.length == 1) {
                item.releaseFiles.forEach(good => {
                    good.radio = good.width / good.height;
                });
            }
        });
        var content = that.data.homeData.list.concat(res.data.list)
        selfObj.setData({
            'homeData.list': content,
            lodaing: false
        });
    },


    failFun: function (res, selfObj) {
        selfObj.setData({
            resh: false
        });
        console.log('failFun', res)
    },

    
    getUserInfo: function (e) {
        this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: true
        })
    },
    delete(e) {
        let type = e.currentTarget.dataset.type;
        let id = e.currentTarget.dataset.id;
        let deleteFlag = e.currentTarget.dataset.deleteflag
        this.setData({
            show1: true,
            deleteData: {
                type: type,
                id: id,
                deleteFlag: deleteFlag
            }
        })
    },
})
