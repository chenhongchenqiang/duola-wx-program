const app = getApp();
import {diaplayTime, lookImgs, tongZhi} from '../../utils/util';

let pageStart = 1;
let loadAgin;
Page({
    data: {
        isIphoneX: app.globalData.isIphoneX,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        duration: 300,  // swiper-item 切换过渡时间
        categoryCur: 0, // 当前数据列索引
        categoryMenu: [], // 分类菜单数据, 字符串数组格式
        categoryData: [], // 所有数据列
        listData: null,
        hotList: [],// 热帖
        hotList_left: [],// 热帖left
        hotList_right: [],// 热帖right
        show: false,//分享组件显示控制
        showMask: false,
        nickName: '',
        sharData: {},
        message: 0,
        school: [],
        topData: {},
        nowSchool: '',
        showAll: false,
        tabBarHeight: 0,
        successShow: false,
        noMore: false,
        currentPage: 1,
        totalPage: 0,
        topicTypeId: '',
        typeNav: false,
        drawStatus: 0
    },
    onPullDownRefresh: function () {
        if (this.data.showAll) {
            wx.stopPullDownRefresh()
            return
        }
        wx.showNavigationBarLoading(); //在标题栏中显示加载图标
        this.setData({
            noMore: false,
            currentPage: 1,
            totalPage: 0,
        })
        this.getList('refresh');
    },

    onReachBottom: function (e) {
        if (this.data.currentPage < this.data.totalPage && !this.data.noMore) {
            this.setData({
                currentPage: this.data.currentPage + 1
            }, () => {
                this.getList()
            })
        }
        // if(!this.data.noMore){
        //     this.getList();
        // }
    },
    getList(type) {
        debugger
        let releaseId = this.data.listData && this.data.listData.length > 0 && type != 'refresh' ? this.data.listData[this.data.listData.length - 1].releaseId : '';
        app.httpGet({
            url: '/release/list?topicTypeId=' + this.data.topicTypeId + '&pageNum=' + this.data.currentPage + '&pageSize=' + app.globalData.pageSize + '&schoolId=' + wx.getStorageSync('schoolId') + '&releaseId=' + releaseId,
            token: wx.getStorageSync('token'),
            loading: true
        }).then((res) => {
            if (type == 'refresh') {
                wx.hideNavigationBarLoading(); //完成停止加载图标
                wx.stopPullDownRefresh();
                setTimeout(() => {
                    this.setData({
                        successShow: true,
                    });
                    setTimeout(() => {
                        this.setData({
                            successTran: true,
                        });
                        setTimeout(() => {
                            this.setData({
                                successShow: false,
                                successTran: false,
                            });
                        }, 350)
                    }, 1500)
                }, 600)
            }
            this.setData({
                totalPage: res.data.pages
            })

            res.data.list && res.data.list.forEach(item => {
                let d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
                item.releaseTime = diaplayTime(d);
                if (item.comments && item.comments.length > 0) {
                    item.comments && item.comments.forEach(items => {
                        items.content = items.content && items.content.replace(/\n|\r/g, "");
                        if (items.childrens.length > 0) {
                            items.childrens.slice(0, 2).forEach(items1 => {
                                items1.content = items1.content.replace(/\n|\r/g, "");
                            })
                        }
                    })
                }

                item.userAvatarUrl = item.userAvatarUrl.indexOf('https') > -1 ? item.userAvatarUrl : app.apiUrlimg + item.userAvatarUrl
                if (item.releaseFiles.length == 1) {
                    item.releaseFiles.forEach(good => {
                        good.radio = good.width / good.height;
                    });
                } else {
                    item.releaseFiles.sort(function (a, b) {
                        return (a.order - b.order)
                    });
                }
                ;
                item.comments && item.comments.forEach(items => {
                    items.avatarUrl = items.avatarUrl.indexOf('https') > -1 ? items.avatarUrl : app.apiUrlimg + items.avatarUrl
                });
            });
            let listData = this.data.currentPage > 1 ? this.data.listData.concat(res.data.list) : res.data.list;
            this.setData({
                listData: listData
            }, () => {
                this.hasMore(res)
            })
        });
        wx.setStorageSync('indexChanged', false);
    },
    hasMore(res) {
        if (res.data.list && res.data.pages == this.data.currentPage) {
            this.setData({
                noMore: true
            })
        }
    },
    // 更新页面数据
    setCurrentData(listData) {
        this.setData({
            listData: listData
        })
    },
    // 获取当前激活页面的数据
    getCurrentData() {
        return this.data
    },
    // 顶部tab切换事件
    toggleCategory(e) {
        debugger
        this.setData({
            listData: null,
            duration: 0,
            topicTypeId: e.detail.item.id,
            categoryCur: e.detail.index,
            noMore: false,
            currentPage: 1,
            totalPage: 0,
            typeNav: false
        });
        this.getList()
    },
    // 页面滑动切换事件
    animationFinish(e) {
        console.log(1313)

        this.setData({
            duration: 300
        });
        setTimeout(() => {
            this.setData({
                categoryCur: e.detail.current
            });
            let pageData = this.getCurrentData();
            if (pageData.listData.length === 0) {
                this.getList('refresh', pageStart);
            }
        }, 0);
    },
    // 刷新数据
    refresh() {
        this.getList('refresh', pageStart);
    },
    // 加载更多
    more() {
        this.getList('more', this.getCurrentData(this.data.categoryCur).page);
    },
    showArticle(e) {
        wx.setClipboardData({
            data: e.currentTarget.dataset.link,
            success(res) {
                wx.showToast({
                    icon: "none",
                    title: "链接已复制到剪切板"
                })
            }
        })
        // wx.navigateTo({
        // 	url: `/pages/swipe-list/webview/index?link=${e.currentTarget.dataset.link}`
        // })
    },
    //查看图片
    lookImg(e) {
        debugger
        loadAgin = true;
        lookImgs(e)
    },
    // 豚榜详情
    goTopDetail(e) {

        let releaseId = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=0'
        })
    },
    // 豚榜more
    goHotDetail(e) {
        wx.navigateTo({
            url: '/packageB/pages/hotList/index'
        })

    },

    //长按复制
    bindTouchStart: function (e) {
        this.startTime = e.timeStamp;
    },
    bindTouchEnd: function (e) {
        this.endTime = e.timeStamp;
    },
    bingLongTap: function (e) {
        console.log("长按");
        wx.setClipboardData({
            data: e.currentTarget.dataset.txt,
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                });
            }
        });
    },
    // 列表详情
    goDetail(e) {
        if (this.endTime - this.startTime < 350) {
            console.log("点击")
            let releaseId = e.currentTarget.dataset.id;
            let bizFileType = e.currentTarget.dataset.bizfiletype;
            let radio = e.currentTarget.dataset.radio;
            wx.navigateTo({
                url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=0'
            })
            // if (bizFileType == 2 && radio > 1) {
            //     wx.navigateTo({
            //         url: '/packageB/pages/videoDetail/index?id=' + releaseId + '&type=0'
            //     })
            // } else {
            //     wx.navigateTo({
            //         url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=0'
            //     })
            // }
        }
    },
    //用户详情
    goUserDetail(e) {
        let fakeFlag = e.currentTarget.dataset.fakeflag;
        let userId = e.currentTarget.dataset.userid;
        let deleteFlag = e.currentTarget.dataset.deleteflag;
        if(fakeFlag){
            wx.showToast({
                title: '该用户为匿名用户',
            });
        }else{
            wx.navigateTo({
                url: '/packageB/pages/logs/logs?userId=' + userId + '&deleteFlag=' + deleteFlag
            })
        }

    },
    //投诉分享
    controlMore(e) {
        debugger
        let listData = this.data.listData;
        let typeId = e.currentTarget.dataset.typeid;
        let flag = e.currentTarget.dataset.flag;
        listData && listData.forEach(item => {
            if (item.releaseId == typeId) {
                item.showMore = flag == 0 ? true : false;
            }
        });
        this.setData({
            showMask: true,
        })
        this.setCurrentData(listData);
    },
    goInfo() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
        } else {
            wx.navigateTo({
                url: '/pages/proInfor/index'
            })
        }
    },
    //海报分享
    share(e) {
        debugger
        let listData = this.data.listData;
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            listData && listData.forEach(item => {
                if (item.releaseId == e.currentTarget.dataset.item.releaseId) {
                    item.showMore = false;
                }
            });
            let sharData = e.currentTarget.dataset.item;
            this.setData({
                show: true,
                sharData: sharData,
                showMask: false,
            })
            this.setCurrentData(listData);
        }
    },
    //好友分享
    onShareAppMessage: function onShareAppMessage(res) {
        if (res.from === 'button') {
            console.log(res.target)
        }
        let pic;
        this.setData({
            show: false
        })
        if (this.data.sharData.releaseFiles.length > 0) {
            if (this.data.sharData.releaseFiles[0].coverRelativePath) {
                pic = app.apiUrlimg + this.data.sharData.releaseFiles[0].coverRelativePath;
            } else {
                pic = app.apiUrlimg + this.data.sharData.releaseFiles[0].fileRelativePath;
            }
        }
        return {
            title: this.data.sharData.contentText,
            path: this.data.sharData.releaseFiles[0] && this.data.sharData.releaseFiles[0].radio > 1 && this.data.sharData.releaseFiles[0].bizFileType == 2 ? '/packageB/pages/videoDetail/index?id=' + this.data.sharData.releaseId + '&type=0&sence=1' : '/packageB/pages/dynamicDetail/index?id=' + this.data.sharData.releaseId + '&type=0&sence=1',
            imageUrl: pic ? pic : '../../images/home/logo.png',
            success: function (res) {
                // 转发成功之后的回调
                if (res.errMsg == 'shareAppMessage:ok') {
                    console.log(1)
                }
            },
        }
    },
    //点赞
    zan(e) {
        debugger
        let that = this;
        let obj = e.currentTarget.dataset.item;
        let type = 0;
        let typeId = obj.releaseId;
        let likeFlag = obj.likeFlag;
        let userId = obj.userId;
        let contentText = obj.contentText;
        let listData = that.data.listData;
        if (wx.getStorageSync('userInfo')) {
            that.setData({
                nickName: wx.getStorageSync('userInfo').nickName
            })
        }
        ;
        var params = {
            status: likeFlag ? false : true,
            type: type,
            typeId: typeId,
        };
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            app.httpPost({
                url: "/user/like", data: params, token: wx.getStorageSync('token')
            }).then((res) => {
                listData && listData.forEach(item => {
                    if (item.releaseId == typeId) {
                        item.likeFlag = !likeFlag;
                        item.likes = !likeFlag ? item.likes + 1 : item.likes - 1
                    }
                });
                that.setCurrentData(listData);
                if (!likeFlag) {
                    that.tongZhi({
                        type: type,
                        typeId: typeId,
                        status: !likeFlag,
                        userId: userId,
                        contentText: contentText,
                        nickName: that.data.nickName
                    })
                }
            });
        }
    },
    tongZhi(data) {
        let that = this;
        let url = app.apiUrl + '/index/get_openid/' + data.userId;
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFun10, that.failFun, wx.getStorageSync('token'), data)
    },
    successFun10(res, selfObj, data) {
        tongZhi(res, selfObj, data)
    },
    //管理员删帖
    deleteRe(e){
        let that=this;
        let id=e.currentTarget.dataset.id;
        wx.showModal({
            title: '提示',
            content: '是否删除该帖',
            confirmText: '确认',
            showCancel: true,
            success: (res) => {
                if (res.confirm) {
                    var params = {};
                    let url = app.apiUrl + '/release/delete/' + id;
                    app.request.requestPostApi(url, params, that, that.successFunDelete, that.failFun, wx.getStorageSync('token'))
                }
            }
        })
    },
    successFunDelete(){
        this.setData({
            showMask: false,
        })
        wx.showToast({
            title: '删除成功',
            icon: 'success',
            duration: 2000
        })
    },
    //举报
    delete(e) {
        let listData = this.data.listData;e
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            let type = e.currentTarget.dataset.type;
            let id = e.currentTarget.dataset.id;
            let deleteFlag = e.currentTarget.dataset.deleteflag
            listData && listData.forEach(item => {
                if (item.releaseId == id) {
                    item.showMore = false;
                }
            });
            this.setData({
                show1: true,
                deleteData: {
                    type: type,
                    id: id,
                    deleteFlag: deleteFlag
                },
                showMask: false,
            })
            this.setCurrentData(listData);
        }
    },
    //蹲
    listen(e) {
        let that = this;
        let obj = e.currentTarget.dataset.item;
        let typeId = obj.releaseId;
        let listenFlag = obj.listenFlag;
        let listData = this.data.listData;
        if (wx.getStorageSync('userInfo')) {
            that.setData({
                nickName: wx.getStorageSync('userInfo').nickName
            })
        }
        ;
        var params = {
            status: listenFlag ? 0 : 1,
            userId: '',
            eventId: parseInt(typeId),
        };
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            app.httpPost({
                url: "/message/listen", data: params, token: wx.getStorageSync('token')
            }).then((res) => {
                listData && listData.forEach(item => {
                    if (item.releaseId == typeId) {
                        item.listenFlag = !listenFlag;
                    }
                });
                that.setCurrentData(listData);
            });
        }
    },
    //搜索
    goCheckList() {
        wx.navigateTo({
            url: '../../pages/search/index'
        })
    },
    //顶部nav
    changeItem(e) {
        debugger
        let type = e.currentTarget.dataset.type;
        if (type == 2) {
            wx.navigateTo({
                url: "/pages/team/index"
            })
        }
        if (type == 1) {
            this.setData({
                typeNav: !this.data.typeNav
            })
        }
    },
    hideNav() {
        this.setData({
            typeNav: false
        })
    },
    //消息中心
    goMessage() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            wx.navigateTo({
                url: '/packageB/pages/message/index'
            })
        }
    },
    goDraw() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            wx.navigateTo({
                url: '/pages/outer/index?type=draw'
            })
        }
    },
    //首次获取学校
    goCheck(e) {
        let schoolId = e.currentTarget.dataset.id;
        let schoolName = e.currentTarget.dataset.schoolname;
        if (schoolId) {
            wx.setStorageSync('schoolId', schoolId);
            wx.setStorageSync('schoolName', schoolName);
            this.setData({
                showAll: false,
            })
        }
        this.common();
    },
    //获取token
    getToken() {
        debugger
        let userInfo;
        if (wx.getStorageSync('userInfo')) {
            userInfo = wx.getStorageSync('userInfo');
        }
        if (this.data.canIUse) {
            app.userInfoReadyCallback = res => {
                if (res.userInfo) {
                    userInfo = res.userInfo
                }
            };
        }
        if (userInfo) {
            this.login(userInfo);
        } else {
            this.common()
        }
    },
    //获取token
    login(userInfo) {
        debugger
        let that = this;
        if (userInfo.userId) {
            wx.setStorageSync('myUserId', e.detail.userInfo.userId);
        }
        wx.login({
            success: res => {
                if (res.code) {
                    app.globalData.code = res.code;
                    app.httpPost({
                        url: "/wx/login", data: {code: res.code, userInfo: userInfo}
                    }).then((res) => {
                        app.globalData.token = res.data.token ? res.data.token : '';
                        app.globalData.userInfo = res.data.userInfo ? res.data.userInfo : '';
                        wx.setStorageSync('token', res.data.token);
                        wx.setStorageSync('userType', res.data.userInfo.userType);
                        wx.setStorageSync('userInfo', res.data.userInfo);
                        wx.setStorageSync('myUserId', res.data.userInfo.userId);
                        that.common()
                    });
                }
            }
        })

    },
    onShow: function () {
        let that = this;
        that.setData({
            userType:wx.getStorageSync('userType'),
        })
        if (loadAgin) {
            loadAgin = false;
            return
        } else {
            let listData = that.data.listData;
            if (typeof this.getTabBar === 'function' && that.getTabBar()) {
                that.getTabBar().setData({
                    selected: 0
                })
            }
            if (wx.getStorageSync('token')) {
                app.httpGet({
                    url: "/message/get_status", token: wx.getStorageSync('token')
                }).then((res) => {
                    app.globalData.message = res.data
                    that.setData({
                        message: res.data
                    });
                })
            }
            if (wx.getStorageSync('releaseId2')) {
                listData && listData.forEach(item => {
                    if (item.releaseId == wx.getStorageSync('releaseId2')) {
                        item.listenFlag = wx.getStorageSync('listenFlag');
                        console.log(wx.getStorageSync('listenFlag'))
                    }
                });
                that.setCurrentData(listData);
            }
            if (wx.getStorageSync('releaseId')) {
                debugger
                listData && listData.forEach(item => {
                    if (item.releaseId == wx.getStorageSync('releaseId')) {
                        if (app.globalData.commentId && app.globalData.commentId != null && app.globalData.commentId != undefined) {
                            let newList = item.comments.filter(function (good) {
                                return good.id != app.globalData.commentId
                            })
                            item.comments = newList
                            item.reviews = wx.getStorageSync('reviews');
                        }
                        if (app.globalData.confessContent && app.globalData.confessContent.length > 0) {
                            item.comments = app.globalData.confessContent;
                            item.reviews = wx.getStorageSync('reviews');
                        }
                        if ((item.likeFlag && wx.getStorageSync('likeFlag')) || (!item.likeFlag && !wx.getStorageSync('likeFlag'))) {
                            return
                        }
                        item.likeFlag = wx.getStorageSync('likeFlag');
                        item.likes = wx.getStorageSync('likeFlag') ? item.likes + 1 : item.likes - 1
                    }
                });
                that.setCurrentData(listData);
            }
            if (app.globalData.release || wx.getStorageSync('indexChanged')) {
                debugger
                that.setData({
                    categoryMenu: [],
                    categoryCur: 0,
                    topicTypeId: '',
                    hotList: [],
                    currentPage: 1,
                    hotList_left: [],
                    hotList_right: [],
                    listData: null,
                    noMore: false
                })
                that.common()
            }
        }
    },
    common() {
        app.httpGet({
            url: "/index/get_prize_draw_status"
        }).then((res) => {
            this.setData({
                drawStatus: res.data,
            })
        });
        app.httpGet({
            url: "/index/topic_type_list?type=0"
        }).then((res) => {
            app.httpGet({
                url: "/release/top_one/" + wx.getStorageSync('schoolId')
            }).then((res1) => {
                let reg = /\n(\n)*( )*(\n)*\n/g;
                let dataTop = res1.data;
                dataTop.contentText = dataTop.contentText.replace(reg, "\n");
                this.setData({
                    topData: dataTop,
                })
            });
            app.httpGet({
                url: "/release/hot_post_list/" + wx.getStorageSync('schoolId')
            }).then((res1) => {
                let hotData = res1.data.filter(item => {
                    return item.contentText.length != 0
                })
                this.setData({
                    hotList: hotData,

                    nowSchool: wx.getStorageSync('schoolName')
                })
            })
            let menus = [{id: "", name: "最新"}].concat(res.data) || [];
            let categoryMenu = [];
            menus.forEach((item, index) => {
                categoryMenu.push(item);
            });

            this.setData({
                categoryMenu,
            });
            // 第一次加载延迟 350 毫秒 防止第一次动画效果不能完全体验
            setTimeout(() => {
                this.refresh();
            }, 350);
        })
    },
    onLoad(query) {
        if (query && query.scene) {
            const querAll = decodeURIComponent(query.scene);
            let scene = query.scene.indexOf('%') === -1 ? {
                id: query.scene,
                type: 10
            } : {
                id: querAll.match(/(\S*)&/)[1],
                type: querAll.match(/&(\S*)/)[1],
            };
            if (scene.type == 0) {
                wx.navigateTo({
                    url: '/packageB/pages/dynamicDetail/index?id=' + scene.id + '&type=' + scene.type
                })
            }
            if (scene.type == 10) {
                wx.navigateTo({
                    url: '/packageB/pages/detailZj/index?id=' + scene.id
                })
            }
        }

        this.setData({
            tabBarHeight: app.globalData.tabBarHeight
        })
        if (!wx.getStorageSync('schoolId')) {
            this.setData({
                showAll: true,
            });
            app.httpGet({
                url: "/school/list"
            }).then((res) => {
                this.setData({
                    school: res.data,
                })
            })
        } else {
            if (!wx.getStorageSync('token')) {
                this.getToken();
            } else {
                this.common()
            }
        }
    },

    onHide() {
        app.globalData.release = false;
        wx.setStorageSync('releaseId', '');
        wx.setStorageSync('releaseId2', '');
        wx.setStorageSync('likeFlag', null);
        wx.setStorageSync('listenFlag', null);
        wx.setStorageSync('reviews', '');
        app.globalData.confessContent = {},
            app.globalData.commentId = null
    },
});




