//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        //判断小程序的API，回调，参数，组件等是否在当前版本可用。
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        school: [],
        showAll: false,
        showAuth: false
    },
    onShow() {
        debugger
        let that = this;
        if (!wx.getStorageSync('schoolId')) {
            that.setData({
                showAll: true,
                navbarHeight: app.globalData.navbarHeight
            });
            let url1 = app.apiUrl + '/school/list';
            let params1 = {};
            app.request.requestGetApi(url1, params1, that, that.successFunSchool, that.failFun)
            return
        } else {
            that.setData({
                showAuth: true,
                navbarHeight: app.globalData.navbarHeight
            });
        }
    },
    successFunSchool: function (res, selfObj) {
        //wx.hideLoading();
        selfObj.setData({
            school: res.data,
        })
    },
    goCheck(e) {
        let schoolId = e.currentTarget.dataset.id;
        let schoolName = e.currentTarget.dataset.schoolname;
        if (schoolId) {
            wx.setStorageSync('schoolId', schoolId);
            wx.setStorageSync('schoolName', schoolName);
            this.setData({
                showAll: false,
                showAuth: true
            })
        }
        ;
    },
    successFun: function (res, selfObj) {
        wx.setStorageSync('indexChanged', true);
        wx.hideLoading();
        wx.navigateBack()
    },
    quxiao() {
        wx.navigateBack()
    },
    failFun: function (res, selfObj) {
        console.log('failFun', res)
    },


    bindGetUserInfoBtn: function (e) {
        debugger
        let that = this;
        if (e.detail.userInfo) {
            wx.showLoading();
            app.globalData.gender = e.detail.userInfo.gender;
            wx.login({
                success: res => {
                    if (res.code) {
                        app.globalData.code = res.code;
                        wx.request({
                            url: app.apiUrl + '/wx/login',
                            method: 'post',
                            data: {
                                code: res.code,
                                userInfo: e.detail.userInfo
                            },
                            success: function (res) {
                                debugger
                                if(res.data.code==200){
                                    if (res.data.data.token) {
                                        app.globalData.token = res.data.data.token;
                                        app.globalData.userInfo = res.data.data.userInfo;
                                        wx.setStorageSync('userType', res.data.data.userInfo.userType);
                                        wx.setStorageSync('token', res.data.data.token)
                                        wx.setStorageSync('userInfo', res.data.data.userInfo)
                                    }

                                    let url = app.apiUrl + '/user/update_user_info';
                                    let params =
                                        {
                                            constellation: '',
                                            grade: '',
                                            profession: e.detail.userInfo.profession,
                                            schoolId: wx.getStorageSync('schoolId') ? wx.getStorageSync('schoolId') : '',
                                            school: wx.getStorageSync('schoolName') ? wx.getStorageSync('schoolName') : '',
                                            signature: e.detail.userInfo.signature,
                                        }
                                    app.request.requestPostApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'))
                                }else{
                                    if (res.data.message) {
                                        wx.showToast({
                                            title: res.data.message,
                                            icon: 'none'
                                        });
                                    }
                                }

                            }
                        })
                    }
                }
            })

        } else {
            wx.showToast({
                title: '已拒绝',
                icon: 'none',
                duration: 2000
            })
            setTimeout(function () {
                wx.switchTab({
                    url: '/pages/index/index'
                })
            }, 2000);
        }
    }
})
