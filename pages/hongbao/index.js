const marginHori = 30
const marginVerti = 220

const app = getApp()
import {diaplayTime, lookImgs} from '../../utils/util';

var startPoint;
Page({

    data: {

    },

    onLoad: function (options) {
        wx.setNavigationBarColor({
            frontColor: '#000000',
            backgroundColor: '#ffffff',
        })

    },
    onShareAppMessage: function () {
        return {
            title: '外卖红包',
        }
    },
    onShow: function () {
        let that = this;
        that.getBanner()
        if (typeof this.getTabBar === 'function' &&
            that.getTabBar()) {
            that.getTabBar().setData({
                selected: 3
            })
        }
    },
    getBanner() {
        let that = this;
        let url = app.apiUrl + '/index/banner/';
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFunBanner, that.failFun)
    },
    successFunBanner: function (res, selfObj) {
        selfObj.setData({
            banner: res.data,
        })
    },
    goElem: function () {
        let that=this;
        wx.navigateToMiniProgram({
            appId: 'wxece3a9a4c82f58c9',
            path:that.data.banner[0].detailPath ,
            envVersion: 'release',
            success(res) {
                // 打开成功
            }
        })
    },
    goMeituan: function () {
        let that=this;
        wx.navigateToMiniProgram({
            appId: 'wx2c348cf579062e56',
            path:that.data.banner[1].detailPath,
            // path:'outer_packages/r2xinvite/coupon/coupon?inviteCode=NnOIp-QOs8SiYF1dcSlL5r8phPrCf6qkH7evMyjIoup2NXxNCLYcBbd3bqpv2X2Ir6i4KKgJoM5qmZ9vqVG3uYTD3lJahFVRoSB0MTvfl2ggH54Om07Oo5SfNlPz0efI0Cx2SeC6bskw8_hcullip1qpBYEo-dhVftXalUfOjX0',
            // path: 'index/pages/h5/h5?noshare=1&f_userId=0&f_openId=0&f_token=0&weburl=https%3A%2F%2Fact.meituan.com%2Fclover%2Fpage%2Fadunioncps%2Fshare_coupon%3FutmSource%3D4531%26utmMedium%3DB07D77B1019B29D3543F9E819D79CD45%26activity%3DOwMkGzn6oK',
            envVersion: 'release',
            success(res) {
                // 打开成功
            }
        })
    },
    isAuth() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
    },


})