var app = getApp();
Page({
    data: {
        link: '',
        linkWallet: '',
    },
    onLoad: function (options) {
        let token = wx.getStorageSync('token');
        if (options) {
          debugger
          let pages = getCurrentPages() //获取加载的页面
          let currentPage = pages[pages.length - 1] //获取当前页面的对象
          let options = currentPage.options //如果要获取url中所带的参数可以查看options
            if (options.type === 'draw') {
                this.setData({
                    link: encodeURI(app.outerUrl + '/draw?token=' + token)
                })
            }
            if (options.type === 'learning') {
                this.setData({
                  link: encodeURI(app.outerUrl + '/learning')
                })
            }
            if (options.type==='aboutUs') {
                this.setData({
                    link: encodeURI(app.outerUrl + '/aboutUs')
                })
            }
            if (options.type === 'rule') {
              this.setData({
                link: encodeURI(app.outerUrl + '/rule')
              })
            }
            if (options.type === 'index') {
              this.setData({
                link: encodeURI(app.outerUrl + options.link)
              })
            }
            if (options.type === 'canvas') {
              debugger
                this.setData({
                  link: encodeURI(app.outerUrl + '/canvas?obj=' + options.obj)
                })
              console.log(encodeURI(app.outerUrl + '/canvas?obj=' + options.obj))
            }
        }
    },
})