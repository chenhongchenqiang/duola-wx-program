const app=getApp();
Page({
    data:{
        content:'',
    },
    onShow(){

    },
    bindinputChange: function(e) {
        console.log(e);
        let that = this
        that.setData({
            content: e.detail.value,
        });
    },
    goRelease(){
        let that=this;
        let url = app.apiUrl + '/feedback/create';
        if(that.data.content.length==0){
            that.setData({
                mask:true
            });
            wx.showToast({
                title: "请填写内容",
                icon: 'none',
                duration: 2000
            });
            setTimeout(function () {
                that.setData({
                    mask:false
                });
            },2000);
            return
        }

        let params = {
            content: that.data.content,
            schoolId: wx.getStorageSync('schoolId'),
            status: true,
        };
        app.request.requestPostApi(url, params, that, that.successFun1, that.failFun,wx.getStorageSync('token'))
    },
    fun(){
        this.setData({
            mask:false
        });
    },
    successFun1: function (res, selfObj) {
        let that=this;
        wx.hideLoading();
        wx.showToast({
          title: "反馈成功",
          icon: 'success',
          duration: 2000
        });
        setTimeout(function () {
          wx.navigateBack()
        }, 2000);
       
    },
    failFun: function (res, selfObj) {
        console.log('failFun', res)
    },
})