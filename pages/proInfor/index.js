//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        userInfo: {},
        school: {},
        index: null,
        index1: null,
        index2: null,
        grade: ['大一', '大二', '大三', '研一', '研二', '研三', '博士', '毕业'],
        constellation: ['白羊座', '金牛座', '双子座', '巨蟹座', '狮子座', '处女座', '天秤座', '天蝎座', '射手座', '摩羯座', '水瓶座', '双鱼座'],
        head: '',
        avatarUrl: '',
        loadAgin: false,
    },
    headimage: function () {
        var _this = this;
        wx.chooseImage({
            count: 1, // 默认9      
            sizeType: ['original', 'compressed'],
            // 指定是原图还是压缩图，默认两个都有      
            sourceType: ['album', 'camera'],
            // 指定来源是相册还是相机，默认两个都有    
            success: function (res) {
                debugger
                // 返回选定照片的本地文件路径tempFilePath可以作为img标签的src属性显示图片 
                //然后请求接口把图片传给后端存到服务器即可       
                _this.setData({
                    head: res.tempFilePaths[0],
                    loadAgin: true
                })
            }
        })
    },
    onShow: function () {
        wx.showLoading({
            mask: true,
        })
        let that = this;
        if (that.data.loadAgin) {
            wx.hideLoading()
            return
        } else {
            let url = app.apiUrl + '/user/base_info';
            var params = {}
            app.request.requestGetApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'))
        }
    },
    successFun: function (res, selfObj) {
        let that = this;
        let url1 = app.apiUrl + '/school/list';
        var params1 = {};
        app.request.requestGetApi(url1, params1, that, that.successFun1, that.failFun)
        wx.hideLoading();
        selfObj.setData({
            userInfo: res.data,
            'userInfo.avatarUrl': res.data.avatarUrl.indexOf('https') > -1 ? res.data.avatarUrl : app.apiUrlimg + res.data.avatarUrl,
        })
        if (that.data.userInfo.constellation) {
            that.data.constellation.findIndex(
                (value, index, arr) => {
                    if (value == that.data.userInfo.constellation) {
                        that.setData({
                            index2: index,
                        })
                    }
                })

        }
        if (that.data.userInfo.grade) {
            that.data.grade.findIndex(
                (value, index, arr) => {
                    if (value == that.data.userInfo.grade) {
                        that.setData({
                            index1: index,
                        })
                    }
                })

        }

    },

    successFun1: function (res, selfObj) {
        let that = this;
        wx.hideLoading();
        selfObj.setData({
            school: res.data,
        })
        if (that.data.userInfo.school) {
            res.data.findIndex(
                (value, index, arr) => {
                    if (value.name == that.data.userInfo.school) {
                        that.setData({
                            index: index,
                        })
                    }
                })

        }
    }
    ,
    failFun: function (res, selfObj) {
        console.log('failFun', res)
    }
    ,
    goIndFor() {
        wx.navigateTo({
            url: '/pages/identification/index'
        })
    },
    bindKeyInput: function (e) {
        this.setData({
            'userInfo.profession': e.detail.value
        })
    }
    ,
    bindKeyInput1: function (e) {
        this.setData({
            'userInfo.nickName': e.detail.value
        })
    },
    bindPickerChange(e) {
        this.setData({
            index: e.detail.value,
        })
    }
    ,
    bindPickerChange1(e) {
        this.setData({
            index1: e.detail.value
        })
    }
    ,
    bindinput: function (e) {
        this.setData({
            'userInfo.signature': e.detail.value,
        });
    }
    ,
    bindPickerChange2(e) {
        this.setData({
            index2: e.detail.value
        })
    }
    ,
    successFun2() {
        wx.hideLoading()
        wx.setStorageSync('schoolId', this.data.school[this.data.index].id);
        wx.setStorageSync('schoolName', this.data.school[this.data.index].name);
        wx.setStorageSync('indexChanged', true);
        wx.setStorageSync('teamChanged', true);
        wx.setStorageSync('confessChanged', true);
        wx.setStorageSync('photosChanged', true);
        wx.switchTab({
            url: '../my/index'
        })
    },
    baocun() {
        debugger
        let that = this;
        if(!that.data.userInfo.nickName){
            wx.showToast({
                title: '昵称不能为空',
                icon: 'fail',
                duration: 2000,
                mask: true
            })
        }else if(!that.data.userInfo.avatarUrl&&!that.data.head){
            wx.showToast({
                title: '头像不能为空',
                icon: 'fail',
                duration: 2000,
                mask: true
            })
        }else{
            wx.showLoading({
                title: '正在保存',
                mask: true
            });
            if (that.data.head) {
                wx.uploadFile({
                    url: app.apiUrl2 + '/document/upload/1/1?code=photo',
                    filePath: that.data.head,
                    formData: {},
                    name: 'fileData',
                    header: {
                        "X-Xnxw-Token": wx.getStorageSync('token'),
                    },
                    success: function (res) {
                        if (JSON.parse(res.data).code == 400) {
                            wx.hideLoading();
                            wx.showToast({
                                title: JSON.parse(res.data).message,
                                icon: 'none'
                            })
                            return
                        } else {
                            that.setData({
                                avatarUrl: (JSON.parse(res.data).data)[0].relativePath
                            })
                            //wx.setStorageSync('headChage', true)
                            that.common()
                        }
                    },

                    fail: function (res) {
                        wx.hideLoading();
                        wx.showModal({
                            title: '错误提示',
                            content: '上传图片失败',
                            showCancel: false,
                            success: function (res) {
                            }
                        })
                    }
                });
            } else {
                that.common()
            }
        }


    },
    common() {
        let that = this;
        let url = app.apiUrl + '/user/update_user_info';
        var params =
            {
                avatarUrl: that.data.avatarUrl ? that.data.avatarUrl : that.data.userInfo.avatarUrl ? that.data.userInfo.avatarUrl : '',
                constellation: that.data.constellation[that.data.index2] || that.data.userInfo.constellation,
                grade: that.data.grade[that.data.index1] || that.data.userInfo.grade,
                nickName: that.data.userInfo.nickName,
                profession: that.data.userInfo.profession,
                school: that.data.index ? that.data.school[that.data.index].name : that.data.userInfo.school,
                schoolId: that.data.school[that.data.index].id,
                signature: that.data.userInfo.signature,
            }
        app.request.requestPostApi(url, params, that, that.successFun2, that.failFun, wx.getStorageSync('token'))
    }

})
