//index.js
//获取应用实例
import {diaplayTime, lookImgs, tongZhi} from '../../utils/util';
let pageStart=1;
const app = getApp();

Page({
    data: {
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 300,
        swiperCurrent: 0,
        swiperCurrent1:0,
        selectCurrent: 0,
        systemInfo: '',
        show1: false,
        nav: [{id: "", name: "最新"}],
        activeIdx: 0,
        moveParams: {
            scrollLeft: 0, // scroll-view滚动的距离,默认为0,因为没有触发滚动
            subLeft: 0, //点击元素距离屏幕左边的距离
            subHalfWidth: 0, //点击元素的宽度一半
            screenHalfWidth: 187.5, //屏幕宽度的一
        },
        load: true,
        homeData: {},
        hotList: [],
          hotList_left:[],
          hotList_right:[],
        banner: [],
        type: 0,
        title: '新鲜事',
        topicTypeId: '',
        show: false,
        showAll: false,
        showAuth: false,
      resh:false,
        school: [],
        nowSchool: '',
        params: {
            topicTypeId: '',
            pageNum: 1,
        },
        navbarHeight: 0,
        sharData: {},
        nickName: '',
        scrollTop: null,
        showLoading: true,
        lodaing: false,
        message:0,
      controlMore:false,
      showMask:false,
    
      
    },

   
    
    
  

    // tapBanner(e) {
    //     let link = e.currentTarget.dataset.link
    //     if (link.indexOf('love') > -1) {
    //         wx.switchTab({
    //             url: '/pages/expandColumn/index',
    //         })
    //     } else {
    //         wx.navigateTo({
    //             url: '/pages/outer/index?type=index&link=' + link
    //         })
    //     }
    // },
    // onPageScroll: function (e) {
    //     this.setData({
    //         scrollTop: e.scrollTop
    //     })
    // },
 
  controlMore(e){
      debugger
    let typeId = e.currentTarget.dataset.typeid;
    let flag = e.currentTarget.dataset.flag;
    this.data.homeData.list && this.data.homeData.list.forEach(item => {
      if (item.releaseId ==typeId) {
        item.showMore =flag==0?true:false;
      }
      return this.data.homeData.list
    });
    this.setData({
        showMask:true,
      'homeData.list': this.data.homeData.list,
    })
  },
    swiperchange: function (e) {
        this.setData({
            swiperCurrent: e.detail.current
        })
    },

    commonNavAttr(e) {
        console.log(e);
        if (e.detail) {
            this.setData({
                commonNavAttr: e.detail
            })
        }
    },
    lookImg(e) {
        lookImgs(e)
    },
    goCheckList(){
        wx.navigateTo({
            url: '../../pages/search/index'
        })
    },
    lookVideo(e) {
        wx.navigateTo({
            url: '../../packageB/pages/videoDetail/index'
        })
    },
    delete(e) {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            let type = e.currentTarget.dataset.type;
            let id = e.currentTarget.dataset.id;
            let deleteFlag = e.currentTarget.dataset.deleteflag
            this.data.homeData.list && this.data.homeData.list.forEach(item => {
                if (item.releaseId ==id) {
                    item.showMore =false;
                }
                return this.data.homeData.list
            });
            this.setData({
                show1: true,
                deleteData: {
                    type: type,
                    id: id,
                    deleteFlag: deleteFlag
                },
                showMask:false,
                'homeData.list': this.data.homeData.list,
            })
        }
    },
    // share(e) {
    //     debugger
    //     if (!wx.getStorageSync('token')) {
    //         wx.navigateTo({
    //             url: '/pages/author/index'
    //         })
    //         return
    //     } else {
    //         this.data.homeData.list && this.data.homeData.list.forEach(item => {
    //             if (item.releaseId ==e.currentTarget.dataset.item.releaseId) {
    //                 item.showMore =false;
    //             }
    //             return this.data.homeData.list
    //         });
    //         let sharData = e.currentTarget.dataset.item;
    //         this.setData({
    //             show: true,
    //             sharData: sharData,
    //             showMask:false,
    //             'homeData.list': this.data.homeData.list,
    //         })
    //     }
    // },
  
    isAuth() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
    },
  goMessage(){
    if (!wx.getStorageSync('token')) {
      wx.navigateTo({
        url: '/pages/author/index'
      })
      return
    } else {
      wx.navigateTo({
        url: '/packageB/pages/message/index'
      })
    }
  },
    // onShareAppMessage: function onShareAppMessage(res) {

    //     if (res.from === 'button') {
    //         console.log(res.target)
    //     }
    //     let pic;
    //     this.setData({
    //         show: false
    //     })
    //     if (this.data.sharData.releaseFiles.length > 0) {
    //         if (this.data.sharData.releaseFiles[0].coverRelativePath) {
    //             pic = app.apiUrlimg + this.data.sharData.releaseFiles[0].coverRelativePath;
    //         } else {
    //             pic = app.apiUrlimg + this.data.sharData.releaseFiles[0].fileRelativePath;
    //         }
    //     }
    //     return {
    //         title: this.data.sharData.contentText,
    //         path: this.data.sharData.releaseFiles[0] && this.data.sharData.releaseFiles[0].radio > 1 && this.data.sharData.releaseFiles[0].bizFileType == 2 ? '/packageB/pages/videoDetail/index?id=' + this.data.sharData.releaseId + '&type=0&sence=1' : '/packageB/pages/dynamicDetail/index?id=' + this.data.sharData.releaseId + '&type=0&sence=1',
    //         imageUrl: pic ? pic : '../../images/home/logo.png',
    //         success: function (res) {
    //             // 转发成功之后的回调
    //             if (res.errMsg == 'shareAppMessage:ok') {
    //                 console.log(1)
    //             }
    //         },
    //     }
    // },
    
    // release() {
    //     wx.navigateTo({
    //         url: '../release/index'
    //     })
    // },
    // choseNav: function (e) {
    //     if (this.data.topicTypeId == e.target.dataset.id) {
    //         return
    //     }
    //     let ele = 'ele' + e.target.dataset.index
    //     this.setData({
    //         activeIdx: e.target.dataset.index,
    //         showLoading: true,
    //         topicTypeId: e.target.dataset.id,
    //         'params.pageNum': 1
    //     });
    //     this.getRect('#' + ele);
    //     this.load(this.data.topicTypeId)
    // },
    // getRect(ele) { //获取点击元素的信息,ele为传入的id
    //     var that = this;
    //     wx.createSelectorQuery().select(ele).boundingClientRect(function (rect) {
    //         let moveParams = that.data.moveParams;
    //         moveParams.subLeft = rect.left;
    //         moveParams.subHalfWidth = rect.width / 2;
    //         that.moveTo();
    //     }).exec()
    // },
    // scrollMove(e) {
    //     let moveParams = this.data.moveParams;
    //     moveParams.scrollLeft = e.detail.scrollLeft;
    //     this.setData({
    //         moveParams: moveParams
    //     })
    // },
    // moveTo: function () {
    //     let subLeft = this.data.moveParams.subLeft;
    //     let screenHalfWidth = this.data.moveParams.screenHalfWidth;
    //     let subHalfWidth = this.data.moveParams.subHalfWidth;
    //     let scrollLeft = this.data.moveParams.scrollLeft;

    //     let distance = subLeft - screenHalfWidth + subHalfWidth;

    //     scrollLeft = scrollLeft + distance;

    //     this.setData({
    //         scrollLeft: scrollLeft
    //     })
    // },

    // bindTouchStart: function (e) {
    //     this.startTime = e.timeStamp;
    // },
    // bindTouchEnd: function (e) {
    //     this.endTime = e.timeStamp;
    // },
    // bingLongTap: function (e) {
    //     console.log("长按");
    //     wx.setClipboardData({
    //         data: e.currentTarget.dataset.txt,
    //         success: function (res) {
    //             wx.showToast({
    //                 title: '复制成功',
    //             });
    //         }
    //     });
    // },
    // goDetail(e) {
    //     if (this.endTime - this.startTime < 350) {
    //         console.log("点击")
    //         let releaseId = e.currentTarget.dataset.id;
    //         let bizFileType = e.currentTarget.dataset.bizfiletype;
    //         let radio = e.currentTarget.dataset.radio;
    //         if (bizFileType == 2 && radio > 1) {
    //             wx.navigateTo({
    //                 url: '/packageB/pages/videoDetail/index?id=' + releaseId + '&type=0'
    //             })
    //         } else {
    //             wx.navigateTo({
    //                 url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=0'
    //             })
    //         }
    //     }

    // },
    // goTopDetail(e) {

    //     let releaseId = e.currentTarget.dataset.id;
    //     let bizFileType = e.currentTarget.dataset.bizfiletype;
    //     let radio = e.currentTarget.dataset.radio;
    //     if (bizFileType == 2 && radio > 1) {
    //         wx.navigateTo({
    //             url: '/packageB/pages/videoDetail/index?id=' + releaseId + '&type=0'
    //         })
    //     } else {
    //         wx.navigateTo({
    //             url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=0'
    //         })
    //     }


    // },
    // goHotDetail(e) {
    //     wx.navigateTo({
    //         url: '/packageB/pages/hotList/index'
    //     })

    // },
    // listen(e){
    //     let that = this;
    //     let obj = e.currentTarget.dataset.item;
    //     let type = 0;
    //     let typeId = obj.releaseId;
    //     let  listenFlag = obj. listenFlag;
    //     let userId = obj.userId;
    //     if (wx.getStorageSync('userInfo')) {
    //         that.setData({
    //             nickName: wx.getStorageSync('userInfo').nickName
    //         })
    //     };
    //     var params = {
    //         status:  listenFlag ? 0 : 1,
    //         userId: '',
    //         eventId: parseInt(typeId),
    //     };
    //     let url = app.apiUrl + '/message/listen';
    //     if (!wx.getStorageSync('token')) {
    //         wx.navigateTo({
    //             url: '/pages/author/index'
    //         })
    //         return
    //     } else {
    //         app.request.requestPostApi(url, params, that, that.successFunListen, that.failFun, wx.getStorageSync('token'), {
    //             type: type,
    //             typeId: typeId,
    //             listenFlag: !listenFlag,
    //             userId: userId,
    //             nickName: that.data.nickName
    //         })
    //     }
    // },
    // successFunListen: function (res, selfObj, data) {
    //     selfObj.data.homeData.list && selfObj.data.homeData.list.forEach(item => {
    //         if (item.releaseId == data.typeId) {
    //             item.listenFlag = data.listenFlag;
    //         }
    //         return selfObj.data.homeData.list
    //     });
    //     selfObj.setData({
    //         'homeData.list': selfObj.data.homeData.list,
    //     })
    // },

    // zan(e) {
    //     debugger
    //     let that = this;
    //     let obj = e.currentTarget.dataset.item;
    //     let type = 0;
    //     let typeId = obj.releaseId;
    //     let deleteFlag = obj.deleteFlag;
    //     let likeFlag = obj.likeFlag;
    //     let userId = obj.userId;
    //     let contentText = obj.contentText;

    //     if (wx.getStorageSync('userInfo')) {
    //         that.setData({
    //             nickName: wx.getStorageSync('userInfo').nickName
    //         })
    //     }
    //     ;
    //     var params = {
    //         status: likeFlag ? false : true,
    //         type: type,
    //         typeId: typeId,
    //     };
    //     let url = app.apiUrl + '/user/like';
    //     if (!wx.getStorageSync('token')) {
    //         wx.navigateTo({
    //             url: '/pages/author/index'
    //         })
    //         return
    //     } else {
    //         app.request.requestPostApi(url, params, that, that.successFun4, that.failFun, wx.getStorageSync('token'), {
    //             type: type,
    //             typeId: typeId,
    //             status: !likeFlag,
    //             userId: userId,
    //             contentText: contentText,
    //             nickName: that.data.nickName
    //         })
    //     }
    // },
    // goUserDetail(e) {
    //     let userId = e.currentTarget.dataset.userid;
    //     let deleteFlag = e.currentTarget.dataset.deleteflag;
    //     wx.navigateTo({
    //         url: '/packageB/pages/logs/logs?userId=' + userId + '&deleteFlag=' + deleteFlag
    //     })
    // },

    isAuth() {
        if (!app.globalData.token && !wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
    },
    // goVDetail() {
    //     wx.navigateTo({
    //         url: '/packageB/pages/official/index?id=160&type=0'
    //     })
    // },
    // onReachBottom: function () {
    //     var that = this;
    //     if (that.data.load&&!that.data.resh) {
    //         if (that.data.params.pageNum < that.data.homeData.pages) {
    //             that.setData({
    //                 'params.pageNum': that.data.params.pageNum * 1 + 1,
    //                 lodaing: true
    //             })
    //             that.loadMore();
    //         }
    //     }else{
    //         return
    //     }
    // },
    // onPullDownRefresh() {
    //     var that = this;
    //     that.setData({
    //         showLoading: true,
    //         resh: true,
    //         'homeData.list': [],
    //     });
    //     if (that.data.showAll || that.data.showAuth ||that.data.lodaing) {
    //         return
    //     } else{
    //         wx.showNavigationBarLoading() //在标题栏中显示加载
    //         setTimeout(function () {
    //             let url = app.apiUrl + '/release/list';
    //             var params = {
    //                 pageNum: 1,
    //                 pageSize: app.globalData.pageSize,
    //                 topicTypeId: that.data.topicTypeId,
    //                 schoolId: wx.getStorageSync('schoolId')
    //             };

    //             app.request.requestGetApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
    //             wx.hideNavigationBarLoading() //完成停止加载
    //             wx.stopPullDownRefresh() //停止下拉刷新
    //         }, 1500);
    //     }
    // },
    successFunSchool: function (res, selfObj) {
        //wx.hideLoading();
        selfObj.setData({
            school: res.data,
        })
    },
    goCheck(e) {
        let schoolId = e.currentTarget.dataset.id;
        let schoolName = e.currentTarget.dataset.schoolname;
        if (schoolId) {
            wx.setStorageSync('schoolId', schoolId);
            wx.setStorageSync('schoolName', schoolName);
            this.setData({
                showAll: false,
                showAuth: false
            })
        }
        ;
        this.getTop();
        this.getHot();
    },
  
    // quxiao() {
    //     this.setData({
    //         showAuth: false
    //     })
    //     this.getTop();
    //     this.getHot();
    // },
    // successFunEdit(res, selfObj) {
    //     selfObj.setData({
    //         showAll: false,
    //         showAuth: false
    //     });
    //     selfObj.getTop();
    //     selfObj.getHot();
    // },

    login(userInfo) {
        let that = this;
        if (userInfo.userId) {
            wx.setStorageSync('myUserId', e.detail.userInfo.userId);
        }
        wx.request({
            url: app.apiUrl + '/wx/login',
            method: 'post',
            data: {
                code: app.globalData.code,
                userInfo: userInfo
            },
            success: function (res) {
                app.globalData.token = res.data.data.token ? res.data.data.token : '';
                app.globalData.userInfo = res.data.data.userInfo ? res.data.data.userInfo : '';
                wx.setStorageSync('token', res.data.data.token);
                wx.setStorageSync('userType', res.data.data.userInfo.userType);
                wx.setStorageSync('userInfo', res.data.data.userInfo);
                wx.setStorageSync('myUserId', res.data.data.userInfo.userId);
                that.getTop();

            }
        })
    },

    getToken() {
        if (app.globalData.userInfo) {
            console.log(333)
            this.login(app.globalData.userInfo);
            return
        } else if (this.data.canIUse) {
            app.userInfoReadyCallback = res => {
                this.login(res.userInfo);
            }

            return
        } else {
            wx.getUserInfo({
                success: res => {
                    this.login(res.userInfo);
                }
            })
        }
        ;
    },
    onLoad(options) {
        let that = this;
        that.setData({
            navH: app.globalData.navHeight
          })
        if (!wx.getStorageSync('schoolId') && !wx.getStorageSync('token')) {
            that.setData({
                showAll: true,
                navbarHeight: app.globalData.navbarHeight
            });
            let url1 = app.apiUrl + '/school/list';
            let params1 = {};
            app.request.requestGetApi(url1, params1, that, that.successFunSchool, that.failFun)
            return
        } else {
            wx.getSystemInfo({
                success: function (res) {
                    let moveParams = that.data.moveParams;
                    moveParams.screenHalfWidth = res.screenWidth / 2;
                    that.setData({
                        systemInfo: res
                    })
                },
            });
            if (!wx.getStorageSync('token')) {
                that.getToken();
            }
            that.getTop();
            that.getHot();
            that.setData({
                nav: [{id: "", name: "最新"}],
                isIphoneX: app.globalData.isIphoneX,
            });
        }
    },


    // getBanner() {
    //     let that = this;
    //     let url = app.apiUrl + '/index/banner/';
    //     let params = {};
    //     app.request.requestGetApi(url, params, that, that.successFunBanner, that.failFun)
    // },
    // successFunBanner: function (res, selfObj) {
    //     selfObj.setData({
    //         banner: res.data,
    //     })
    // },
    // getHot() {
    //     let that = this;
    //     let url = app.apiUrl + '/release/hot_post_list/' + wx.getStorageSync('schoolId');
    //     let params = {};
    //     app.request.requestGetApi(url, params, that, that.successFunHot, that.failFun)
    // },
    // successFunHot: function (res, selfObj) {
    //     let hotData = res.data.filter(item => {
    //         return item.contentText.length != 0
    //     })
    //     selfObj.setData({
    //       hotList: hotData,
    //       hotList_left: hotData.slice(0,3),
    //       hotList_right: hotData.slice(3, 6),
    //         nowSchool: wx.getStorageSync('schoolName')
    //     })
    // },
    // getTop() {
    //     let that = this;
    //     let url = app.apiUrl + '/index/topic_type_list?type=0';
    //     let params = {};
    //     app.request.requestGetApi(url, params, that, that.successFun, that.failFun)
    // },

    // successFun: function (res, selfObj) {
    //     let that = this;
    //     selfObj.setData({
    //         nav: that.data.nav.concat(res.data),
    //         activeIdx: 0,
    //         schoolId: wx.getStorageSync('schoolId')
    //     })
    //     this.getList('refresh', pageStart);
    //     let url = app.apiUrl + '/release/list';
    //     var params = {
    //         topicTypeId: '',
    //         pageNum: 1,
    //         schoolId: wx.getStorageSync('schoolId')
    //     };
    //     app.request.requestGetApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
    // },
    successFunMess: function (res, selfObj) {
      app.globalData.message = res.data
      selfObj.setData({
        message: app.globalData.message
      });
    },
    onShow: function () {
      debugger
        let that = this;
        if (typeof this.getTabBar === 'function' && that.getTabBar()) {
            that.getTabBar().setData({
                selected: 0
            })
        }
        if (wx.getStorageSync('token')){
            let that = this;
            let url = app.apiUrl + '/message/get_status';
            let params = {};
          app.request.requestGetApi(url, params, that, that.successFunMess, that.failFun, wx.getStorageSync('token'))
        }
       
        //that.getBanner();轮播图
        let url = app.apiUrl + '/release/list';
      if (wx.getStorageSync('releaseId2')) {
        that.data.homeData.list && that.data.homeData.list.forEach(item => {
          if (item.releaseId == wx.getStorageSync('releaseId2')) {
            item.listenFlag = wx.getStorageSync('listenFlag');
            console.log(wx.getStorageSync('listenFlag'))
          }
          return that.data.homeData.list
        });
        that.setData({
          'homeData.list': that.data.homeData.list,
        })
      }
        if (wx.getStorageSync('releaseId')) {
            debugger
            that.data.homeData.list && that.data.homeData.list.forEach(item => {
                if (item.releaseId == wx.getStorageSync('releaseId')) {
                    if (app.globalData.commentId) {
                        let newList = item.comments.filter(function (good) {
                            return good.id != app.globalData.commentId
                        })
                        item.comments = newList
                        item.reviews = wx.getStorageSync('reviews');
                    }
                    if (wx.getStorageSync('confessContent')) {
                        item.comments.unshift(app.globalData.confessContent)
                        item.reviews = wx.getStorageSync('reviews');
                    }
                    if ((item.likeFlag && wx.getStorageSync('likeFlag')) || (!item.likeFlag && !wx.getStorageSync('likeFlag'))) {
                        return
                    }
                    item.likeFlag = wx.getStorageSync('likeFlag');
                    item.likes = wx.getStorageSync('likeFlag') ? item.likes + 1 : item.likes - 1
                }
                return that.data.homeData.list
            });
            that.setData({
                'homeData.list': that.data.homeData.list,
            })
        }
        if (app.globalData.release || wx.getStorageSync('indexChanged')) {
            debugger
            wx.pageScrollTo({
                scrollTop: 0,
            })
            that.getHot();
            that.setData({
                'params.schoolId': wx.getStorageSync('schoolId'),
                'params.pageNum': 1,
                'params.topicTypeId': '',
                activeIdx: 0,
            });
            app.request.requestGetApi(url, that.data.params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
        }
    },
    onHide() {
        app.globalData.release = false;
        wx.setStorageSync('releaseId', '');
        wx.setStorageSync('releaseId2', '');
        wx.setStorageSync('likeFlag', null);
        wx.setStorageSync('listenFlag', null);
        wx.setStorageSync('confessContent', '');
        wx.setStorageSync('reviews', '');
        app.globalData.confessContent = {},
            app.globalData.commentId = null
    },

    load(topicTypeId) {
        let that = this;
        let url = app.apiUrl + '/release/list';
        that.setData({
            homeData: {},
        })
        var params = {
            pageNum: that.data.params.pageNum,
            pageSize: app.globalData.pageSize,
            topicTypeId: topicTypeId ? topicTypeId : '',
            schoolId: wx.getStorageSync('schoolId')
        };
        app.request.requestGetApi(url, params, that, that.successFun3, that.failFun, wx.getStorageSync('token'))
    },
    loadMore() {
        let that = this;
        let url = app.apiUrl + '/release/list';
        var params = {
            pageNum: that.data.params.pageNum,
            pageSize: app.globalData.pageSize,
            topicTypeId: that.data.topicTypeId,
            schoolId: wx.getStorageSync('schoolId')
        };
        app.request.requestGetApi(url, params, that, that.successFun2, that.failFun, wx.getStorageSync('token'))
    },
    successFun4: function (res, selfObj, data) {
        selfObj.data.homeData.list && selfObj.data.homeData.list.forEach(item => {
            if (item.releaseId == data.typeId) {
                item.likeFlag = data.status;
                item.likes = data.status ? item.likes + 1 : item.likes - 1
            }
            return selfObj.data.homeData.list
        });
        selfObj.setData({
            'homeData.list': selfObj.data.homeData.list,
        })
        if (data.status) {
            selfObj.tongZhi(data)
        }
    },
    successFun10(res, selfObj, data) {
        tongZhi(res, selfObj, data)
    },
    tongZhi(data) {
        let that = this;
        let url = app.apiUrl + '/index/get_openid/' + data.userId;
        let params = {};
        app.request.requestGetApi(url, params, that, that.successFun10, that.failFun, wx.getStorageSync('token'), data)
    },
    successFun1: function (res, selfObj) {
        res.data.list && res.data.list.forEach(item => {
            let d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
            item.releaseTime = diaplayTime(d);
            item.userAvatarUrl=item.userAvatarUrl.indexOf('https')>-1?item.userAvatarUrl:app.apiUrlimg+item.userAvatarUrl
            if (item.releaseFiles.length == 1) {
                item.releaseFiles.forEach(good => {
                    good.radio = good.width / good.height;
                });
            };
            item.comments && item.comments.forEach(items => {
                items.avatarUrl= items.avatarUrl.indexOf('https')>-1? items.avatarUrl:app.apiUrlimg+ items.avatarUrl
            });
        });
        selfObj.setData({
            homeData: res.data,
            scrollLeft: 0,
            showLoading: false,
            resh:false
        })
        wx.setStorageSync('indexChanged', false);
    },
    successFun3: function (res, selfObj) {
        //wx.hideLoading();
        res.data.list.forEach(item => {
            let d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
            item.releaseTime = diaplayTime(d);
            item.userAvatarUrl=item.userAvatarUrl.indexOf('https')>-1?item.userAvatarUrl:app.apiUrlimg+item.userAvatarUrl
            item.comments && item.comments.forEach(items => {
                items.avatarUrl= items.avatarUrl.indexOf('https')>-1? items.avatarUrl:app.apiUrlimg+ items.avatarUrl
            });
            if (item.releaseFiles.length == 1) {
                item.releaseFiles.forEach(good => {
                    good.radio = good.width / good.height;
                });
            }
        });
        selfObj.setData({
            homeData: res.data,
            showLoading: false
        });
    },
    successFun2: function (res, selfObj) {
        let that = this;
        res.data.list.forEach(item => {
            let d = new Date(item.releaseTime.replace(/-/g, '/')).getTime();
            item.releaseTime = diaplayTime(d);
            item.userAvatarUrl=item.userAvatarUrl.indexOf('https')>-1?item.userAvatarUrl:app.apiUrlimg+item.userAvatarUrl;
            item.comments && item.comments.forEach(items => {
                items.avatarUrl= items.avatarUrl.indexOf('https')>-1? items.avatarUrl:app.apiUrlimg+ items.avatarUrl
            });
            if (item.releaseFiles.length == 1) {
                item.releaseFiles.forEach(good => {
                    good.radio = good.width / good.height;
                });
            }
        });
        var content = that.data.homeData.list.concat(res.data.list)
        selfObj.setData({
            'homeData.list': content,
            lodaing: false
        });
    },


    failFun: function (res, selfObj) {
      selfObj.setData({
        resh: false
      });
        console.log('failFun', res)
    },


    getUserInfo: function (e) {
        this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: true
        })
    },
   
})
