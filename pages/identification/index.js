const app = getApp();
Page({
    data: {
        school: [],
        index: null,
        authFilePath: '',
        idCard:'',
        profession:'',
        grade:'',
        studentId:''
    },
    onLoad() {
        app.httpGet({
            url: '/school/list',
            loading: true
        }).then((res) => {
            this.setData({
                school: res.data,
            })
            if (wx.getStorageSync('schoolId')) {
                res.data.findIndex((value, index, arr) => {
                    if (value.id == wx.getStorageSync('schoolId')) {
                        this.setData({
                            index: index,
                        })
                    }
                })
    
            }
        })
    },
    bindPickerChange(e) {
        this.setData({
            index: e.detail.value,
        })
    },
    bindKeyInput: function (e) {
        this.setData({
            profession: e.detail.value
        })
    },
    bindKeyInput1: function (e) {
        this.setData({
            grade: e.detail.value
        })
    },
    bindKeyInput2: function (e) {
        this.setData({
            studentId: e.detail.value
        })
    },
    goUpdate: function () {
        var _this = this;
        wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['original', 'compressed'],
            // 指定是原图还是压缩图，默认两个都有
            sourceType: ['album', 'camera'],
            // 指定来源是相册还是相机，默认两个都有
            success: function (res) {
                debugger
                // 返回选定照片的本地文件路径tempFilePath可以作为img标签的src属性显示图片
                //然后请求接口把图片传给后端存到服务器即可
                _this.setData({
                    idCard: res.tempFilePaths,
                })
            }
        })
    },
    update(){
        
        app.httpPost({
            url: '/user/auth',
            loading: true,
            data: {
                authFilePath:this.data.authFilePath,
                school:this.data.school[this.data.index].name,
                profession:this.data.profession,
                grade:this.data.grade,
                studentId:this.data.studentId
            },
            token: wx.getStorageSync('token')
        }).then((res) => {
            wx.showModal({
                title: '温馨提示',
                content: '认证已转人工审核，请耐心等待',
                showCancel: false,
                confirmText: '知道了',
                success(res) {
                    if (res.confirm) {
                        wx.switchTab({
                            url: '/pages/my/index'
                        })
                    }
                }
            })
            return
        })
    },
    save() {
        debugger
        let that=this;
        if(!that.data.idCard[0]||!that.data.school[that.data.index].name||!that.data.profession||!that.data.grade||!that.data.studentId){
            wx.showToast({
                icon:'none',
                title: '请完善信息',
            })
            return
        }
        if(that.data.idCard){
            wx.showLoading({
                title:'保存中',
                mask:true
            });
            wx.uploadFile({
                url: app.apiUrl2 + '/document/upload/1/1',
                filePath: that.data.idCard[0],
                name: 'fileData',
                header: {
                    "X-Xnxw-Token": wx.getStorageSync('token'),
                },
                success: function (res) {
                    if (JSON.parse(res.data).code==400){
                        wx.hideLoading();
                        wx.showToast({
                            title: JSON.parse(res.data).message,
                            icon: 'none'
                        })
                        return
                    }else{
                        that.setData({
                            authFilePath: (JSON.parse(res.data).data)[0].relativePath
                        });
                        that.update()
                    }
                },

                fail: function (res) {
                    wx.hideLoading();
                    wx.showModal({
                        title: '错误提示',
                        content: '上传图片失败',
                        showCancel: false,
                        success: function (res) {
                        }
                    })
                }
            });
        }

    }
})