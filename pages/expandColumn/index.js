//index.js
//获取应用实例
const app = getApp()
Page({
    data: {
        params:{

        },
    },
    onShow: function () {
        let that=this;
        if (typeof this.getTabBar === 'function' &&
            that.getTabBar()) {
            that.getTabBar().setData({
                selected: 1
            })
        }
    },

    onLoad: function () {

    },
    onHide() {
        app.globalData.release = false;
    },
    successFun: function (res, selfObj) {

    },
    onShareAppMessage: function () {
      return {
        title: '哆啦校圈-扩列',
      }
    },
    confess(){
        wx.navigateTo({
            url: '/packageB/pages/confession/index'
        })
    },
    photos(){
        wx.navigateTo({
            url: '/pages/photos/index'
        })
    },

});
