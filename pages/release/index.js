const app = getApp();
let date = new Date();
let currentHours = date.getHours();
let currentMinute = date.getMinutes();
Page({
    data: {
        images: [],
        video: [],
        array: [],
        topicindex: 30,
        content: '',
        contentCount: 0, //正文字数
        size: '视频上传中...',
        videoSize: null,
        title: '',
        fileIdOrders: [],
        contentText: '',
        topicTypeId: null,
        type: 1,
        startDay: '',
        endDay: '',
        startTime: '',
        endTime: '',
        endedTime: '2019-01-01 12:38',
        time: '00:00',
        date: '',
        person: [2, 3, 4, 5, 6, 7, 8, 9, 10],
        perIndex: 0,
        process: {
            toUserClass: '',
            toUserName: '',
            toUserProfession: '',
            toUserSchool: ''
        },
        index: null,
        school: {},
        hasAudio: false,
        fakeFlag:false,
        vestFlag:false,
        userType:0,
        groupSendType:0,
        drawStatus:0
    },
    fakeFlagChange(e){
        let fakeFlag =e.detail.value;
        this.setData({
            fakeFlag:fakeFlag
        })
    },
    vestFlagChange(e){
        let vestFlag =e.detail.value;
        this.setData({
            vestFlag:vestFlag
        })
    },
    groupSendTypeChange(e){
        let groupSendType =e.detail.value;
        this.setData({
            groupSendType:groupSendType?2:1
        })
    },
    lookRule() {
        wx.navigateTo({
            url: '/pages/outer/index?type=rule'
        })
    },
    commonNavAttr(e) {
        console.log(e);
        if (e.detail) {
            this.setData({
                commonNavAttr: e.detail
            })
        }
    },
    bindPickerChange1: function (e) {
        this.setData({
            perIndex: e.detail.value
        })
    },
    onPickerChange3: function (e) {
        console.log(e.detail);
        this.setData({
            endedTime: e.detail.dateString
        })
    },
    toDouble: function (num) {
        if (num >= 10) {//大于10
            return num;
        } else {//0-9
            return '0' + num
        }
    },

    getTimeNow() {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let day2 = new Date(date.setDate(new Date().getDate() + 5));
        let startDay = year + '-' + this.toDouble(month) + '-' + this.toDouble(day);
        let endDay = day2.getFullYear() + '-' + this.toDouble(day2.getMonth() + 1) + '-' + this.toDouble(day2.getDate());
        let startTime = date.getHours() + ':' + this.toDouble(day2.getMinutes());
        let endTime = (date.getHours() + 12) + ':' + this.toDouble(day2.getMinutes());
        this.setData({
            startDay: startDay,
            endDay: endDay,
            date: startDay,
            //startTime:startTime,
        })
    },
    onShow() {
        let that = this;
        let pages = getCurrentPages() //获取加载的页面
        let currentPage = pages[pages.length - 1] //获取当前页面的对象
        let type = currentPage.options.type //如果要获取url中所带的参数可以查看options
        that.getTimeNow()
        that.setData({
            title: type == 1 ? '新鲜事' : type == 2 ? '组队' : type == 3 ? '表白' : type == 4 ? '颜值' : '哆啦校圈',
            type: type,
            images: app.globalData.images,
            video: app.globalData.video,
        })
        if (type == 1 || type == 2) {
            that.setData({
                userType:wx.getStorageSync('userType'),
            })
            let url = app.apiUrl + '/index/topic_type_list';
            var params = {
                type: that.data.type == 2 ? 1 : 0
            };
            app.request.requestGetApi(url, params, that, that.successFun, that.failFun)
        }

    },
    bindPickerChange(e) {
        this.setData({
            index: e.detail.value,
            'process.toUserSchool': this.data.school[e.detail.value].name
        })
    },
    successFun6: function (res, selfObj) {
        selfObj.setData({
            school: res.data,
        })
    },
    successFun: function (res, selfObj) {
        wx.hideLoading();
        selfObj.setData({
            array: res.data,
        })
        app.httpGet({
            url: "/index/get_prize_draw_status"
        }).then((res) => {
            selfObj.setData({
                drawStatus: res.data,
            })
        });
        // this.common(app.globalData.images, 1)

    },
    addPic() {
        let len = app.globalData.sendImagesSize - app.globalData.images.length;
        let that = this
        let images = app.globalData.images
        wx.chooseImage({
            count: len,
            sizeType: ['original', 'compressed'],
            sourceType: ['album', 'camera'],
            success(res) {
                // tempFilePath可以作为img标签的src属性显示图片
                const images = app.globalData.images.concat(res.tempFilePaths);
                that.setData({
                    images: images,
                });
                // 限制最多只能留下9张照片
                app.globalData.images = images.length <= 9 ? images : images.slice(0, 9);
                //that.common(res.tempFilePaths,1);
                //app.globalData.images = images.length <= 9 ? images : images.slice(0, 9);
                // tempFilePath可以作为img标签的src属性显示图片
            }
        })
    },
    sendVideo() {
        debugger
        let that = this
        wx.chooseVideo({
            sourceType: ['album', 'camera'],
            maxDuration: 60,
            camera: 'back',
            success(res) {
                app.globalData.video = res.tempFilePath;
                that.setData({
                    videoSize: (res.size / (1024 * 1024)).toFixed(2)
                });
                that.setData({
                    video: res.tempFilePath,
                });
                //that.common(res.tempFilePath, 2, that.data.videoSize);

            }
        })
    },


    submitOn() {

        let that = this;
        if (that.data.type == 4 && that.data.images.length < 1) {
            wx.showToast({
                title: "请上传照片",
                icon: 'none',
                duration: 2000,
                mask: true
            });
            return
        }

        if (!that.data.contentText.replace(/\s*/g, "") && (that.data.type == 2 || that.data.type == 3)) {
            wx.showToast({
                title: "内容不能为空",
                icon: 'none',
                duration: 2000,
                mask: true
            });
            return
        }
        if (!that.data.contentText.replace(/\s*/g, "")
            && that.data.images.length < 1 && that.data.video.length < 1) {
            wx.showToast({
                title: "上传内容不能为空",
                icon: 'none',
                duration: 2000,
                mask: true
            });
            return
        }

        if (!that.data.topicTypeId && that.data.type != 3 && that.data.type != 4) {
            wx.showToast({
                title: "请选择话题",
                icon: 'none',
                duration: 2000,
                mask: true
            });
            return
        }
        if (that.data.type == 2) {
            let aa = that.data.date + ' ' + that.data.time + ':00';
            let dd = new Date(aa.replace(/-/g, '/')).getTime();
            if (date.getTime() > dd) {
                wx.showToast({
                    title: "时间不能小于当前时间",
                    icon: 'none',
                    duration: 2000,
                    mask: true
                });
                return
            }
        }
        if (!that.data.process.toUserName && that.data.type == 3) {
            wx.showToast({
                title: "请填写表白对象",
                icon: 'none',
                duration: 2000,
                mask: true
            });
            return
        }
        wx.requestSubscribeMessage({
            tmplIds: app.globalData.template_id,
            success(res) {

            }
        });

        wx.showLoading({
            title: '正在发布...',
            icon: 'loading',
            mask: true
        })
        if (that.data.images.length > 0) {
            for (var i = 0, h = that.data.images.length; i < h; i++) {
                (function (i) {
                    //上传文件
                    wx.uploadFile({
                        url: app.apiUrl2 + '/document/upload/1/1',
                        filePath: that.data.images[i],
                        name: 'fileData',
                        header: {
                            "X-Xnxw-Token": wx.getStorageSync('token'),
                        },
                        success: function (res) {
                            if (JSON.parse(res.data).code == 400) {
                                wx.hideLoading();
                                wx.showToast({
                                    title: JSON.parse(res.data).message,
                                    icon: 'none'
                                })
                                return
                            } else {
                                that.setData({
                                    fileIdOrders: that.data.fileIdOrders.concat({
                                        fileId: (JSON.parse(res.data).data)[0].id,
                                        order: i,
                                        src: (JSON.parse(res.data).data)[0].relativePath
                                    })
                                })
                                if (that.data.fileIdOrders.length == that.data.images.length) {
                                    that.subCommon();
                                }
                            }
                        },

                        fail: function (res) {
                            return
                            wx.hideLoading();
                            wx.showModal({
                                title: '错误提示',
                                content: '上传图片失败',
                                showCancel: false,
                                success: function (res) {
                                }
                            })
                        }
                    });
                })(i)
            }
        }
        if (that.data.video.length > 0) {
            var src = that.data.video;
            wx.uploadFile({
                url: app.apiUrl2 + '/document/upload/2/2',
                method: 'POST',//这句话好像可以不用
                filePath: src,
                header: {
                    "Content-Type": "multipart/form-data",
                    "X-Xnxw-Token": wx.getStorageSync('token'),
                },
                name: 'fileData',//服务器定义的Key值
                success: function (res) {
                    if (JSON.parse(res.data).code == 400) {
                        wx.hideLoading();
                        wx.showToast({
                            title: JSON.parse(res.data).message,
                            icon: 'none'
                        })
                        return
                    } else {
                        that.data.fileIdOrders.push({fileId: (JSON.parse(res.data).data)[0].id, order: 1})
                        if (that.data.videoSize > 20) {
                            console.log('视频222')
                            wx.showToast({
                                title: '视频不能超过20M',
                                icon: '',
                                image: '',
                                duration: 1500,
                                mask: true
                            })
                            return
                        }
                        if (that.data.fileIdOrders.length == 1 && that.data.video) {
                            that.setData({
                                hasAudio: true
                            })
                            that.subCommon();
                        }
                        console.log('视频上传成功')
                    }
                },
                fail: function () {
                    wx.showLoading({
                        title: '接口调用失败',
                        mask: true
                    })
                    return
                    console.log('接口调用失败')
                }
            })
            return
        }
        if (that.data.video.length == 0 && that.data.images.length == 0) {
            that.subCommon();
        }
    },
    subMit() {
        let that = this;
        let url, params;
        if (that.data.type == 3) {
            url = app.apiUrl + '/confess/release';
            params = {
                fileIdOrders: JSON.stringify(that.data.fileIdOrders),
                contentText: that.data.contentText,
                schoolId: wx.getStorageSync('schoolId'),
                toUserClass: that.data.process.toUserClass,
                toUserName: that.data.process.toUserName,
                toUserProfession: that.data.process.toUserProfession,
                toUserSchool: that.data.process.toUserSchool,
            };
        } else if (that.data.type == 2) {
            debugger
            url = app.apiUrl + '/team/create';

            params = {
                fileIdOrders: JSON.stringify(that.data.fileIdOrders),
                contentText: that.data.contentText,
                topicTypeId: that.data.topicTypeId,
                schoolId: wx.getStorageSync('schoolId'),
                endTime: that.data.date + ' ' + that.data.time + ':00',
                peopleNum: that.data.person[that.data.perIndex],
                startTime: that.data.date + ' ' + that.data.time + ':00',
            };
        } else if (that.data.type == 4) {
            url = app.apiUrl + '/face/release';
            params = {
                fileIdOrders: JSON.stringify(that.data.fileIdOrders),
                contentText: that.data.contentText,
                faculty: '',
                schoolId: wx.getStorageSync('schoolId'),
                grade: '',
            };
        } else {
            url = app.apiUrl + '/release/release_topic';
            let newArr = [];
            that.data.fileIdOrders.forEach((item) => {
                newArr.push(
                    {
                        fileId: item.fileId,
                        order: item.order,
                    }
                )
            })
            params = {
                fileIdOrders: newArr,
                contentText: that.data.contentText,
                fakeFlag:that.data.fakeFlag,
                vestFlag:that.data.vestFlag,
                groupSendType:that.data.groupSendType,
                topicTypeId: that.data.topicTypeId,
                schoolId: wx.getStorageSync('schoolId'),
                hasAudio: that.data.hasAudio
            };
        }

        app.request.requestPostApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
    },

    subCommon() {
        let that = this;
        that.subMit()
    },
    successFun1: function (res, selfObj) {
        wx.hideLoading()
        if (selfObj.data.hasAudio) {
            wx.showModal({
                title: '温馨提示',
                content: '帖子已转人工审核，请耐心等待',
                showCancel: false,
                confirmText: '知道了',
                success(res) {
                    if (res.confirm) {
                        wx.switchTab({
                            url: '/pages/index/index'
                        })
                    }
                }
            })
            return
        } else {
            debugger
            wx.showToast({
                title: "发布成功",
                icon: 'success',
                duration: 2000
            });
            setTimeout(function () {
                //wx.hideToast();
                app.globalData.release = true;
                selfObj.setData({
                    videoSize: null
                })
                if (selfObj.data.type == 3 || selfObj.data.type == 4) {
                    wx.switchTab({
                        url: '/pages/photos/index'
                    })
                    return
                }

                if (selfObj.data.type == 2) {
                    wx.switchTab({
                        url: '/pages/team/index'
                    })
                    return
                }
                wx.switchTab({
                    url: '/pages/index/index'
                })
            }, 2500);
        }
    },
    failFun: function (res, selfObj) {
        console.log('failFun', res)
    },
    onUnload() {
        app.globalData.images = [];
        app.globalData.video = [];
    },
    bindTimeChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            time: e.detail.value
        })
    },
    bindDateChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            date: e.detail.value
        })
    },
    bindKeyInput(e) {
        let type = e.currentTarget.dataset.type;
        if (type == 1)
            this.setData({
                'process.toUserName': e.detail.value
            })
        if (type == 2)
            this.setData({
                'process.toUserSchool': e.detail.value
            })
        if (type == 3)
            this.setData({
                'process.toUserProfession': e.detail.value
            })
        if (type == 4)
            this.setData({
                'process.toUserClass': e.detail.value
            })
    },

    change(e) {
        this.setData({
            topicTypeId: e.currentTarget.dataset.id,
            topic: parseInt(e.currentTarget.dataset.index),
        })
    },
    bindinput: function (e) {
        this.setData({
            contentText: e.detail.value,
            contentCount: e.detail.value.length
        });
    },
    removeImage(e) {
        debugger
        let idx = e.target.dataset.idx;
        let fileId = e.target.dataset.fileid;
        app.globalData.images.splice(idx, 1);
        let newOrders = this.data.fileIdOrders.filter(item => {
            return item.fileId != fileId
        })
        this.setData({
            images: app.globalData.images,
            fileIdOrders: newOrders
        });
    },
    removeVideo(e) {
        debugger
        app.globalData.video = [];
        this.setData({
            video: app.globalData.video,
            fileIdOrders: [],
            videoSize: null
        });
    },


    handleImagePreview(e) {
        debugger
        let idx = e.target.dataset.idx
        let images = this.data.images
        let imgsList = [];
        images.forEach(item => {
            imgsList.push(item)
        })
        wx.previewImage({
            current: images[idx],  //当前预览的图片
            urls: images,  //所有要预览的图片
        })
    },
});