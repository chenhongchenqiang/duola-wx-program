const app=getApp();
Page({
    data:{
        showSingUp:false,
        signNum: 0,  //签到数
        signState: false, //签到状态
        min: 1,  //默认值日期第一天1
        max: 7,  //默认值日期最后一天7
        userInfo:{},
        navbarHeight:0,
        head:'../../images/home/logo.png',
        message:null,
        loadDone:false
    },
    onLoad() {
        
        let that=this;
        that.setData({
            navbarHeight:app.globalData.navbarHeight
        });

    },
    
    common(){
        let that=this;
        let url = app.apiUrl + '/user/base_info';
        let params  = {}
        if(!wx.getStorageSync('token')){
            that.setData({
              'userInfo.avatarUrl':'',
              'userInfo.follows':0,
              'userInfo.fans': 0,
              'userInfo.likes': 0,
              loadDone:true
            })
            return
        }else{
          //wx.showLoading();
          app.request.requestGetApi(url, params, that, that.successFun, that.failFun, wx.getStorageSync('token'))
        }
    },
    onShow: function () {
        let that=this;
        if (typeof this.getTabBar === 'function' &&
            that.getTabBar()) {
            that.getTabBar().setData({
                selected: 4
            })
        }
        that.common();
    },
    successFun: function(res, selfObj) {
        //wx.hideLoading()
        selfObj.setData({
            userInfo : res.data,
            'userInfo.avatarUrl': res.data.avatarUrl.indexOf('https')>-1? res.data.avatarUrl:app.apiUrlimg+ res.data.avatarUrl,
            loadDone:true
        })
        wx.setStorageSync('userInfo', res.data);
        let url = app.apiUrl + '/message/get_status';
        app.request.requestGetApi(url, {}, selfObj, selfObj.successFunMess, selfObj.failFun, wx.getStorageSync('token'))
    },
    successFunMess: function (res, selfObj) {
      app.globalData.message = res.data
      selfObj.setData({
        message: app.globalData.message
      });
    },
    goUserDetail(e){
      if (!wx.getStorageSync('token')) {
        wx.navigateTo({
            url:'/pages/author/index'
        })
        return
      }else{
        wx.navigateTo({
          url: '/packageB/pages/logs/logs?deleteFlag=true'
        })
      }
       
    },
    showSingUp(){
      this.setData({
          showSingUp:true
      })
    },
    goIdent(){
      wx.navigateTo({
        url: '/pages/identification/index'
      })
    },
    lookCar(e){
      if (!wx.getStorageSync('token')) {
        wx.navigateTo({
          url: '/pages/author/index'
        })
        return
      }else{
        let type = e.currentTarget.dataset.type;
        wx.navigateTo({
          url: '/pages/chatList1/index?type=' + type + '&deleteFlag=true'
        })
      }
       
    },
    hide(){
        this.setData({
            showSingUp:false
        })
    },
    gotoType(e){
        let type=e.currentTarget.dataset.type;
        if (type === '1') {
          if (!wx.getStorageSync('token')) {
            wx.navigateTo({
              url: '/pages/author/index'
            })
            return
          }else{
            wx.navigateTo({
              url: '/packageB/pages/logs/logs?deleteFlag=true'
            })
          }
          
        }
        if (type === '2') {
            wx.navigateTo({
                url: '/pages/outer/index?type=learning'
            })
        }
        if (type === '6') {
            wx.navigateTo({
                url: '/pages/outer/index?type=aboutUs'
            })
        }
        if (type === '3') {
          if (!wx.getStorageSync('token')) {
            wx.navigateTo({
              url: '/pages/author/index'
            })
            return
          } else {
            wx.navigateTo({
              url: '/packageB/pages/message/index'
            })
          }
        }
        if (type === '8') {
          if (!wx.getStorageSync('token')) {
            wx.navigateTo({
              url: '/pages/author/index'
            })
            return
          } else {
            wx.navigateTo({
              url: '/pages/proInfor/index'
            })
          }
        }
        if (type === '9') {
          if (!wx.getStorageSync('token')) {
            wx.navigateTo({
              url: '/pages/author/index'
            })
            return
          } else {
            wx.navigateTo({
              url: '/pages/releaseStreet/index'
            })
          }
        }
        if (type === '7') {
          if (!wx.getStorageSync('token')) {
            wx.navigateTo({
              url: '/pages/author/index'
            })
            return
          } else {
            wx.navigateTo({
              url: '/packageB/pages/collect/index'
            })
          }
        }
        if(type==='4'){
          if (!wx.getStorageSync('token')) {
            wx.navigateTo({
              url: '/pages/author/index'
            })
            return
          }else{
            wx.navigateTo({
              url: '/pages/opition/index'
            })
          }
         
        }
    },
    bindSignIn(e) {
        var that = this,
            num = e.currentTarget.dataset.num;
        num++
        wx.showToast({
            icon: 'success',
            title: '签到成功',
        })
        that.setData({
            signNum: num,
        })

        var min = e.currentTarget.dataset.min,
            max = e.currentTarget.dataset.max,
            be = e.currentTarget.dataset.be;

        if (num % 7 == 0) {
            be += 1;
            that.setData({
                be: be
            })
        }

        if (num == 7 * be + 1) {
            that.setData({
                min: 7 * be + 1,
                max: 7 * be + 7
            })
        }
    }

})