//index.js
//获取应用实例
const app = getApp()
Page({
    data: {
        params:{
            pageNum: 1,
            userId:'',
        },
        userId2:'',
        list:[],
        totalData:{},
      load: true,
        showActionsheet:false,
    },
    onLoad(){
        this.setData({
            slideButtons: [{
                type: 'warn',
                text: '警示',
                extClass: 'test',
                src: '/page/weui/cell/icon_del.svg',
            }],
        });
    },
    successFun:function (res, selfObj) {
        if (res.code === 200) {
            res.data.list && res.data.list.forEach(item => {
              item.avatarUrl=item.avatarUrl.indexOf('https')>-1?item.avatarUrl:app.apiUrlimg+item.avatarUrl
            });
            selfObj.setData({
                totalData:res.data,
                list:res.data.list
            })
        }
    },
    onReachBottom: function () {
      var that = this;
      if (that.data.load) {
        if (that.data.params.pageNum < that.data.totalData.pages) {
          that.setData({
            'params.pageNum': that.data.params.pageNum * 1 + 1,
          })
          that.loadMore();
        }
      }
    },
    loadMore(){
      let that = this;
      let url = app.apiUrl + '/user/follow_list';
      app.request.requestGetApi(url, that.data.params, that, that.successFun2, that.failFun, wx.getStorageSync('token'))
    },
    successFun2: function (res, selfObj) {
      let that = this;
      res.data.list && res.data.list.forEach(item => {
        item.avatarUrl=item.avatarUrl.indexOf('https')>-1?item.avatarUrl:app.apiUrlimg+item.avatarUrl
      });
      var content = that.data.list.concat(res.data.list)
      selfObj.setData({
        totalData: res.data,
        list: content
      });
    },
    onShow: function () {
        debugger
        let that = this;
        let url = app.apiUrl + '/user/follow_list';
        let pages = getCurrentPages() //获取加载的页面
        let currentPage = pages[pages.length - 1] //获取当前页面的对象
        let type = currentPage.options.type //如果要获取url中所带的参数可以查看options
        let deleteFlag = JSON.parse(currentPage.options.deleteFlag) 
        let userId = currentPage.options.userId 
        if(type){
            that.setData({
                'params.type':type,
                'params.pageSize':20
            })
          if (deleteFlag) { 
            that.setData({
              'params.queryMe': true,
            })
          }else{
            that.setData({
              'params.queryMe': false,
            })
          }
          if (userId) {
            that.setData({
              'params.otherUserId': userId,
            })
          }
            wx.setNavigationBarTitle({
                title:type==1?'粉丝':'关注'
            });
        }

        app.request.requestGetApi(url, that.data.params, that, that.successFun, that.failFun,wx.getStorageSync('token'))
    },
    slideButtonTap(e) {
        console.log('slide button tap', e.detail)
    },
    takeCare(e){
        let url = app.apiUrl + '/user/follow';
        let that=this;
        let type=e.currentTarget.dataset.type;
        if(type==0){
            this.setData({
                showActionsheet:true,
                userId2:e.currentTarget.dataset.userid,
            })
            return
        }
        var params = {
            status: type,
            userId2:e.currentTarget.dataset.userid,
        };
        app.request.requestPostApi(url, params, that, that.successFun6, that.failFun,wx.getStorageSync('token'))
    },
    sure(){
        let url = app.apiUrl + '/user/follow';
        let that=this;
        var params = {
            status: 0,
            userId2:this.data.userId2
        };
        app.request.requestPostApi(url, params, that, that.successFun3, that.failFun,wx.getStorageSync('token'))
    },
    _closeActionSheet(e) {
        this.setData({
            showActionsheet: false,
        });
    },
    successFun3(){
        debugger
        wx.showToast({
            icon: 'success',
            title: '取消关注成功',
        });
        let newList=this.data.list.filter(item=>item.userId!=this.data.userId2);
        this.setData({
            list:newList
        })
        this._closeActionSheet()
        console.log(111)
    },
    // goUserDetail(e) {
    //     let userId = e.currentTarget.dataset.userid;
    //     let deleteFlag = e.currentTarget.dataset.deleteflag;
    //     wx.navigateTo({
    //         url: '/packageB/pages/logs/logs?userId=' + userId + '&deleteFlag=' + deleteFlag
    //     })
    // },
  goUserDetail(e) {
    debugger
    let userId = e.currentTarget.dataset.userid;
    let deleteFlag;
    let type = e.currentTarget.dataset.type;
    if (type == 1) {
      deleteFlag = wx.getStorageSync('myUserId') == userId ? true : false
    } else {
      deleteFlag = e.currentTarget.dataset.deleteflag;
    }
    wx.navigateTo({
      url: '/packageB/pages/logs/logs?userId=' + userId + '&deleteFlag=' + deleteFlag
    })
  },
    successFun6(){
        wx.showToast({
            icon: 'success',
            title: '关注成功',
        });
        console.log(111)
    },
});
