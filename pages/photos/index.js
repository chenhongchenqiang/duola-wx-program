const marginHori = 30
const marginVerti = 220

const app = getApp()
import {diaplayTime, lookImgs} from '../../utils/util';

var startPoint;
Page({

    data: {

        animation1: {},
        animation2: {},

        top1: marginVerti,
        left1: marginHori,
        top2: marginVerti,
        left2: marginHori,
        cardHeight: 1600,
        startX: 0,
        startY: 0,

        count1: 0,
        count2: 1,

        name1: "",
        location1: "",
        name2: "",
        location2: "",
        isLike1: null,
        isLike2: null,

        likeImgURL1: "",
        likeImgURL2: "",

        windowWidth: 0,
        windowHeight: 0,
        buttonTop: 0,
        buttonLeft: 0,


        isFirstCard: true,
        isFirstInit: true,

        data: [],
        num1: [1, 2, 3, 4, 5],
        num2: [6, 7, 8, 9, 10],
        count: 0,
        params: {
            schoolId: '',
        },
        faceId: null,
    },
    lookImg(e) {
        lookImgs(e)
    },
    onLoad: function (options) {
        var that = this

        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    windowWidth: res.windowWidth,
                    windowHeight: res.windowHeight,
                    navbarHeight: app.globalData.navbarHeight,
                    cardHeight: (res.windowHeight) * 2 - 420
                })
            }
        })
        that.requestMenu()

    },
    onShareAppMessage: function () {
        return {
            title: '哆啦校圈-颜值',
        }
    },
    onShow: function () {
        let that = this;
        if (typeof this.getTabBar === 'function' &&
            that.getTabBar()) {
            that.getTabBar().setData({
                selected: 3
            })
        }
    },
    clicks: function () {
        wx.navigateTo({
            url: '/packageB/pages/rank/index'
        })
    },
    clicks2: function () {
        this.isAuth();
        wx.navigateTo({
            url: '/pages/release/index?type=4'
        })
    },
    isAuth() {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        }
        ;
    },
    buttonEnd: function (e) {
    },
    buttonStart: function (e) {
        startPoint = e.touches[0]
    },
    buttonMove: function (e) {
        var endPoint = e.touches[e.touches.length - 1]
        var translateX = endPoint.clientX - startPoint.clientX
        var translateY = endPoint.clientY - startPoint.clientY
        startPoint = endPoint
        var buttonTop = this.data.buttonTop + translateY
        var buttonLeft = this.data.buttonLeft + translateX
        //判断是移动否超出屏幕
        if (buttonLeft + 50 >= this.data.windowWidth) {
            buttonLeft = this.data.windowWidth - 50;
        }
        if (buttonLeft <= 0) {
            buttonLeft = 0;
        }
        if (buttonTop <= 0) {
            buttonTop = 0
        }
        if (buttonTop + 50 >= this.data.windowHeight) {
            buttonTop = this.data.windowHeight - 50;
        }
        this.setData({
            buttonTop: buttonTop,
            buttonLeft: buttonLeft
        })
    },
    viewTouchInside: function (event) {

        var that = this

        var pointX = event.touches[0].clientX
        var pointY = event.touches[0].clientY

        that.setData({
            startX: pointX,
            startY: pointY
        })

        var animation = wx.createAnimation({
            duration: 100,
            timingFunction: 'ease-out',
        })
        animation.scale(0.9).step()

        if (that.data.isFirstCard == true) {
            that.setData({
                animation1: animation.export()
            })
        } else {
            that.setData({
                animation2: animation.export()
            })
        }

    },


    viewDidMove: function (event) {
        var that = this

        var pointX = event.touches[0].clientX
        var pointY = event.touches[0].clientY

        var widthCenter = that.data.windowWidth / 2
        var heightCenter = that.data.windowHeight / 2

        var perX = (pointX - that.data.startX) / widthCenter
        var perY = (pointY - that.data.startY) / heightCenter
        var maxPer = (Math.abs(perX) > Math.abs(perY)) ? Math.abs(perX) : Math.abs(perY)


        var animationOpacity = wx.createAnimation({
            duration: 100,
            timingFunction: 'ease-out',
        })
        animationOpacity.opacity(1).step()
        var animationRotate = wx.createAnimation({
            duration: 100,
            timingFunction: 'ease-out',
        })
        animationRotate.scale(0.9).rotate(perX * 20).step()

        var x = marginHori + pointX - that.data.startX
        var y = marginVerti + pointY - that.data.startY

        if (that.data.isFirstCard == true) {
            that.setData({
                left1: x,
                top1: y
            })
            that.setData({
                animation1: animationRotate.export(),
                animation2: animationOpacity.export()
            })
        } else {
            that.setData({
                left2: x,
                top2: y
            })
            that.setData({
                animation1: animationOpacity.export(),
                animation2: animationRotate.export(),
            })
        }
    },


    viewTouchUpDownInside: function (event) {

        var that = this

        var endX = event.changedTouches[0].clientX
        var endY = event.changedTouches[0].clientY

        var distanceX = endX - that.data.startX
        var distanceY = endY - that.data.startY

        if (distanceX > 93.75) {
            that.removeCard('right')
        } else if (distanceX < -93.75) {
            that.removeCard('left')
        } else if (distanceY < -100) {
            that.removeCard('up')
        } else if (distanceY > 100) {
            that.removeCard('down')
        }

        if (distanceX < 93.75 && distanceX > -93.75 && distanceY > -150 && distanceY < 150) {
            if (that.data.isFirstCard == true) {
                that.setData({
                    top1: marginVerti,
                    left1: marginHori
                })
            } else {
                that.setData({
                    top2: marginVerti,
                    left2: marginHori
                })
            }
        }

        var animation = wx.createAnimation({
            duration: 100,
            timingFunction: 'ease-out',
        })
        animation.scale(1).step()

        if (that.data.isFirstCard == true) {
            that.setData({
                animation1: animation.export()
            })
        } else {
            that.setData({
                animation2: animation.export()
            })
        }

        if (that.data.data.length - that.data.count == 2) {
            that.requestMenu()
        }
    },

    removeCard: function (direction) {

        var that = this

        var animation = wx.createAnimation({
            duration: 250,
            timingFunction: 'linear',
        })

        if (direction == 'right') {
            animation.translateX(400).rotate(45).opacity(0).step()
            animation.translateX(0).rotate(0).step()
        } else if (direction == 'left') {
            animation.translateX(-400).rotate(-45).opacity(0).step()
            animation.translateX(0).rotate(0).step()
        } else if (direction == 'up') {
            animation.translateY(-400).opacity(0).step()
            animation.translateY(0).step()
        } else if (direction == 'down') {
            animation.translateY(400).opacity(0).step()
            animation.translateY(0).step()
        }
        this.setData({})
        if (that.data.isFirstCard == true) {
            that.setData({
                animation1: animation.export(),
                count1: that.data.count1 + 2,
                count: that.data.count + 1,
                faceId: that.data.data[that.data.count + 1] ? that.data.data[that.data.count + 1]["faceId"] : '',
                userId: that.data.data[that.data.count + 1] ? that.data.data[that.data.count + 1]["userId"] : '',
                info1: that.data.data[that.data.count1 + 2],
                userAvatarUrl1: that.data.data[that.data.count1 + 2].userAvatarUrl.indexOf('https')>-1?that.data.data[that.data.count1 + 2].userAvatarUrl:app.apiUrlimg+that.data.data[that.data.count1 + 2].userAvatarUrl,
                isFirstCard: false
            })

            setTimeout(function () {
                that.setData({
                    top1: marginVerti,
                    left1: marginHori
                })
            }.bind(that), 250)
        } else {
            that.setData({
                animation2: animation.export(),
                count2: that.data.count2 + 2,
                count: that.data.count + 1,
                faceId: that.data.data[that.data.count + 1] ? that.data.data[that.data.count + 1]["faceId"] : '',
                userId: that.data.data[that.data.count + 1] ? that.data.data[that.data.count + 1]["userId"] : '',
                info2: that.data.data[that.data.count2 + 2],
                userAvatarUrl2:  that.data.data[that.data.count2 + 2]&&that.data.data[that.data.count2 + 2].userAvatarUrl.indexOf('https')>-1? that.data.data[that.data.count2 + 2].userAvatarUrl:app.apiUrlimg+ that.data.data[that.data.count2 + 2].userAvatarUrl,
                isFirstCard: true
            })

            setTimeout(function () {
                that.setData({
                    top2: marginVerti,
                    left2: marginHori
                })
            }.bind(that), 100)

        }
    },


    requestMenu: function () {
        var that = this
        that.pushData()
    },

    pushData: function () {
        var that = this
        wx.showLoading();
        let url = app.apiUrl + '/face/list';
        that.setData({
            'params.schoolId': wx.getStorageSync('schoolId')
        })
        app.request.requestGetApi(url, that.data.params, that, that.successFun, that.failFun);
    },
    successFun: function (res, selfObj) {
        wx.hideLoading();
        //let list=[];
        var newData = selfObj.data.data
        res.data && res.data.forEach(item => {
            // item.releaseFiles.forEach(eItem=>{
            //   newData.push({
            //     faceId: item.faceId,
            //     faculty: item.faculty,
            //     fileIdOrders: item.fileIdOrders,
            //     gender: item.gender,
            //     genderStr: item.genderStr,
            //     grade: item.grade,
            //     bgImg: app.apiUrlimg+eItem.fileRelativePath,
            //     releaseTime: item.releaseTime,
            //     userAvatarUrl: item.userAvatarUrl,
            //     userId: item.userId,
            //     userNickName: item.userNickName,
            //     userSchool: item.userSchool,
            //   })
            // })
            if (item.releaseFiles.length > 0) {
                item.bgImg = app.apiUrlimg + item.releaseFiles[0].fileRelativePath
            }
        });
        for (var i = 0; i < res.data.length; i++) {
            newData.push(res.data[i])
        }
        if (selfObj.data.isFirstInit == true) {
            debugger
            selfObj.setData({
                data: newData,
                info1: newData[selfObj.data.count],
                userAvatarUrl1: newData[selfObj.data.count]?newData[selfObj.data.count].userAvatarUrl.indexOf('https')>-1?newData[selfObj.data.count].userAvatarUrl:app.apiUrlimg+newData[selfObj.data.count].userAvatarUrl:null,
                info2: newData[selfObj.data.count + 1],
                userAvatarUrl2:newData[selfObj.data.count+1]?newData[selfObj.data.count+1].userAvatarUrl.indexOf('https')>-1?newData[selfObj.data.count+1].userAvatarUrl:app.apiUrlimg+newData[selfObj.data.count+1].userAvatarUrl:null,
                faceId: newData[selfObj.data.count] ? newData[selfObj.data.count]["faceId"] : '',
                userId: newData[selfObj.data.count] ? newData[selfObj.data.count]["userId"] : '',
                isFirstInit: false
            })

            /*that.setImgURL()*/
        }
    },

    score(e) {
        if (!wx.getStorageSync('token')) {
            wx.navigateTo({
                url: '/pages/author/index'
            })
            return
        } else {
            let that = this;
            let score = e.currentTarget.dataset.score;
            let url = app.apiUrl + '/face/score';
            let params = {
                score: score,
                faceId: that.data.faceId
            }
            app.request.requestPostApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'));
        }
    },
    successFun1: function (res, selfObj) {
        wx.showToast({
            icon: 'success',
            title: '评分成功',
        })
    },
    failFun: function (res, selfObj) {
        wx.showModal({
            title: '提示',
            content: res.data.message,
            showCancel: false
        })
    },
})