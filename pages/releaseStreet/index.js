const app = getApp();
let date = new Date();
let currentHours = date.getHours();
let currentMinute = date.getMinutes();
Page({
    data: {
        images: [],
        video: [],
        array: [],
        topicindex: 30,
        content: '',
        contentCount: 0, //正文字数
        size: '视频上传中...',
        videoSize: null,
        title: '',
        fileIdOrders: [],
        contentText: '',
        topicTypeId: null,
        type: 1,
        startDay: '',
        endDay: '',
        startTime: '',
        endTime: '',
        endedTime: '2019-01-01 12:38',
        time: '00:00',
        date: '',
        person: [2, 3, 4, 5, 6, 7, 8, 9, 10],
        perIndex: 0,
        process: {
            toUserClass: '',
            toUserName: '',
            toUserProfession: '',
            toUserSchool: ''
        },
        index: null,
        school: {},
        hasAudio: false
    },

    lookRule() {
        wx.navigateTo({
            url: '/pages/outer/index?type=rule'
        })
    },
    commonNavAttr(e) {
        console.log(e);
        if (e.detail) {
            this.setData({
                commonNavAttr: e.detail
            })
        }
    },
    bindPickerChange1: function (e) {
        this.setData({
            perIndex: e.detail.value
        })
    },
    onPickerChange3: function (e) {
        console.log(e.detail);
        this.setData({
            endedTime: e.detail.dateString
        })
    },
    toDouble: function (num) {
        if (num >= 10) {//大于10
            return num;
        } else {//0-9
            return '0' + num
        }
    },

    onShow() {
        let that = this;

        that.setData({
            type: 2,
            images: app.globalData.images,
            video: app.globalData.video,
        })
        let url = app.apiUrl + '/index/topic_type_list';
        var params = {
            type: 2
        };
        app.request.requestGetApi(url, params, that, that.successFun, that.failFun)

    },
    bindPickerChange(e) {
        this.setData({
            index: e.detail.value,
            'process.toUserSchool': this.data.school[e.detail.value].name
        })
    },
    successFun6: function (res, selfObj) {
        selfObj.setData({
            school: res.data,
        })
    },
    successFun: function (res, selfObj) {
        wx.hideLoading();
        selfObj.setData({
            array: res.data,
        })
        // this.common(app.globalData.images, 1)

    },
    addPic() {
        let len = app.globalData.sendImagesSize - app.globalData.images.length;
        let that = this
        let images = app.globalData.images
        wx.chooseImage({
            count: len,
            sizeType: ['original', 'compressed'],
            sourceType: ['album', 'camera'],
            success(res) {
                // tempFilePath可以作为img标签的src属性显示图片
                const images = app.globalData.images.concat(res.tempFilePaths);
                that.setData({
                    images: images,
                });
                // 限制最多只能留下9张照片
                app.globalData.images = images.length <= 9 ? images : images.slice(0, 9);
                //that.common(res.tempFilePaths,1);
                //app.globalData.images = images.length <= 9 ? images : images.slice(0, 9);
                // tempFilePath可以作为img标签的src属性显示图片
            }
        })
    },
    sendVideo() {
        debugger
        let that = this
        wx.chooseVideo({
            sourceType: ['album', 'camera'],
            maxDuration: 60,
            camera: 'back',
            success(res) {
                app.globalData.video = res.tempFilePath;
                that.setData({
                    videoSize: (res.size / (1024 * 1024)).toFixed(2)
                });
                that.setData({
                    video: res.tempFilePath,
                });
                //that.common(res.tempFilePath, 2, that.data.videoSize);

            }
        })
    },


    submitOn() {

        let that = this;

        if (!that.data.contentText.replace(/\s*/g, "") && (that.data.type == 2 || that.data.type == 3)) {
            wx.showToast({
                title: "内容不能为空",
                icon: 'none',
                duration: 2000
            });
            return
        }
        if (!that.data.contentText.replace(/\s*/g, "") && that.data.images.length < 1 && that.data.video.length < 1) {
            wx.showToast({
                title: "上传内容不能为空",
                icon: 'none',
                duration: 2000
            });
            return
        }

        if (!that.data.topicTypeId) {
            wx.showToast({
                title: "请选择话题",
                icon: 'none',
                duration: 2000
            });
            return
        }


        wx.showLoading({
            title: '正在发布...',
            icon: 'loading',
            mask: true
        })
        if (that.data.images.length > 0) {
            for (var i = 0, h = that.data.images.length; i < h; i++) {
                (function (i) {
                    //上传文件
                    wx.uploadFile({
                        url: app.apiUrl2 + '/document/upload/1/1',
                        filePath: that.data.images[i],
                        name: 'fileData',
                        header: {
                            "X-Xnxw-Token": wx.getStorageSync('token'),
                        },
                        success: function (res) {
                            if (JSON.parse(res.data).code == 400) {
                                wx.hideLoading();
                                wx.showToast({
                                    title: JSON.parse(res.data).message,
                                    icon: 'none'
                                })
                                return
                            } else {
                                that.setData({
                                    fileIdOrders: that.data.fileIdOrders.concat({
                                        fileId: (JSON.parse(res.data).data)[0].id,
                                        order: i,
                                        src: (JSON.parse(res.data).data)[0].relativePath
                                    })
                                })
                                if (that.data.fileIdOrders.length == that.data.images.length) {
                                    that.subCommon();
                                }
                            }
                        },

                        fail: function (res) {
                            return
                            wx.hideLoading();
                            wx.showModal({
                                title: '错误提示',
                                content: '上传图片失败',
                                showCancel: false,
                                success: function (res) {
                                }
                            })
                        }
                    });
                })(i)
            }
        }
        if (that.data.video.length > 0) {
            var src = that.data.video;
            wx.uploadFile({
                url: app.apiUrl2 + '/document/upload/2/2',
                method: 'POST',//这句话好像可以不用
                filePath: src,
                header: {
                    "Content-Type": "multipart/form-data",
                    "X-Xnxw-Token": wx.getStorageSync('token'),
                },
                name: 'fileData',//服务器定义的Key值
                success: function (res) {
                    if (JSON.parse(res.data).code == 400) {
                        wx.hideLoading();
                        wx.showToast({
                            title: JSON.parse(res.data).message,
                            icon: 'none'
                        })
                        return
                    } else {
                        that.data.fileIdOrders.push({fileId: (JSON.parse(res.data).data)[0].id, order: 1})
                        if (that.data.videoSize > 20) {
                            console.log('视频222')
                            wx.showToast({
                                title: '视频不能超过20M',
                                icon: '',
                                image: '',
                                duration: 1500,
                                mask: true
                            })
                            return
                        }
                        if (that.data.fileIdOrders.length == 1 && that.data.video) {
                            that.setData({
                                hasAudio: true
                            })
                            that.subCommon();
                        }
                        console.log('视频上传成功')
                    }
                },
                fail: function () {
                    wx.showLoading({
                        title: '接口调用失败',
                        mask: true
                    })
                    return
                    console.log('接口调用失败')
                }
            })
            return
        }
        if (that.data.video.length == 0 && that.data.images.length == 0) {
            that.subCommon();
        }
    },

    subMit() {
        let that = this;
        let url, params;

        url = app.apiUrl + '/back/street/create';
        let newArr = [];
        that.data.fileIdOrders.forEach((item) => {
            newArr.push(
                {
                    fileId: item.fileId,
                    order: item.order,
                }
            )
        })
        params = {
            address: that.data.address,
            feature: that.data.feature,
            phone: that.data.phone,
            price: that.data.price,
            storeName: that.data.storeName,
            wechat: that.data.wechat,
            fileIdOrders: newArr,
            contentText: that.data.contentText,
            topicTypeId: that.data.topicTypeId,
            schoolId: wx.getStorageSync('schoolId'),
            //hasAudio:that.data.hasAudio
        };

        app.request.requestPostApi(url, params, that, that.successFun1, that.failFun, wx.getStorageSync('token'))
    },

    subCommon() {
        let that = this;
        that.subMit()
    },
    successFun1: function (res, selfObj) {
        wx.hideLoading()
        wx.showToast({
            title: "发布成功",
            icon: 'success',
            duration: 2000
        });
        setTimeout(function () {
            //wx.hideToast();
            app.globalData.release = true;
            selfObj.setData({
                videoSize: null
            })
            // wx.switchTab({
            //   url: '/pages/index/index'
            // })
        }, 2500);
    },
    failFun: function (res, selfObj) {
        console.log('failFun', res)
    },
    onUnload() {
        app.globalData.images = [];
        app.globalData.video = [];
    },
    bindTimeChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            time: e.detail.value
        })
    },
    bindDateChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            date: e.detail.value
        })
    },
    bindKeyInput(e) {
        let type = e.currentTarget.dataset.type;
        if (type == 1)
            this.setData({
                address: e.detail.value
            })
        if (type == 2)
            this.setData({
                feature: e.detail.value
            })
        if (type == 3)
            this.setData({
                phone: e.detail.value
            })
        if (type == 4)
            this.setData({
                price: e.detail.value
            })
        if (type == 5)
            this.setData({
                storeName: e.detail.value
            })
        if (type == 6)
            this.setData({
                wechat: e.detail.value
            })
    },

    change(e) {
        this.setData({
            topicTypeId: e.currentTarget.dataset.id,
            topic: parseInt(e.currentTarget.dataset.index),
        })
    },
    bindinput: function (e) {
        this.setData({
            contentText: e.detail.value,
            contentCount: e.detail.value.length
        });
    },
    removeImage(e) {
        debugger
        let idx = e.target.dataset.idx;
        let fileId = e.target.dataset.fileid;
        app.globalData.images.splice(idx, 1);
        let newOrders = this.data.fileIdOrders.filter(item => {
            return item.fileId != fileId
        })
        this.setData({
            images: app.globalData.images,
            fileIdOrders: newOrders
        });
    },
    removeVideo(e) {
        debugger
        app.globalData.video = [];
        this.setData({
            video: app.globalData.video,
            fileIdOrders: [],
            videoSize: null
        });
    },


    handleImagePreview(e) {
        debugger
        let idx = e.target.dataset.idx
        let images = this.data.images
        let imgsList = [];
        images.forEach(item => {
            imgsList.push(item)
        })
        wx.previewImage({
            current: images[idx],  //当前预览的图片
            urls: images,  //所有要预览的图片
        })
    },
});