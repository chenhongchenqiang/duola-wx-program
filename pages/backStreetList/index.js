  //index.js
  //获取应用实例
import { diaplayTime, lookImgs, tongZhi} from '../../utils/util';

  const app = getApp();

  Page({
      data: {
          load: true,
          leftList: [],
          rightList: [],
          leftHight: 0,
          rightHight: 0,
          currentPage: 1,
          totalPage:0,
          params:{
              pageNum:1
          }
      },






      goDetail(e) {
          let releaseId = e.currentTarget.dataset.id;
          let bizFileType = e.currentTarget.dataset.bizfiletype;
          let radio = e.currentTarget.dataset.radio;
          if (bizFileType == 2 && radio>1) {
              wx.navigateTo({
                  url: '/packageB/pages/videoDetail/index?id=' + releaseId + '&type=0'
              })
          } else {
              wx.navigateTo({
                  url: '/packageB/pages/dynamicDetail/index?id=' + releaseId + '&type=0'
              })
          }
      },
      goDetail(e) {
        let id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/packageB/pages/backStreetDetail/index?id=' + id
        })
    },
      onReachBottom: function () {
          debugger
          var that = this;
          if (that.data.load) {
              if (that.data.params.pageNum < that.data.noramalData.pages) {
                  that.setData({
                      'params.pageNum': that.data.params.pageNum * 1 + 1,
                  })
                  that.loadMore();
              }
          }
      },
      loadMore(){
          app.httpGet({
              url: "/back/street/list",
              data:{
                  topicTypeId:this.data.topicTypeId,
                  pageNum:this.data.params.pageNum,
                  schoolId:wx.getStorageSync('schoolId'),
                  pageSize:app.globalData.pageSize
              },
              token: wx.getStorageSync('token'),
              loading:true
          }).then((res) => {
              wx.hideNavigationBarLoading(); //完成停止加载图标
              wx.stopPullDownRefresh();
              this.successFun2(res)
          });
      },
      successFun2: function (res) {
          let that = this;
          var content = that.data.noramalData.list.concat(res.data.list)
          that.setData({
              'noramalData.list': content,
              lodaing: false
          });
          that.imgList()
      },
      onLoad(){
          let that = this;
          let pages = getCurrentPages() //获取加载的页面
          let currentPage = pages[pages.length - 1] //获取当前页面的对象
          let topicTypeId = currentPage.options.id //如果要获取url中所带的参数可以查看options
          let name = currentPage.options.name //如果要获取url中所带的参数可以查看options
          wx.setNavigationBarTitle({
            title: name,
          })
          app.httpGet({
              url: '/back/street/list?topicTypeId='+topicTypeId+'&pageNum=' + this.data.currentPage +'&pageSize='+app.globalData.pageSize+ '&schoolId=' + wx.getStorageSync('schoolId'),
              loading:false
          }).then((res) => {
              that.setData({
                  noramalData: res.data,
                  topicTypeId:topicTypeId
              })
              that.imgList()
          })
      },
      imgList() {
          var that = this;
          var allData = that.data.noramalData.list;
          //定义两个临时的变量来记录左右两栏的高度，避免频繁调用setData方法
          var leftH = that.data.leftHight;
          var rightH = that.data.rightHight;
          var leftData = [];
          var rightData = [];
          var leftHight = 0, rightHight = 0, itemWidth = 0, maxHeight = 0;
          wx.getSystemInfo({
              success: (res) => {
                  let percentage = 750 / res.windowWidth;
                  let margin = 2 / percentage;
                  itemWidth = (res.windowWidth - margin) / 2;//计算 瀑布流展示的宽度
                  maxHeight = 400//计算瀑布流的最大高度，防止长图霸屏 maxHeight = itemWidth / 0.8
              }
          });
          for (let i = 0; i < allData.length; i++) {
              let tmp = allData[i];
              tmp.width = tmp.releaseFiles.length>0?parseInt(tmp.releaseFiles[0].width):0;
              tmp.height = tmp.releaseFiles.length>0?parseInt(tmp.releaseFiles[0].height):0;
              tmp.itemWidth = 343
              let per = tmp.width / tmp.itemWidth;
              tmp.itemHeight = tmp.height / per;
              if (tmp.itemHeight > maxHeight) {
                  tmp.itemHeight = maxHeight;
              }
              if (leftH == rightH || leftH < rightH) {//判断左右两侧当前的累计高度，来确定item应该放置在左边还是右边
                  leftData.push(tmp);
                  leftH += tmp.itemHeight;
              } else {
                  rightData.push(tmp);
                  rightH += tmp.itemHeight;
              }
          }


          //更新左右两栏的数据以及累计高度
          that.setData({
              leftHight: leftH,
              rightHight: rightH,
              leftList: leftData,
              rightList: rightData
          })
      },
    
      getTopic(){
        app.httpGet({
            url: '/index/topic_type_list?type=2',
            loading:false
        }).then((res) => {
            this.setData({
                topic: res.data,
            })
      })
    }
    
  })
