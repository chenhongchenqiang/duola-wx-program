const app = getApp();
const formatTime = date => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()
    return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function timestampToTime(timestamp) {
    var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    return Y + M + D + h + m + s;
}

// timestampToTime(1403058804);
// console.log(timestampToTime(1403058804));//2014-06-18 10:33:24
function diaplayTime(updateTime) {
    if (updateTime === null) {
        return ''
    }
    let now = new Date().getTime()
    let second = Math.floor((now - updateTime) / (1000))
    let minute = Math.floor(second / 60)
    let hour = Math.floor(minute / 60)
    let day = Math.floor(hour / 24)
    let month = Math.floor(day / 31)
    let year = Math.floor(month / 12)
    if (year > 0) {
        //return year + '年前'
        return timestampToTime(updateTime / 1000);
        ;
    } else if (month > 0) {
        //return month + '月前'
        return timestampToTime(updateTime / 1000);
        ;
    } else if (day > 0) {
        let ret = day + '天前'
        if (day >= 7 && day < 14) {
            ret = '1周前'
        } else if (day >= 14 && day < 21) {
            ret = '2周前'
        } else if (day >= 21 && day < 28) {
            ret = '3周前'
        } else if (day >= 28 && day < 31) {
            ret = '4周前'
        }
        return ret
    } else if (hour > 0) {
        return hour + '小时前'
    } else if (minute > 0) {
        return minute + '分钟前'
    } else if (second > 0) {
        return second + '秒前'
    } else {
        return '刚刚'
    }
}

function sub1(val) {
    debugger
    if (val.length == 0 || val == undefined) {
        return;
    }
    if (val.length > 17) {
        return val.substring(0, 17) + "...";
    } else {
        return val;
    }
}

function tongZhi(res, selfObj, data) {
    debugger
    let date = new Date();
    let Y = date.getFullYear();
    let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
    let D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    let Time = Y + '年' + M + '月' + D + '日'
    wx.request({
        url: app.apiUrl + '/wx/send_msg',
        data: {
            touser: res.data.openid,
            page: data.video && data.confess ? '/packageB/pages/videoDetail/index?id=' + data.typeId + '&type=1' : data.confess ? '/packageB/pages/dynamicDetail/index?id=' + data.typeId + '&type=1' : data.video ? '/packageB/pages/videoDetail/index?id=' + data.typeId + '&type=0' : data.number == 1 ? '/packageB/pages/detailZj/index?id=' + data.id : '/packageB/pages/dynamicDetail/index?id=' + data.typeId + '&type=0',
            lang: 'zh_CN',
            template_id: data.confessContent ? app.globalData.template_id[1] : data.number == 1 ? app.globalData.template_id[2] : app.globalData.template_id[0],//'5GcK-s0tnWjiN2R-7ndbRdM3xFIg6uS7e9uo8MUaVYQ',
            //miniprogram_state: 'trial', //trial developer
            data: data.confessContent ? {
                thing1: {
                    value: data.contentText.length > 20 ? data.contentText.substring(0, 17) + "..." : data.contentText || '哆啦校圈'
                },
                thing2: {
                    value: data.confessContent.content.length > 20 ? data.confessContent.content.substring(0, 17) + "..." : data.confessContent.content
                },
                time3: {
                    value: Time
                },
            } : data.number == 1 ? {
                thing1: {
                    value: data.contentText.length > 20 ? data.contentText.substring(0, 17) + "..." : data.contentText || '哆啦校圈'
                },
                phone_number4: {
                    value: data.phone_number
                },
                number5: {
                    value: 1
                },
            } : {
                name1: {
                    value: data.nickName
                },
                date2: {
                    value: Time
                },
                thing3: {
                    value: data.contentText.length > 20 ? data.contentText.substring(0, 17) + "..." : data.contentText || '哆啦校圈'
                },
            }
        },
        header: {
            'content-type': 'application/json'
        },
        method: 'POST',
        dataType: 'json',
        success: function (res) {

        },
        fail: function () {

        },
    });
}

function lookImgs(event) {
    debugger
    var src, imgList;
    src = event.currentTarget ? app.apiUrlimg + event.currentTarget.dataset.src : app.apiUrlimg + event.src;
    imgList = event.currentTarget ? event.currentTarget.dataset.imgs : event.imgs;//获取data-list
    let imgsList = [];
    imgList.forEach(item => {
        if (item.picPath) {
            imgsList.push(app.apiUrlimg + item.picPath)
        } else {
            imgsList.push(app.apiUrlimg + item.fileRelativePath)
        }

    })
    //图片预览
    wx.previewImage({
        current: src, // 当前显示图片的http链接
        urls: imgsList // 需要预览的图片http链接列表
    })
}

const fsm = wx.getFileSystemManager();
const FILE_BASE_NAME = 'tmp_base64src';

function base64src(base64data) {
    return new Promise((resolve, reject) => {
        const [, format, bodyData] = /data:image\/(\w+);base64,(.*)/.exec(base64data) || [];
        if (!format) {
            reject(new Error('ERROR_BASE64SRC_PARSE'));
        }
        const filePath = `${wx.env.USER_DATA_PATH}/${FILE_BASE_NAME}.${format}`;
        const buffer = wx.base64ToArrayBuffer(bodyData);
        fsm.writeFile({
            filePath,
            data: buffer,
            encoding: 'binary',
            success() {
                resolve(filePath);
            },
            fail() {
                reject(new Error('ERROR_BASE64SRC_WRITE'));
            },
        });
    });
};

module.exports = {
    formatTime: formatTime,
    diaplayTime: diaplayTime,
    lookImgs: lookImgs,
    tongZhi: tongZhi,
    base64src: base64src
}
