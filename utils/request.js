const app=getApp();
function requestPostApi(url, params, sourceObj, successFun, failFun,isAuthor, completeFun) {
    requestApi(url, params, 'POST', sourceObj, successFun, failFun,isAuthor, completeFun)
}

function requestGetApi(url, params, sourceObj, successFun, failFun,isAuthor, completeFun) {
    requestApi(url, params, 'GET', sourceObj, successFun,failFun,isAuthor, completeFun)
}

function requestApi(url, params, method, sourceObj, successFun, failFun,isAuthor, completeFun) {
  // 登录

    if (method == 'POST') {
        var contentType = 'application/json'
    } else {
        var contentType = 'application/json'
    }
    let token='';
    if(isAuthor){
        token=isAuthor
    }
    wx.request({
        url:    url,
        method: method,
        data:   params,
        header: {'Content-Type': contentType,
                "X-Xnxw-Token":  token},
        success: function (res) {
            if(res.data.code==400||res.data.code==403){
                debugger
                wx.showToast({
                  title: res.data.message,
                  icon:'none'
                })
                return
            }
            if(res.data.code==401){
                wx.showModal({
                    title: '提示',
                    content: '用户已过期，请重新授权',
                    success (res) {
                      if (res.confirm) {
                        wx.navigateTo({
                            url:'/pages/author/index'
                        })
                      } else if (res.cancel) {
                        console.log('用户点击取消')
                      }
                    }
                  })
                return
            }
            typeof successFun  == 'function' && successFun(res.data, sourceObj,completeFun)
        },
        fail: function (res) {
            typeof failFun     == 'function' && failFun(res.data, sourceObj)
        },
        complete: function (res) {
            typeof completeFun == 'function' && completeFun(res.data, sourceObj)
        }
    })
}
module.exports = {
    requestPostApi,
    requestGetApi,
}