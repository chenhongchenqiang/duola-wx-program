const app = getApp();
Component({
    data: {

        color: "#000",
        selectedColor: "#342977",
        selected: 0,
        list: [
            {
                pagePath: "/pages/index/index",
                iconPath: "/images/nav/33.png",
                selectedIconPath: "/images/nav/44.png",
                text: "校友圈"
            },
            {
                pagePath: "/pages/team/index",
                iconPath: "/images/nav/kuo.png",
                selectedIconPath: "/images/nav/kuo1.png",
                text: "组队"
            },
            {
                pagePath: "/pages/chatList1/index",
              iconPath: "/images/home/addB.png",
              selectedIconPath: "/images/home/addB.png",
                text: ""
            },
            {
                pagePath: "/pages/hongbao/index",
                iconPath: "/images/nav/redBag.png",
                selectedIconPath: "/images/nav/redBag.png",
                text: ""
            },
            {
                pagePath: "/pages/my/index",
                iconPath: "/images/nav/11.png",
                selectedIconPath: "/images/nav/22.png",
                text: "我的"
            }
        ],
        show:false
    },
    properties: {
        isinfotan: Boolean
    },
    methods: {
        switchTab(e) {
            const data = e.currentTarget.dataset
            const url = data.path
            if(data.index==2){
                console.log(1111)
                this.aa();
                return
            }
            wx.switchTab({url})
            this.setData({
                selected: data.index
            })
        },
      hide(){
        this.setData({
          show: !this.data.show
        })
      },
        aa(){
            if(!wx.getStorageSync('token')){
                wx.navigateTo({
                    url:'/pages/author/index'
                })
                return
            };
            this.setData({
                show: !this.data.show
            })
        },
        send(e){
            let type=e.currentTarget.dataset.type;
            wx.navigateTo({
                url: '/pages/release/index?type='+type
            })
        },
        sendPicture() {
            wx.chooseImage({
                count: 9,
                sizeType: ['original', 'compressed'],
                sourceType: ['album', 'camera'],
                success(res) {
                    // tempFilePath可以作为img标签的src属性显示图片
                    const images = app.globalData.images.concat(res.tempFilePaths);
                    // 限制最多只能留下3张照片
                    app.globalData.images = images.length <= 9 ? images : images.slice(0, 9);
                    wx.navigateTo({
                        url: '/pages/release/index'
                    })
                    //$digest(this)
                }
            })
        },
        sendVideo() {
            wx.chooseVideo({
                sourceType: ['album', 'camera'],
                maxDuration: 60,
                duration: 10,
                camera: 'back',
                success(res) {
                    app.globalData.video = res.tempFilePath;
                    app.globalData.size = (res.size / (1024 * 1024)).toFixed(2);
                    wx.navigateTo({
                        url: '../release/index'
                    })
                }
            })
        },
    }
})