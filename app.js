//app.js
const request = require('./utils/request.js')
const APP_ID = 'wx9ddd73d26fdbacba';//输入小程序appid//wx97381a87ca907d20,wx9ddd73d26fdbacba
//const APP_SECRET = 'b67fe05c597b0ee51b86ddedc1cd0146';//输入小程序app_secret
const APP_SECRET = '9693e597984f9d10719396334b18f57f'//d6a1ab54644264f6b81b421d65b43e45
App({
    secret: APP_SECRET,
    request: request,
    onLaunch: function () {
        let token = wx.getStorageSync('token');
        //登录
        wx.login({
            success: res => {
                if(res.code) {
                    this.globalData.code=res.code
                }
            }
        })
        if (wx.canIUse('getUpdateManager')) {
            const updateManager = wx.getUpdateManager()
            updateManager.onCheckForUpdate(function (res) {
                if (res.hasUpdate) {
                    updateManager.onUpdateReady(function () {
                        wx.showModal({
                            title: '更新提示',
                            content: '优化升级',
                            success: function (res) {
                                if (res.confirm) {
                                    updateManager.applyUpdate()
                                }
                            }
                        })
                    })
                    updateManager.onUpdateFailed(function () {
                        wx.showModal({
                            title: '已经有新版本了哟~',
                            content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~'
                        })
                    })
                }
            })
        } else {
            wx.showModal({
                title: '提示',
                content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
            })
        }

        // 展示本地存储能力
        var logs = wx.getStorageSync('logs') || []
        logs.unshift(Date.now())
        wx.setStorageSync('logs', logs)

        wx.getSystemInfo({
            success: res => {
                this.globalData.navHeight = res.statusBarHeight + 46;
                // 通过像素比计算出画布的实际大小（330x490）是展示的出来的大小
                this.globalData.width = res.windowWidth,
                this.globalData.height = res.windowHeight,
                this.globalData.pixelRatio = res.pixelRatio
                const model = res.model;
                const modelInclude = ["iPhone X", 'iPhone XR', "iPhone XS", "iPhone XS MAX","iPhone 11","iPhone 12"];
                for (let i = 0; i < modelInclude.length; i++) {
                  //模糊判断是否是modelInclude 中的机型,因为真机上测试显示的model机型信息比较长无法一一精确匹配
                  if (model.indexOf(modelInclude[i]) != -1) {
                    this.globalData.isIphoneX = true
                  }
                }
            }
        });

      

        // 获取用户信息
        wx.getSetting({
            success: res => {
                console.log(1)
                if (res.authSetting['scope.userInfo']) {
                    // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                    wx.getUserInfo({
                        success: res => {
                            // 可以将 res 发送给后台解码出 unionId
                            //this.globalData.userInfo = res1.userInfo
                            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                            // 所以此处加入 callback 以防止这种情况
                            if (this.userInfoReadyCallback) {
                                this.userInfoReadyCallback(res)
                            }
                      
                        }
                    })
                }
            }
        })


    },

    apiUrl: 'https://xxm.jishangkj.cn/duola',
    outerUrl: 'https://xxm.jishangkj.cn/duola-h5/#',
    apiUrlimg: 'https://xxm.jishangkj.cn/document',
    apiUrl2: 'https://xxm.jishangkj.cn/file',
  // apiUrl: 'https://xxm.jishangkj.cn/duola-test',
  // outerUrl: 'https://xxm.jishangkj.cn/duola-h5/#',
  // apiUrlimg: 'https://xxm.jishangkj.cn/document',
  // apiUrl2: 'https://xxm.jishangkj.cn/file',

  httpBase: function (method, url, data, token, loading, loadingMsg) {
        let _this = this;
        let requestUrl = this.apiUrl + url;
        if (loading) {
            wx.showLoading({
                title: loadingMsg || '加载中...',
                mask: true
            });
        } else {
            wx.showNavigationBarLoading()
        }

        function request(resolve, reject) {
            wx.request({
              header: {
                'Content-Type': 'application/json',
                "X-Xnxw-Token": token
              },
               
                
                method: method,
                url: requestUrl,
                data: data,
                success: function (result) {
                    if (loading) {
                        wx.hideLoading();
                    } else {
                        wx.hideNavigationBarLoading()
                    }

                    let res = result.data || {};
                    let code = res.code;
                    if(code==401){
                        reject(res);
                        wx.showModal({
                            title: '提示',
                            content: '用户已过期，请重新授权',
                            success (res) {
                              if (res.confirm) {
                                wx.navigateTo({
                                    url:'/pages/author/index'
                                })
                              } else if (res.cancel) {
                                console.log('用户点击取消')
                              }
                            }
                        })
                        return
                    }
                    if (code == 400) {
                        reject(res);
                        if (res.message) {
                            wx.showToast({
                                title: res.message,
                                icon: 'none'
                            });
                        }
                    } else {
                        resolve(res);
                    }
                },
                fail: function (res) {
                    reject(res);
                    if (loading) {
                        wx.hideLoading();
                    } else {
                        wx.hideNavigationBarLoading()
                    }
                    wx.showToast({
                        title: '网络出错',
                        icon: 'none'
                    });
                }
            })
        }
        return new Promise(request);
    },
    httpGet: function ({ url = "", data = {}, token = token,loading = false, loadingMsg = ""} = {}) {
      return this.httpBase("GET", url, data,token, loading, loadingMsg);
    },
    httpPost: function ({url = "", data = {}, token = token,loading = false, loadingMsg = ""} = {}) {
        return this.httpBase("POST", url, data,token, loading, loadingMsg);
    },

    globalData: {
        token: '',
        code: '',
        userInfo: null,
        sendImagesSize: 9,
        images: [],
        video: [],
        size: 0,
        gender: 0,
        pageSize: 10,
        release: false,
        access_token: '',
      template_id: ['onUz2ydLLWL-i00Cy7Apwb_faeHU0H_PpvxkoQkqqQQ','XrhIBtcvhPcQsqY4Ww-9fEopFiY0ZKWwsrYfIJjwsbc','ReWV7MZnilXMqPMb_gJRqQwfOX0eQynlfYKJCgf8deY'],
        systeminfo: false,   //系统信息
        headerBtnPosi: false,  //头部菜单高度
        tabBarHeight:0,
        navbarHeight: 0,
        schoolId: '',
        inNeedUser: false,
        isIphoneX: false,
        width: null,
        height: null,
        navHeight:0,
        pixelRatio: null,
      confessContent:{},
      commentId:null,
      message:0
    },

})
